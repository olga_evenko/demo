from rest_framework.generics import ListAPIView
from django.http import JsonResponse
from django.core.paginator import Paginator
from django.apps import apps
from datetime import *

import json
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework import status
from django.db import connections
from rest_framework.permissions import IsAuthenticated, AllowAny, BasePermission,IsAuthenticatedOrReadOnly

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.decorators import detail_route, list_route, permission_classes
from operator import itemgetter

# from backend.backend.PROJECT.models import Project
from .models import *
from .serializers import *
from TICKETS.models import *
from TICKETS.serializers import *
from plus_administration.models import *
from django.db import connections
from xlsxwriter.workbook import Workbook
from django.http import HttpResponse
from operator import itemgetter

@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
    })

from rest_framework import viewsets


class ProjectList(viewsets.ModelViewSet):

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    authentication_class = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def list(self, request):
        project = Project.objects.all()
        # visitors = Tickets.objects.filter(category__project=project, order__state=1)
        # count_visitors = visitors.count()

        if 'data' in request.GET:

            serverData = json.loads(request.GET['data'])

            if (serverData['sort']['field'] and serverData['sort']['type']):

                if serverData['sort']['type'] == 'desc':
                    serverData['sort']['field'] = '-' + serverData['sort']['field']

                project = project.order_by(serverData['sort']['field'])

            if 'select' in serverData and serverData['select'].keys().__len__():
                for select in serverData['select']:
                    if type(serverData['select'][select])==list and select.find('__in')<0:

                        serverData['select'][select + '__in'] = serverData['select'][select]
                        del serverData['select'][select]
                project = project.filter(**serverData['select'])

            if request.user.is_authenticated:
                temp_content = ProjectSerializerList(project, many=True).data
                paginator = Paginator(temp_content, serverData['perPage'])
                content = paginator.page(serverData['page']).object_list

            else:
                content = ProjectSerializerMobil(project, many=True).data

            count_visitors = project.count()

            return Response({'rows': content, 'totalRecords': count_visitors})

        # content = ProjectSerializerList(project, many=True).data
        return Response({'rows': {}, 'totalRecords': 0})

    @list_route(methods=['get'])
    def events(self, request):
        events = Events.objects.exclude(project__status='end')
        content = EventsSerializerView(events, many=True).data
        return Response(data=content)


    @list_route(methods=['get'])
    def statistics(self, request, ):

        projects = Project.objects.all().order_by('date_end')
        if 'data' in request.GET:
            serverData = json.loads(request.GET['data'])

            if (serverData['sort']['field'] and serverData['sort']['type']):

                if serverData['sort']['type'] == 'desc':
                    serverData['sort']['field'] = '-' + serverData['sort']['field']

                projects = project.order_by(serverData['sort']['field'])

            if 'select' in serverData and serverData['select'].keys().__len__():

                for select in serverData['select']:
                    if type(serverData['select'][select])==list and select.find('__in')<0:

                        serverData['select'][select + '__in'] = serverData['select'][select]
                        del serverData['select'][select]

                projects = projects.filter(**serverData['select'])

            content = ProjectSerializerList(projects, many=True).data

            tickets={'activate':[], 'visitation':[], 'base_data':[]}

            for project in projects:
                ticket = Tickets.objects.filter(category__project_id = project.id).exclude(order__state=3).exclude(order__state=0)

                if ticket.count():

                    order_pay = Order.objects.filter(state=1, project__id=project.id)
                    order_summ = 0
                    for order in order_pay:
                        order_summ += order.amount

                    # tickets['amount_order'] = order_summ

                    tickets['activate'].append({
                        'status':project.status,
                        'date_end':project.date_end,
                        'name':project.name,
                        'id':project.id,
                        'activate': ticket.filter(state = True, status=True, order__state=1).count(),
                        'noactivate': ticket.filter(state = False, status=True, order__state=1).count(),
                        'reset': ticket.filter(status=False,  order__state=2).count(),
                        'amount_order' : order_summ
                    })



                    tickets['base_data'] .append([{
                            'type': 'money_head',
                            'label': 'Всего',
                            'count': ticket.count(),
                        },{
                            'type':'money_row',
                            'label': 'Оплачено',
                            'count': ticket.count() - ticket.filter(status=False, order__state=2).count(),
                            'percent':str(round((ticket.count() - ticket.filter(status=False, order__state=2).count())*100/ticket.count(), 2)) + ' %',
                        }, {
                            'type': 'money_row',
                            'label': 'Отменено',
                            'count': ticket.filter(status=False).count(),
                            'percent': str(round(ticket.filter(status=False, order__state=2).count() * 100 / ticket.count(),2)) + ' %',

                        }, {
                            'type': 'activate_head',
                            'label': 'Всего оплачено',
                            'count': ticket.count() - ticket.filter(status=False, order__state=2).count(),

                        },{
                            'type': 'activate_row',
                            'label': 'Активировано',
                            'count': ticket.filter(state = True, status=True, order__state=1).count(),
                            'percent':str(round(ticket.filter(state = True, status=True, order__state=1).count()*100/(ticket.count() - ticket.filter(status=False, order__state=2).count()), 2)) + ' %',
                        },{
                            'type': 'activate_row',
                            'label': 'Не активировано',
                            'count': ticket.filter(state = False, status=True, order__state=1).count(),
                            'percent': str(round(ticket.filter(state = False, status=True, order__state=1).count() * 100 / (ticket.count() - ticket.filter(status=False, order__state=2).count()), 2)) + ' %',

                        }])

            # # for project in projects:
            #     ticket = ticket.filter(state = True, status=True, order__state=1)
            #     if ticket.count():
            #         date = []
            #         result_ticket = []
            #         for t in ticket:
            #             if t.date_activate.strftime('%d-%m-%Y') in date:
            #                 continue
            #             else:
            #                 date.append(t.date_activate.strftime('%d-%m-%Y'))
            #
            #         date.sort()
            #
            #         for d in date:
            #             temp_date = d.split('-')
            #
            #             category = TicketsCategory.objects.filter(project = project)
            #
            #             ticket_filter_day = ticket.filter(date_activate__day = temp_date[0],
            #                                            date_activate__month = temp_date[1],
            #                                            date_activate__year = temp_date[2])
            #
            #             temp_ticket = {
            #                 'date':d,
            #                 'data':[]
            #             }
            #
            #             temp_ticket['data'].append({
            #                 'label': 'Всего',
            #                 'color': '#e91e63',
            #                 'count': ticket_filter_day.count()
            #             })
            #
            #             for c in category:
            #                 temp_ticket['data'].append({
            #                     'label': c.name,
            #                     'color': c.color if not not c.color else '#909399',
            #                     'count': ticket_filter_day.filter(category=c).count()
            #                 })
            #
            #             result_ticket.append(temp_ticket)
            #
            #         if ticket.count():
            #             tickets['visitation'].append(result_ticket)
            #
            # for i in tickets['visitation']:
            #     for j in i:
            #         j['data'] = sorted(j['data'], key=itemgetter('count'), reverse=True)

            return Response(data=tickets)


        return Response(data={})


class ProjectDetail(viewsets.ModelViewSet):

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    authentication_class = (JSONWebTokenAuthentication,)
    # permission_classes = (IsAuthenticatedOrReadOnly,)
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def list(self, request, id=None, pk=None):
        project = Project.objects.get(id=id)
        visitors = Tickets.objects.filter(category__project=project, order__state=1)
        count_visitors = visitors.count()

        if 'data' in request.GET:
            serverData = json.loads(request.GET['data'])

            if (serverData['sort']['field'] and serverData['sort']['type']):

                if serverData['sort']['field'] == 'buyer':
                    serverData['sort']['field'] = 'order__buyer'

                if serverData['sort']['type'] == 'desc':
                    serverData['sort']['field'] = '-' + serverData['sort']['field']

                visitors = visitors.order_by(serverData['sort']['field'])

            if 'select' in serverData and serverData['select']['state']:
                visitors = visitors.filter(**serverData['select'])

            if 'search' in serverData and len(serverData['search']['value']) >= 2:
                # visitors = visitors.filter(number__icontains=serverData['search']['value'])
                visitors = visitors.filter(Q(number__icontains=serverData['search']['value']) |
                                           Q(code__icontains=serverData['search']['value']) |
                                           Q(order__buyer__icontains=serverData['search']['value']))

            temp_content = VisitorsSerializerList(visitors, many=True).data
            paginator = Paginator(temp_content, serverData['perPage'])
            content = paginator.page(serverData['page']).object_list

            count_visitors = visitors.count()

            return Response({'rows': content, 'totalRecords': count_visitors})

        content = VisitorsSerializerList(visitors, many=True).data
        return Response({'rows': content, 'totalRecords': count_visitors})

    def retrieve(self, request, pk=None):
        try:
            if request.auth:
                try:
                    if pk == 'new':
                        pk=None
                    obj = Project.objects.get(id=pk)
                except Project.DoesNotExist:
                    obj = Project()

                if not obj:
                    obj = Project()

                serializer = ProjectSerializer(obj)
                content = serializer.data
                return Response(content)
            else:
                try:
                    obj = Project.objects.get(id=pk)
                    serializer = ProjectSerializerMobil(obj)
                    content = serializer.data
                    return Response(content)
                except Project.DoesNotExist:
                    return Response({'detail': ' Ошибочный запрос'}, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response({'detail':' Ошибочный запрос'}, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['get'])
    def options(self, request, id=None):
        content={}
        if request.auth:
            cursor = connections['default'].cursor()
            cursor.execute("SELECT id, name FROM public.projects_project WHERE date_end >= '%s' " % (
            datetime.datetime.now().strftime("%Y-%m-%d")))

            content['p_expobit'] = []
            for row in cursor.fetchall():
                temp = {}
                temp['id'] = str(row[0])
                temp['name'] = row[1]
                content['p_expobit'].append(temp)

            fcm = FcmData.objects.all()
            content['fcm_data'] = []
            for row in fcm:
                temp = {}
                temp['id'] = str(row.id)
                temp['name'] = row.project_label
                content['fcm_data'].append(temp)

            content['type_confirm_user'] = []
            for row in Project.TYPE_CONFIRM:
                temp = {}
                temp['id'] = row[0]
                temp['name'] = row[1]
                content['type_confirm_user'].append(temp)

            content['status'] = []
            for row in Project.STATUS:
                temp = {}
                temp['id'] = row[0]
                temp['name'] = row[1]
                content['status'].append(temp)

            content['is_form_type'] = []
            for row in Project.IS_FORM_TYPE:
                temp = {}
                temp['id'] = row[0]
                temp['name'] = row[1]
                content['is_form_type'].append(temp)

            content['type_printing'] = []
            for row in Project.TYPE_PRINTING:
                temp = {}
                temp['id'] = row[0]
                temp['name'] = row[1]
                content['is_form_type'].append(temp)
            return Response(content)
        return Response({"detail": "Учетные данные не были предоставлены."}, status=status.HTTP_401_UNAUTHORIZED)

    @detail_route(methods=['get'])
    def dataDev(self, request, pk=None):

        if 'id' in request.GET and request.auth:
            cursor = connections['default'].cursor()
            cursor.execute("SELECT *  FROM public.projects_project WHERE id = '%s'" % (request.GET['id']))
            results = []
            colnames = [desc[0] for desc in cursor.description]
            for row in cursor.fetchall():
                results.append(dict(zip(colnames, row)))

            for res in results[0]:
                results[0][res] = str(results[0][res])

            results[0]['status'] = 'plane'

            return Response(results)
        return Response({"detail": "Учетные данные не были предоставлены."}, status=status.HTTP_401_UNAUTHORIZED)

    def get_data_object(self, pk):
        try:
            int(pk)
            return Project.objects.get(id=pk)
        except:
            return

    def get_object_ser(self, pk):
        obj = self.get_data_object(pk)

        if not obj:
            obj = Project()
        serializer = ProjectSerializer(obj)
        data = {}
        data['formData'] = serializer.data
        return (obj, data)

    def save_data(req, temp, obj):
        if obj:

            if 'logo' in temp:
                if temp['logo'] == 'delete':
                    obj.logo = None
                temp.pop('logo')

            if 'ticket_logo' in temp:
                if temp['ticket_logo'] == 'delete':
                    obj.ticket_logo = None
                temp.pop('ticket_logo')

            if 'fcmData' in temp:
                if temp['fcmData'] == 'None' or temp['fcmData'] == 'null':
                    obj.fcmData = None
                    temp.pop('fcmData')

            serializer = ProjectSerializer(obj, data=temp)
            serializer._args[0].logo = obj.logo
            serializer._args[0].ticket_logo = obj.ticket_logo
            if serializer.is_valid():
                serializer.validated_data
                obj = serializer.save()
                if req.FILES != {}:
                    temp.update(req.FILES)
                    serializer = ProjectSerializer(obj, data=temp)

                    if serializer.is_valid():
                        # serializer.validated_data['logo']
                        serializer.validated_data
                obj = serializer.save()
                serializer = ProjectSerializer(obj)
                data = {}
                data['formData'] = serializer.data
                data['active_button'] = 'view'

                return Response(data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['get'])
    def data(self, request, pk=None):
        obj = self.get_data_object(pk)
        serializer = ProjectSerializerMobil(obj)
        data = serializer.data
        return JsonResponse(data=data, safe=False)

    # для админ панели
    @detail_route(methods=['get'])
    def data_admin(self, request, pk=None):
        content = {
            'auth': request.auth,  # None
        }

        obj, data = self.get_object_ser(pk)

        cursor = connections['default'].cursor()
        cursor.execute("SELECT id, name FROM public.projects_project WHERE date_end >= '%s'" % (
            datetime.datetime.now().strftime("%Y-%m-%d")))

        data['p_expobit'] = []
        for row in cursor.fetchall():
            temp = {}
            temp['id'] = str(row[0])
            temp['name'] = row[1]
            data['p_expobit'].append(temp)

        fcm = FcmData.objects.all()
        data['fcm_data'] = []
        for row in fcm:
            temp = {}
            temp['id'] = str(row.id)
            temp['name'] = row.project_label
            data['fcm_data'].append(temp)

        data['type_confirm_user'] = []
        for row in obj.TYPE_CONFIRM:
            temp = {}
            temp['id'] = row[0]
            temp['name'] = row[1]
            data['type_confirm_user'].append(temp)

        data['status'] = []
        for row in obj.STATUS:
            temp = {}
            temp['id'] = row[0]
            temp['name'] = row[1]
            data['status'].append(temp)

        if obj:
            data['active_button'] = "view"
            return Response(data)
        data['active_button'] = "add"
        return Response(data)

    @detail_route(methods=['post'])
    def add(self, request, pk=None):
        try:
            temp = request.POST.copy()

            if pk=='new':
                obj = Project()
            else:
                obj = Project.objects.get(id = pk)
                if temp['status'] == 'sale' and obj.is_pay:
                    if not obj.sbr:
                        return Response({'detail': 'Платежные данные не установлены'}, status=status.HTTP_400_BAD_REQUEST)

            if obj:
                if 'logo' in temp:
                    if temp['logo'] == 'delete':
                        obj.logo = None
                    temp.pop('logo')

                if 'ticket_logo' in temp:
                    if temp['ticket_logo'] == 'delete':
                        obj.ticket_logo = None
                    temp.pop('ticket_logo')

                if 'fcmData' in temp:
                    if temp['fcmData'] == 'None' or temp['fcmData'] == 'null':
                        obj.fcmData = None
                        temp.pop('fcmData')

                temp['is_form_type'] = 'n_buy'

                serializer = ProjectSerializer(obj, data=temp)
                serializer._args[0].logo = obj.logo
                serializer._args[0].ticket_logo = obj.ticket_logo
                if serializer.is_valid():
                    serializer.validated_data
                    obj = serializer.save()
                    if request.FILES != {}:
                        temp.update(request.FILES)
                        serializer = ProjectSerializer(obj, data=temp)

                        if serializer.is_valid():
                            serializer.validated_data
                    obj = serializer.save()
                    serializer = ProjectSerializer(obj)
                    data = {}
                    data = serializer.data

                    return Response(data)

            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response({'detail':' Ошибочный запрос'}, status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['post'])
    def save(self, request, pk=None):
        temp = request.POST.copy()
        obj = self.get_data_object(pk)
        obj.user = ExtUser.objects.get(username=request.user)
        data = ProjectDetail.save_data(request, temp, obj)
        return data

    @detail_route(methods=['post'])
    def delete(self, request, pk, format=None):
        obj = self.get_data_object(pk)
        obj.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ReportsProject(viewsets.ModelViewSet):

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    authentication_class = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def list(self, request):
        project = Project.objects.all()
        # visitors = Tickets.objects.filter(category__project=project, order__state=1)
        # count_visitors = visitors.count()

        if 'data' in request.GET:

            serverData = json.loads(request.GET['data'])

            if (serverData['sort']['field'] and serverData['sort']['type']):

                if serverData['sort']['type'] == 'desc':
                    serverData['sort']['field'] = '-' + serverData['sort']['field']

                project = project.order_by(serverData['sort']['field'])

            if 'select' in serverData and serverData['select'].keys().__len__():
                for select in serverData['select']:
                    if type(serverData['select'][select])==list and select.find('__in')<0:

                        serverData['select'][select + '__in'] = serverData['select'][select]
                        del serverData['select'][select]
                project = project.filter(**serverData['select'])

            if request.user.is_authenticated:
                temp_content = ProjectSerializerList(project, many=True).data
                paginator = Paginator(temp_content, serverData['perPage'])
                content = paginator.page(serverData['page']).object_list

            else:
                content = ProjectSerializerMobil(project, many=True).data

            count_visitors = project.count()

            return Response({'rows': content, 'totalRecords': count_visitors})

        # content = ProjectSerializerList(project, many=True).data
        return Response({'rows': {}, 'totalRecords': 0})

    @list_route(methods=['get'])
    def options(self, request, id=None):

        def rules_valid(table, child):
            rules = {
                'Project': ['fcmData', 'sbr', ],
            }

            if not (table in rules) or not(child in rules[table]):
                return True
            return False

        def append_data(arr, table, column_name, name, column, json, layer):

            rules0 = {
                'Order':['id', ''],
                'Project':['id', 'p_expobit', 'fcmData', 'metrika'],
                'Tickets':['loyalty_promocode', 'ticket_graph', 'ticket_check', 'uuid', 'ticket_email']
            };
            rules1 = {
                'Order': ['id', 'pan', 'uuid', 'receipt id', 'cardAuthInfo'],
                'Project': ['metrika', "date_start", 'status', "date_end", "description", 'description_event',\
                            "time_start", "time_end", "time_last", "logo", "is_business", "is_weekend", 'is_plane', \
                            'is_form','is_pay', 'is_form_type', 'type_confirm_user', 'is_promocode',  "status_is_site",\
                            "status_is_kiosk",'p_expobit', 'ticket_logo', 'type_printing', 'status_is_cash'],
                'TicketsCategory':['id', 'description', 'ticket_img', ],
                'Promocode':['id', '']
            };

            if layer:
                rules = rules1
            else:
                rules = rules0

            if not (table in rules) or not(column in rules[table]):
                for item in arr:
                    if item['value'] == column_name:
                        return
                    print(item['value'])

                arr.append({
                    'value': column_name,
                    'label':  name,
                    'json':json,
                })

        def get_related(Model, arr, head, name):
            for field in Model._meta.get_fields():
                if Model._meta.get_field(field.name).get_internal_type() == 'ForeignKey':

                    if field.one_to_many:
                        continue
                    if field.many_to_one:
                        if rules_valid(Model._meta.object_name, field.name):
                            get_related(field.related_model, column, head + "__" + field.name, field.verbose_name)
                        continue
                if Model._meta.get_field(field.name).get_internal_type() == 'ManyToManyField':
                    continue

                append_data(arr, Model._meta.object_name,  head + "__"+ field.column, name + '/ ' + field.verbose_name,\
                            field.column, Model._meta.get_field(field.name).get_internal_type() == 'JSONField', 1)

        if request.GET['data']:

            data = json.loads(request.GET['data'])
            Model = apps.get_model('TICKETS', data['table'])

            column = []

            for field in Model._meta.get_fields():
                if Model._meta.get_field(field.name).get_internal_type() == 'ForeignKey':
                    if field.one_to_many:
                        print('one_to_many ------ ' + field.name)
                        continue
                    if field.many_to_one:
                        if rules_valid(Model._meta.object_name, field.name):
                            get_related(field.related_model, column, field.name, field.verbose_name)

                        print('many_to_one ------ ' + field.name)
                        continue

                append_data(column, data['table'], field.column, field.verbose_name, field.column, \
                            Model._meta.get_field(field.name).get_internal_type() == 'JSONField', 0)

            newlist = sorted(column, key=itemgetter('label'))
            return Response(data=newlist)

    @list_route(methods=['get'])
    def get_type_field(self, request, id=None, ):

        if request.GET['data']:
            data = json.loads(request.GET['data'])

            Model = apps.get_model('TICKETS', data['table'])
            filter = data['filter']

            ModelNew = Model
            arr_col = filter['column'].split('__')
            i = 0
            type_field = None
            choices=[]
            while i < arr_col.__len__():
                fields = ModelNew._meta.get_field(arr_col[i])
                i += 1
                if i < arr_col.__len__():
                    ModelNew = fields.related_model
                else:
                    type_field = ModelNew._meta.get_field(arr_col[i-1]).get_internal_type()
                    choices = ModelNew._meta.get_field(arr_col[i-1]).choices
            json_choice = {
                'ques':[],
                'answer':[],
            }

            if type_field == 'ForeignKey':
                t_category = model._meta.get_field(data[0]).remote_field.model.objects.filter(project_id=id)
                choices = []
                for t_c in t_category:
                    choices.append([t_c.id, t_c.name + (' (Выходной)' if t_c.is_weekend else ' (Будний)')])

            if type_field == 'JSONField' :
                if  data['table'] == 'Forms_Data':
                    form = Forms.objects.get(project__id = id, type = 'registr')
                    json_data = json.loads(form.data)

                    for js in json_data:
                        if js['type'] == 'choice':
                            json_choice['ques'].append(js['text'])
                            json_choice['answer'].append(js['params']['choice'])

            return Response({
                'type_field': type_field,
                'choices': choices,
                'json_choice':json_choice
            })

    @list_route(methods=['get'])
    def create_report(self, request, id=None):
        if request.GET['data']:
            data = json.loads(request.GET['data'])
            Model = apps.get_model('TICKETS', data['table'])

            query = Model.objects.all()

            filters={}

            for filter in data['filter']:
                if filter['column'] in filters:
                    filters[filter['column']].append({
                        'column': filter['column'],
                        'operator': filter['operator'],
                        'value': filter['value'],
                        'json': filter['json']
                    })
                else:
                    filters[filter['column']]=[{
                        'column':filter['column'],
                        'operator':filter['operator'],
                        'value':filter['value'],
                        'json':filter['json']
                    },]

            for filter in filters:
                # col = filter
                temp = []
                data_query=[]
                for obj in filters[filter]:
                    if obj['json']['ques']:
                        data_query = query
                    else:
                        if data_query:
                            data_query = data_query.filter(**({obj['column'] + obj['operator']: obj['value']}))
                        else:
                            data_query = query.filter(**({obj['column']+obj['operator']:obj['value']}))


                    if temp:
                        temp = (data_query | temp).distinct()
                    else: temp = data_query
                query = data_query
            serializer=[]
            for res in query:
                temp_setializer_row={}
                for column in json.loads(request.GET['data'])['columns']:
                    col = column.split("__")
                    i = 0
                    value = res
                    while i < col.__len__():
                        value = value.__getattribute__(col[i])
                        i+=1
                        if i==col.__len__():
                            temp_setializer_row[column] = value


                serializer.append(temp_setializer_row)

            # для анкет
            temp_mass=[]
            for filter in filters:
                result = []
                for obj in filters[filter]:
                    if obj['json']['ques']:
                        form = Forms.objects.get(project__id=id, type='registr')
                        json_data = json.loads(form.data)

                        for i in data['columns']:
                            if i == obj['column']:
                                data['columns'].append({'data':json_data})


                        index=0
                        for js in json_data:
                            if js['type'] == 'choice':
                                index += 1
                                if str(index) == obj['json']['ques']:
                                    for jsn in serializer:
                                        json_data_form = json.loads(jsn['data'])
                                        a = 0
                                        try:
                                            if json_data_form and json_data_form[js['id']]['value'] == obj['json']['answer'] and not obj['operator']:
                                                if not (jsn['id'] in temp_mass):
                                                    temp_mass.append(jsn['id'])
                                                    result.append(jsn)
                                            if json_data_form and json_data_form[js['id']]['value'] != obj['json']['answer'] and obj['operator']:
                                                if not (jsn['id'] in temp_mass):
                                                    temp_mass.append(jsn['id'])
                                                    result.append(jsn)
                                        except:
                                            pass

                        serializer = result
            if not data['download']:
                return Response({'data':serializer, 'column':data['columns']})

            try:
                from StringIO import StringIO
            except ImportError:
                from io import BytesIO

            from django.http import HttpResponse
            from xlsxwriter.workbook import Workbook

            output = BytesIO()
            book = Workbook(output)
            sheet = book.add_worksheet('test')

            head = book.add_format({
                'bold': 1,
                'align': 'center',
                'valign': 'vcenter',
            })

            text = book.add_format({
                'align': 'center',
                'valign': 'vcenter',
            })


            sheet.set_default_row(18)
            sheet.set_column('A:Z', 10)

            col = 0
            row = 1
            for column in json.loads(request.GET['data'])['columns']:
                if data['table'] == 'Forms_Data' and column=='data':
                    form = Forms.objects.get(project__id=id, type='registr')
                    json_data = json.loads(form.data)

                    for i in json_data:
                        sheet.write(1, col, i['text'], head)
                        col += 1
                else:
                    ModelNew = Model
                    arr_col = column.split('__')
                    i=0
                    while i< arr_col.__len__():
                        fields = ModelNew._meta.get_field(arr_col[i])
                        i+=1
                        if i < arr_col.__len__():
                            ModelNew = fields.related_model
                        else:
                            sheet.write(row, col, fields.verbose_name, head)
                            col += 1
            row+=1
            col=0

            for ser in serializer:

                for column in json.loads(request.GET['data'])['columns']:
                    if data['table'] == 'Forms_Data' and column=='data':
                        form = Forms.objects.get(project__id=id, type='registr')
                        form_head = json.loads(form.data)
                        ser_json = json.loads(ser['data'])
                        for i in ser_json:

                            if form_head[int(i)]['type'] == 'choice':
                                temp_local = ''
                                for n in form_head[int(i)]['params']['choice']:
                                    # print(ser_json[i])
                                    if 'value' in ser_json[i]:
                                        if type(ser_json[i]['value']) == list:
                                            for l_t in ser_json[i]['value']:
                                                if l_t == n['value']:
                                                    temp_local += n['text'] + ' '
                                        else:
                                            if ser_json[i]['value'] == n['value']:
                                                temp_local += n['text'] + ' '
                                    else:
                                        temp_local += ser_json[i] + ' '


                                        # a = n
                                sheet.write(row, col, str(temp_local), text)

                            elif form_head[int(i)]['type'] == 'city':

                                if ser_json[i] and 'name' in ser_json[i]:
                                    city = City.objects.get(id=ser_json[i]['id'])
                                    temp_local = '%s, %s, %s' % (city.region.country.name, city.region.name, city.name)
                                    sheet.write(row, col, str(temp_local), text)

                                if ser_json[i] and 'city' in ser_json[i] and ser_json[i]['city'] and 'id' in ser_json[i]['city']:
                                    city = City.objects.get(id=ser_json[i]['city']['id'])
                                    temp_local = '%s, %s, %s' % (city.region.country.name, city.region.name, city.name)
                                    sheet.write(row, col, str(temp_local), text)

                            else:
                                sheet.write(row, col, str(ser_json[i]), text)
                            col += 1

                    else:
                        sheet.write(row, col, ser[column], text)
                        col += 1
                row += 1
                col = 0

            book.close()
            output.seek(0)
            response = HttpResponse(output.read(),
                                            content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            response['Content-Disposition'] = "attachment; filename=report.xlsx"

            return response

    @list_route(methods=['get', 'post'])
    def download_report_form(self, request, id=None, ):

        try:
            from StringIO import StringIO
        except ImportError:
            from io import BytesIO

        from django.http import HttpResponse

        from xlsxwriter.workbook import Workbook

        output = BytesIO()

        book = Workbook(output)
        sheet = book.add_worksheet('test')

        head = book.add_format({
            'bold': 1,
            'align': 'center',
            'valign': 'vcenter',
        })

        text = book.add_format({
            'align': 'center',
            'valign': 'vcenter',
        })

        sheet.set_default_row(18)
        sheet.set_column('A:Z', 10)

        project = Project.objects.get(id=id)
        obj = Forms_Data.objects.filter(ticket__order__project__id=id).order_by('-date')
        serializers = Forms_DataSerializers(obj, many=True).data

        if request.GET['data']:
            filters = json.loads(request.GET['data'])
            filter_valid = {}
            for filter in filters:
                if filter['column'] in filter_valid:
                    filter_valid[filter['column']]['value'].append(filter['value'])
                else:
                    filter_valid[filter['column']] = {'column':filter['column'], 'value':[filter['value'],]}

            for filter_val in filter_valid:
                serializers_data = []
                serializers_arr = []

                for ser_data in serializers:
                    temp = json.loads(ser_data['data'])
                    for i in temp:
                        if i == filter_val:
                            a = 0
                            for j in filter_valid[filter_val]['value']:
                                if temp[i]['value'] == j:
                                    if not(ser_data['id'] in serializers_arr):
                                        serializers_data.append(ser_data)
                                        serializers_arr.append(ser_data['id'])
                serializers = serializers_data


        form = Forms.objects.get(project__id=id, type='registr')
        serializers_form = FormsSerializers(form)
        form_head = json.loads(serializers_form.data['data'])

        row = 0
        col = 0

        for i in form_head:
            sheet.write(row, col, i['text'], head)
            col += 1

        if project.is_pay:
            sheet.write(row, col, 'Оплата', head)
            col += 1
        sheet.write(row, col, 'Дата регистрации', head)
        col = 0
        row += 1

        for i in serializers:
            # serializers = Forms_DataSerializersView(i)
            temp = json.loads(i['data'])
            for j in temp:

                if form_head[int(j)]['type'] == 'choice':
                    temp_local = ''
                    for n in form_head[int(j)]['params']['choice']:
                        if (type(temp[j]['value']) == list):
                            for l_t in temp[j]['value']:
                                if l_t == n['value']:
                                    temp_local += n['text'] + ' '
                        else:
                            if temp[j]['value'] == n['value']:
                                temp_local += n['text'] + ' '
                            # a = n
                    sheet.write(row, col, str(temp_local), text)

                elif form_head[int(j)]['type'] == 'city':

                    if temp[j] and 'name' in temp[j]:
                        city = City.objects.get(id=temp[j]['id'])
                        temp_local = '%s, %s, %s' % (city.region.country.name, city.region.name, city.name)
                        sheet.write(row, col, str(temp_local), text)

                    if temp[j] and 'city' in temp[j] and temp[j]['city'] and 'id' in temp[j]['city']:
                        city = City.objects.get(id=temp[j]['city']['id'])
                        temp_local = '%s, %s, %s' % (city.region.country.name, city.region.name, city.name)
                        sheet.write(row, col, str(temp_local), text)

                else:
                    sheet.write(row, col, str(temp[j]), text)
                col += 1

            if project.is_pay:
                ticket = Tickets.objects.get(uuid = i['ticket'].__str__())
                if ticket.order.state == 0:
                    sheet.write(row, col, 'Нет', text)
                if ticket.order.state == 1:
                    sheet.write(row, col, 'Да', text)
                if ticket.order.state == 2:
                    sheet.write(row, col, 'Отмена', text)

                col += 1
            sheet.write(row, col, i['date'], text)
            col = 0
            row += 1

        book.close()

        output.seek(0)
        response = HttpResponse(output.read(),
                                content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        response['Content-Disposition'] = "attachment; filename=report.xlsx"

        return response

    @list_route(methods=['get', 'post'])
    def option_form(self, request, id=None, ):
        form = Forms.objects.get(project__id = id, type='registr')
        json_data = json.loads(form.data)

        json_options = {
            'column':[],
            'value':{}
        }

        for js in json_data:
            if js['type'] == 'choice':
                json_options['column'].append({
                    'value':js['text'],
                    'key':js['id']
                })

                json_options['value'][js['id']] = js['params']['choice']

        return Response(json_options)

    @list_route(methods=['get', 'post'])
    def chart_project(self, request, id=None):
        project = Project.objects.get(id = id)
        content = {
            'all':{
                'all':Tickets.objects.filter(order__project__id = id).count(),
                'pay':Tickets.objects.filter(order__project__id = id, order__state=1).count(),
                'notpay':Tickets.objects.filter(order__project__id = id, order__state=0).count(),
                'cancel':Tickets.objects.filter(order__project__id = id, order__state=2).count(),
            },
            'buy': {
                'all': Tickets.objects.filter(order__project__id=id, order__state=1).count(),
                'site': Tickets.objects.filter(order__project__id=id, order__state=1, order__payment_method__in=['online', 'free_online']).count(),
                'kiosk': Tickets.objects.filter(order__project__id=id, order__state=1, order__payment_method='kiosk').count(),
                'expobit': Tickets.objects.filter(order__project__id=id, order__state=1, order__payment_method='expobit').count(),
            },
            'active': {
                'all': Tickets.objects.filter(order__project__id=id, order__state=1).count(),
                'active': Tickets.objects.filter(order__project__id=id, order__state=1, state=True).count(),
                'notactive': Tickets.objects.filter(order__project__id=id, order__state=1, state=False).count(),
            }
        }

        return Response(content)

    @list_route(methods=['get', 'post'])
    def chart_project_stat(self, request, id=None):
        project = Project.objects.get(id=id)
        if request.GET['data']:
            data = json.loads(request.GET['data'])
            if(data['data']['category']):
                category = TicketsCategory.objects.filter(project__id=id)
            a = 0
            if data['type'] == 'buy':
                content = {
                    'buy':{
                        'all_site': '',
                        'all_kiosk': {},
                        'all_expobit': {},
                    }
                }
                if data['data']['date_start'] and data['data']['date_end']:
                    start = datetime.datetime.strptime(data['data']['date_start'], "%Y-%m-%d")
                    end = datetime.datetime.strptime(data['data']['date_end'], "%Y-%m-%d")+ timedelta(days=1)
                    date_generated = [start + datetime.timedelta(days=x) for x in range(0, (end - start).days)]

                    content['buy']['all_site'] = Tickets.objects.filter(
                        order__project__id=id, order__state=1,
                        order__payment_method__in=['online', 'free_online'],
                        order__date__gte = start, order__date__lte = end).count()

                    if content['buy']['all_site']>0:
                        content['buy']['site'] = {}
                        for date in date_generated:

                            if (data['data']['category']):
                                content['buy']['site'][date.strftime("%Y-%m-%d")] = []
                                for cat in category :
                                    content['buy']['site'][date.strftime("%Y-%m-%d")].append(Tickets.objects.filter(
                                        order__project__id=id, order__state=1, category=cat,
                                        order__payment_method__in=['online', 'free_online'],
                                        order__date__year=date.year, order__date__month=date.month,
                                        order__date__day=date.day).count())

                            else:
                                content['buy']['site'][date.strftime("%Y-%m-%d")] = Tickets.objects.filter(
                                    order__project__id=id, order__state=1,
                                    order__payment_method__in=['online', 'free_online'],
                                    order__date__year=date.year, order__date__month=date.month, order__date__day=date.day).count()

                    content['buy']['all_kiosk'] = Tickets.objects.filter(
                        order__project__id=id, order__state=1,
                        order__payment_method='kiosk',
                        order__date__gte=start, order__date__lte=end).count()

                    if content['buy']['all_kiosk'] > 0:
                        content['buy']['kiosk'] = {}
                        for date in date_generated:
                            if (data['data']['category']):
                                content['buy']['kiosk'][date.strftime("%Y-%m-%d")] = []
                                for cat in category:
                                    content['buy']['kiosk'][date.strftime("%Y-%m-%d")].append(Tickets.objects.filter(
                                        order__project__id=id, order__state=1, category=cat,
                                        order__payment_method = 'kiosk',
                                        order__date__year=date.year, order__date__month=date.month,
                                        order__date__day=date.day).count())

                            else:
                                content['buy']['kiosk'][date.strftime("%Y-%m-%d")] = Tickets.objects.filter(
                                    order__project__id=id, order__state=1,
                                    order__payment_method='kiosk',
                                    order__date__year=date.year, order__date__month=date.month,
                                    order__date__day=date.day).count()

                    content['buy']['all_expobit'] = Tickets.objects.filter(
                        order__project__id=id, order__state=1,
                        order__payment_method='expobit',
                        order__date__gte=start, order__date__lte=end ).count()

                    if content['buy']['all_expobit'] > 0:
                        content['buy']['expobit'] = {}
                        for date in date_generated:
                            if (data['data']['category']):
                                content['buy']['expobit'][date.strftime("%Y-%m-%d")] = []
                                for cat in category:
                                    content['buy']['expobit'][date.strftime("%Y-%m-%d")].append(Tickets.objects.filter(
                                        order__project__id=id, order__state=1, category=cat,
                                        order__payment_method = 'expobit',
                                        order__date__year=date.year, order__date__month=date.month,
                                        order__date__day=date.day).count())

                            else:
                                content['buy']['expobit'][date.strftime("%Y-%m-%d")] = Tickets.objects.filter(
                                    order__project__id=id, order__state=1,
                                    order__payment_method='expobit',
                                    order__date__year=date.year, order__date__month=date.month,
                                    order__date__day=date.day).count()

                elif data['data']['date_start']:

                    content = {
                        'buy': {
                            'all_site': '',
                            'all_kiosk': {},
                            'all_expobit': {},
                        }
                    }

                    start = datetime.datetime.strptime(data['data']['date_start'], "%Y-%m-%d")
                    end = datetime.datetime.strptime(data['data']['date_start'], "%Y-%m-%d") + timedelta(days=1)
                    content['buy']['all_site'] = Tickets.objects.filter(
                        order__project__id=id, order__state=1,
                        order__payment_method__in=['online', 'free_online'],
                        order__date__gte=start, order__date__lte=end).count()

                    if content['buy']['all_site'] > 0:
                        content['buy']['site'] = {}
                        for hour in range(0, 24):
                            if (data['data']['category']):
                                content['buy']['site'][hour] = []
                                for cat in category:
                                    content['buy']['site'][hour].append(Tickets.objects.filter(
                                        order__project__id=id, order__state=1, category=cat,
                                        order__payment_method__in=['online', 'free_online'],
                                        order__date__gte=start, order__date__lte=end,
                                        order__date__hour=hour).count())
                            else:
                                content['buy']['site'][hour] = Tickets.objects.filter(
                                    order__project__id=id, order__state=1,
                                    order__payment_method__in=['online', 'free_online'],
                                    order__date__gte=start, order__date__lte=end,
                                    order__date__hour=hour).count()

                    content['buy']['all_kiosk'] = Tickets.objects.filter(
                        order__project__id=id, order__state=1,
                        order__payment_method='kiosk',
                        order__date__gte=start, order__date__lte=end).count()

                    if content['buy']['all_kiosk'] > 0:
                        content['buy']['kiosk'] = {}
                        for hour in range(0, 23):
                            if (data['data']['category']):
                                content['buy']['kiosk'][hour] = []
                                for cat in category:
                                    content['buy']['kiosk'][hour].append(Tickets.objects.filter(
                                        order__project__id=id, order__state=1, category=cat,
                                        order__payment_method='kiosk',
                                        order__date__gte=start, order__date__lte=end,
                                        order__date__hour=hour).count())
                            else:
                                content['buy']['kiosk'][hour] = Tickets.objects.filter(
                                    order__project__id=id, order__state=1,
                                    order__payment_method='kiosk',
                                    order__date__gte=start, order__date__lte=end,
                                    order__date__hour=hour).count()

                    content['buy']['all_expobit'] = Tickets.objects.filter(
                        order__project__id=id, order__state=1,
                        order__payment_method='expobit',
                        order__date__gte=start, order__date__lte=end).count()

                    if content['buy']['all_expobit'] > 0:
                        content['buy']['expobit'] = {}
                        for hour in range(0, 23):
                            if (data['data']['category']):
                                content['buy']['expobit'][hour] = []
                                for cat in category:
                                    content['buy']['kiosk'][hour].append(Tickets.objects.filter(
                                        order__project__id=id, order__state=1, category=cat,
                                        order__payment_method='kiosk',
                                        order__date__gte=start, order__date__lte=end,
                                        order__date__hour=hour).count())
                            else:
                                content['buy']['expobit'][hour] = Tickets.objects.filter(
                                    order__project__id=id, order__state=1,
                                    order__payment_method='expobit',
                                    order__date__gte=start, order__date__lte=end,
                                    order__date__hour=hour).count()

                else:
                    if data['data']['type'].__len__()==1:
                        if data['data']['type'][0] == 'site':
                            start = Tickets.objects.filter(
                                order__project__id=id, order__state=1,
                                order__payment_method__in=['online', 'free_online'],).order_by(
                                'order__date').first().order.date
                            end = Tickets.objects.filter(
                                order__project__id=id, order__state=1,
                                order__payment_method__in=['online', 'free_online'],).order_by(
                                'order__date').last().order.date + timedelta(days=2)
                        if data['data']['type'][0] == 'kiosk':
                            start = Tickets.objects.filter(
                                order__project__id=id, order__state=1,
                                order__payment_method='kiosk', ).order_by(
                                'order__date').first().order.date
                            end = Tickets.objects.filter(
                                order__project__id=id, order__state=1,
                                order__payment_method='kiosk', ).order_by(
                                'order__date').last().order.date + timedelta(days=2)
                        if data['data']['type'][0] == 'expobit':
                            start = Tickets.objects.filter(
                                order__project__id=id, order__state=1,
                                order__payment_method='expobit' ).order_by(
                                'order__date').first().order.date
                            end = Tickets.objects.filter(
                                order__project__id=id, order__state=1,
                                order__payment_method='expobit' ).order_by(
                                'order__date').last().order.date + timedelta(days=2)
                    else:
                        start = Tickets.objects.filter(
                            order__project__id=id, order__state=1).order_by('order__date').first().order.date
                        end = Tickets.objects.filter(
                            order__project__id=id, order__state=1).order_by(
                            'order__date').last().order.date + timedelta(days=2)

                    date_generated = [start + datetime.timedelta(days=x) for x in range(0, (end - start).days)]

                    content['buy']['all_site'] = Tickets.objects.filter(
                        order__project__id=id, order__state=1,
                        order__payment_method__in=['online', 'free_online'],
                        order__date__gte=start, order__date__lte=end).count()

                    if content['buy']['all_site'] > 0:
                        content['buy']['site'] = {}
                        for date in date_generated:
                            if (data['data']['category']):
                                content['buy']['site'][date.strftime("%Y-%m-%d")] = []
                                for cat in category:
                                    content['buy']['site'][date.strftime("%Y-%m-%d")].append(Tickets.objects.filter(
                                order__project__id=id, order__state=1, category=cat,
                                order__payment_method__in=['online', 'free_online'],
                                order__date__year=date.year, order__date__month=date.month,
                                order__date__day=date.day).count())
                            else:
                                content['buy']['site'][date.strftime("%Y-%m-%d")] = Tickets.objects.filter(
                                    order__project__id=id, order__state=1,
                                    order__payment_method__in=['online', 'free_online'],
                                    order__date__year=date.year, order__date__month=date.month,
                                    order__date__day=date.day).count()

                    content['buy']['all_kiosk'] = Tickets.objects.filter(
                        order__project__id=id, order__state=1,
                        order__payment_method='kiosk',
                        order__date__gte=start, order__date__lte=end).count()

                    if content['buy']['all_kiosk'] > 0:
                        content['buy']['kiosk'] = {}
                        for date in date_generated:
                            if (data['data']['category']):
                                content['buy']['kiosk'][date.strftime("%Y-%m-%d")] = []
                                for cat in category:
                                    content['buy']['kiosk'][date.strftime("%Y-%m-%d")].append(Tickets.objects.filter(
                                order__project__id=id, order__state=1, category=cat,
                                order__payment_method = 'kiosk',
                                order__date__year=date.year, order__date__month=date.month,
                                order__date__day=date.day).count())
                            else:
                                content['buy']['kiosk'][date.strftime("%Y-%m-%d")] = Tickets.objects.filter(
                                    order__project__id=id, order__state=1,
                                    order__payment_method='kiosk',
                                    order__date__year=date.year, order__date__month=date.month,
                                    order__date__day=date.day).count()

                    content['buy']['all_expobit'] = Tickets.objects.filter(
                        order__project__id=id, order__state=1,
                        order__payment_method='expobit',
                        order__date__gte=start, order__date__lte=end).count()

                    if content['buy']['all_expobit'] > 0:
                        content['buy']['expobit'] = {}
                        for date in date_generated:
                            if (data['data']['category']):
                                content['buy']['expobit'][date.strftime("%Y-%m-%d")] = []
                                for cat in category:
                                    content['buy']['expobit'][date.strftime("%Y-%m-%d")].append(Tickets.objects.filter(
                                order__project__id=id, order__state=1, category=cat,
                                order__payment_method = 'expobit',
                                order__date__year=date.year, order__date__month=date.month,
                                order__date__day=date.day).count())
                            else:
                                content['buy']['expobit'][date.strftime("%Y-%m-%d")] = Tickets.objects.filter(
                                    order__project__id=id, order__state=1,
                                    order__payment_method='expobit',
                                    order__date__year=date.year, order__date__month=date.month,
                                    order__date__day=date.day).count()
            if data['type'] == 'activate':
                content = {
                    'activate': {
                        'all_site': '',
                        'all_kiosk': {},
                        'all_expobit': {},
                    }
                }
                if data['data']['date_start'] and data['data']['date_end']:
                    start = datetime.datetime.strptime(data['data']['date_start'], "%Y-%m-%d")
                    end = datetime.datetime.strptime(data['data']['date_end'], "%Y-%m-%d")+ timedelta(days=1)
                    date_generated = [start + datetime.timedelta(days=x) for x in range(0, (end - start).days)]

                    content['activate']['all_site'] = Tickets.objects.filter(
                        order__project__id=id, state=True,
                        order__payment_method__in=['online', 'free_online'],
                        date_activate__gte = start, date_activate__lte = end).count()

                    if content['activate']['all_site']>0:
                        content['activate']['site'] = {}
                        for date in date_generated:
                            if (data['data']['category']):
                                content['activate']['site'][date.strftime("%Y-%m-%d")] = []
                                for cat in category:
                                    content['activate']['site'][date.strftime("%Y-%m-%d")].append(Tickets.objects.filter(
                                order__project__id=id, state=True, category=cat,
                                order__payment_method__in=['online', 'free_online'],
                                date_activate__year=date.year, date_activate__month=date.month, date_activate__day=date.day).count())
                            else:
                                content['activate']['site'][date.strftime("%Y-%m-%d")] = Tickets.objects.filter(
                                order__project__id=id, state=True,
                                order__payment_method__in=['online', 'free_online'],
                                date_activate__year=date.year, date_activate__month=date.month, date_activate__day=date.day).count()

                    content['activate']['all_kiosk'] = Tickets.objects.filter(
                        order__project__id=id, state=True,
                        order__payment_method='kiosk',
                        date_activate__gte=start, date_activate__lte=end).count()

                    if content['activate']['all_kiosk'] > 0:
                        content['activate']['kiosk'] = {}
                        for date in date_generated:
                            if (data['data']['category']):
                                content['activate']['kiosk'][date.strftime("%Y-%m-%d")] = []
                                for cat in category:
                                    content['activate']['kiosk'][date.strftime("%Y-%m-%d")].append(Tickets.objects.filter(
                                order__project__id=id, state=True, category=cat,
                                order__payment_method = 'kiosk',
                                date_activate__year=date.year, date_activate__month=date.month, order__date__day=date.day).count())
                            else:
                                content['activate']['kiosk'][date.strftime("%Y-%m-%d")] = Tickets.objects.filter(
                                    order__project__id=id, state=True,
                                    order__payment_method='kiosk',
                                    date_activate__year=date.year, date_activate__month=date.month,
                                    date_activate__day=date.day).count()

                    content['activate']['all_expobit'] = Tickets.objects.filter(
                        order__project__id=id, state=True,
                        order__payment_method='expobit',
                        date_activate__gte=start, date_activate__lte=end ).count()

                    if content['activate']['all_expobit'] > 0:
                        content['activate']['expobit'] = {}
                        for date in date_generated:
                            if (data['data']['category']):
                                content['activate']['expobit'][date.strftime("%Y-%m-%d")] = []
                                for cat in category:
                                    content['activate']['expobit'][date.strftime("%Y-%m-%d")].append(Tickets.objects.filter(
                                order__project__id=id, state=True, category=cat,
                                order__payment_method = 'expobit',
                                date_activate__year=date.year, date_activate__month=date.month, order__date__day=date.day).count())
                            else:
                                content['activate']['expobit'][date.strftime("%Y-%m-%d")] = Tickets.objects.filter(
                                    order__project__id=id, state=True,
                                    order__payment_method='expobit',
                                    date_activate__year=date.year, date_activate__month=date.month,
                                    date_activate__day=date.day).count()

                elif data['data']['date_start']:
                    content = {
                        'activate': {
                            'all_site': '',
                            'all_kiosk': {},
                            'all_expobit': {},
                        }
                    }

                    start = datetime.datetime.strptime(data['data']['date_start'], "%Y-%m-%d")
                    end = datetime.datetime.strptime(data['data']['date_start'], "%Y-%m-%d") + timedelta(days=1)
                    content['activate']['all_site'] = Tickets.objects.filter(
                        order__project__id=id, state=True,
                        order__payment_method__in=['online', 'free_online'],
                        date_activate__gte=start, date_activate__lte=end).count()

                    if content['activate']['all_site'] > 0 and 'site' in data['data']['type']:
                        content['activate']['site'] = {}
                        for hour in range(0, 24):
                            if (data['data']['category']):
                                content['activate']['site'][hour] = []
                                for cat in category:
                                    content['activate']['site'][hour].append( Tickets.objects.filter(
                                order__project__id=id, state=True, category=cat,
                                order__payment_method__in=['online', 'free_online'],
                                date_activate__gte=start, date_activate__lte=end,
                                date_activate__hour=hour).count())
                            else:
                                content['activate']['site'][hour] = Tickets.objects.filter(
                                    order__project__id=id, state=True,
                                    order__payment_method__in=['online', 'free_online'],
                                    date_activate__gte=start, date_activate__lte=end,
                                    date_activate__hour=hour).count()

                    content['activate']['all_kiosk'] = Tickets.objects.filter(
                        order__project__id=id, state=True,
                        order__payment_method='kiosk',
                        date_activate__gte=start, date_activate__lte=end).count()

                    if content['activate']['all_kiosk'] > 0 and 'kiosk' in data['data']['type']:
                        content['activate']['kiosk'] = {}
                        for hour in range(0, 23):
                            if (data['data']['category']):
                                content['activate']['kiosk'][hour] = []
                                for cat in category:
                                    content['activate']['kiosk'][hour].append( Tickets.objects.filter(
                                order__project__id=id, state=True, category=cat,
                                order__payment_method='kiosk',
                                date_activate__gte=start, date_activate__lte=end,
                                date_activate__hour=hour).count())
                            else:
                                content['activate']['kiosk'][hour] = Tickets.objects.filter(
                                    order__project__id=id, state=True,
                                    order__payment_method='kiosk',
                                    date_activate__gte=start, date_activate__lte=end,
                                    date_activate__hour=hour).count()

                    content['activate']['all_expobit'] = Tickets.objects.filter(
                        order__project__id=id, state=True,
                        order__payment_method='expobit',
                        date_activate__gte=start, date_activate__lte=end).count()

                    if content['activate']['all_expobit'] > 0 and 'expobit' in data['data']['type']:
                        content['activate']['expobit'] = {}
                        for hour in range(0, 23):
                            if (data['data']['category']):
                                content['activate']['expobit'][hour] = []
                                for cat in category:
                                    content['activate']['expobit'][hour].append( Tickets.objects.filter(
                                order__project__id=id, state=True, category=cat,
                                order__payment_method='expobit',
                                date_activate__gte=start, date_activate__lte=end,
                                date_activate__hour=hour).count())
                            else:
                                content['activate']['expobit'][hour] = Tickets.objects.filter(
                                    order__project__id=id, state=True,
                                    order__payment_method='expobit',
                                    date_activate__gte=start, date_activate__lte=end,
                                    date_activate__hour=hour).count()

                else:
                    if data['data']['type'].__len__()==1:
                        if data['data']['type'][0] == 'site':
                            start = Tickets.objects.filter(
                                order__project__id=id, state=True,
                                order__payment_method__in=['online', 'free_online'],).order_by(
                                'date_activate').first().date_activate
                            end = Tickets.objects.filter(
                                order__project__id=id, state=True,
                                order__payment_method__in=['online', 'free_online'],).order_by(
                                'date_activate').last().date_activate + timedelta(days=2)
                        if data['data']['type'][0] == 'kiosk':
                            start = Tickets.objects.filter(
                                order__project__id=id, state=True,
                                order__payment_method='kiosk', ).order_by(
                                'date_activate').first().date_activate
                            end = Tickets.objects.filter(
                                order__project__id=id, state=True,
                                order__payment_method='kiosk', ).order_by(
                                'date_activate').last().date_activate + timedelta(days=2)
                        if data['data']['type'][0] == 'expobit':
                            start = Tickets.objects.filter(
                                order__project__id=id, state=True,
                                order__payment_method='expobit' ).order_by(
                                'date_activate').first().date_activate
                            end = Tickets.objects.filter(
                                order__project__id=id, state=True,
                                order__payment_method='expobit' ).order_by(
                                'date_activate').last().date_activate + timedelta(days=2)
                    else:
                        start = Tickets.objects.filter(
                            order__project__id=id, state=True).order_by('date_activate').first().date_activate
                        end = Tickets.objects.filter(
                            order__project__id=id, state=True).order_by(
                            'date_activate').last().date_activate + timedelta(days=2)

                    date_generated = [start + datetime.timedelta(days=x) for x in range(0, (end - start).days)]

                    content['activate']['all_site'] = Tickets.objects.filter(
                        order__project__id=id, state=True,
                        order__payment_method__in=['online', 'free_online'],
                        date_activate__gte=start, date_activate__lte=end).count()

                    if content['activate']['all_site'] > 0 and 'site' in data['data']['type']:
                        content['activate']['site'] = {}
                        for date in date_generated:
                            if (data['data']['category']):
                                content['activate']['site'][date.strftime("%Y-%m-%d")] = []
                                for cat in category:
                                    content['activate']['site'][date.strftime("%Y-%m-%d")].append( Tickets.objects.filter(
                                        order__project__id=id, state=True,
                                        order__payment_method__in=['online', 'free_online'], category=cat,
                                        date_activate__year=date.year, date_activate__month=date.month,
                                        date_activate__day=date.day).count())
                            else:
                                content['activate']['site'][date.strftime("%Y-%m-%d")] = Tickets.objects.filter(
                                    order__project__id=id, state=True,
                                    order__payment_method__in=['online', 'free_online'],
                                    date_activate__year=date.year, date_activate__month=date.month,
                                    date_activate__day=date.day).count()

                    content['activate']['all_kiosk'] = Tickets.objects.filter(
                        order__project__id=id, state=True,
                        order__payment_method='kiosk',
                        date_activate__gte=start, date_activate__lte=end).count()

                    if content['activate']['all_kiosk'] > 0 and 'kiosk' in data['data']['type']:
                        content['activate']['kiosk'] = {}
                        for date in date_generated:
                            if (data['data']['category']):
                                content['activate']['kiosk'][date.strftime("%Y-%m-%d")] = []
                                for cat in category:
                                    content['activate']['kiosk'][date.strftime("%Y-%m-%d")].append( Tickets.objects.filter(
                                        order__project__id=id, state=True,
                                        order__payment_method='kiosk', category=cat,
                                        date_activate__year=date.year, date_activate__month=date.month,
                                        date_activate__day=date.day).count())
                            else:
                                content['activate']['kiosk'][date.strftime("%Y-%m-%d")] = Tickets.objects.filter(
                                    order__project__id=id, state=True,
                                    order__payment_method='kiosk',
                                    date_activate__year=date.year, date_activate__month=date.month,
                                    date_activate__day=date.day).count()

                    content['activate']['all_expobit'] = Tickets.objects.filter(
                        order__project__id=id, state=True,
                        order__payment_method='expobit',
                        date_activate__gte=start, date_activate__lte=end).count()

                    if content['activate']['all_expobit'] > 0 and 'expobit' in data['data']['type']:
                        content['activate']['expobit'] = {}
                        for date in date_generated:
                            if (data['data']['category']):
                                content['activate']['expobit'][date.strftime("%Y-%m-%d")] = []
                                for cat in category:
                                    content['activate']['expobit'][date.strftime("%Y-%m-%d")].append( Tickets.objects.filter(
                                        order__project__id=id, state=True,
                                        order__payment_method='expobit', category=cat,
                                        date_activate__year=date.year, date_activate__month=date.month,
                                        date_activate__day=date.day).count())
                            else:
                                content['activate']['expobit'][date.strftime("%Y-%m-%d")] = Tickets.objects.filter(
                                    order__project__id=id, state=True,
                                    order__payment_method='expobit',
                                    date_activate__year=date.year, date_activate__month=date.month,
                                    date_activate__day=date.day).count()

            if data['type'] =='form':
                form_options = json.loads(Forms.objects.filter(project__id=id, type='registr')[0].data)[int(data['data']['type'])]['params']['choice']
                form = Forms_Data.objects.filter(ticket__category__project__id=id)

                result = {}

                if data['data']['date_start'] and data['data']['date_end']:
                    start = datetime.datetime.strptime(data['data']['date_start'], "%Y-%m-%d")
                    end = datetime.datetime.strptime(data['data']['date_end'], "%Y-%m-%d") + timedelta(days=1)
                    date_generated = [start + datetime.timedelta(days=x) for x in range(0, (end - start).days)]

                    form = form.filter(
                        ticket__order__state=1, ticket__order__date__gte=start, ticket__order__date__lte=end)

                    if data['data']['category']:
                        for date in date_generated:
                            form_temp = form.filter(ticket__order__date__year=date.year, ticket__order__date__month=date.month,
                                                    ticket__order__date__day=date.day)
                            if(form_temp.count())>0:
                                temp_count = form_temp.count()
                                result[date.strftime("%Y-%m-%d")] = {}

                                for j in form_options:
                                    result[date.strftime("%Y-%m-%d")][j['value']] = 0
                                result[date.strftime("%Y-%m-%d")]['other'] = 0
                                result[date.strftime("%Y-%m-%d")]['all'] = form_temp.count()

                                for one_form in form_temp:
                                    one_form_data=json.loads(one_form.data)

                                    for k in form_options:
                                        if one_form_data[data['data']['type']]['value'] == k['value']:
                                            result[date.strftime("%Y-%m-%d")][k['value']]+=1
                                            temp_count-=1
                                result[date.strftime("%Y-%m-%d")]['other'] = temp_count
                    else:
                        temp_count = form.count()
                        result["Всего"] = {}

                        for j in form_options:
                            result["Всего"][j['value']] = 0
                        result["Всего"]['other'] = 0
                        result["Всего"]['all'] = form.count()

                        for one_form in form:
                            one_form_data = json.loads(one_form.data)

                            for k in form_options:
                                if one_form_data[data['data']['type']]['value'] == k['value']:
                                    result["Всего"][k['value']] += 1
                                    temp_count -= 1
                                result["Всего"]['other'] = temp_count

                elif data['data']['date_start']:

                    start = datetime.datetime.strptime(data['data']['date_start'], "%Y-%m-%d")
                    end = datetime.datetime.strptime(data['data']['date_start'], "%Y-%m-%d") + timedelta(days=1)
                    form = form.filter(ticket__order__state=1, ticket__order__date__gte=start, ticket__order__date__lte=end)
                    if data['data']['category']:
                        for hour in range(0, 24):
                            form_temp = form.filter(ticket__order__date__gte=start, ticket__order__date__lte=end,
                                                    ticket__order__date__hour=hour)

                            if (form_temp.count()) > 0:
                                temp_count = form_temp.count()
                                result[hour] = {}

                                for j in form_options:
                                    result[hour][j['value']] = 0
                                result[hour]['other'] = 0

                                for one_form in form_temp:
                                    one_form_data = json.loads(one_form.data)

                                    for k in form_options:
                                        if one_form_data[data['data']['type']]['value'] == k['value']:
                                            result[hour][k['value']] += 1
                                            temp_count -= 1
                                result[hour]['other'] = temp_count
                    else:
                        temp_count = form.count()
                        result["Всего"] = {}

                        for j in form_options:
                            result["Всего"][j['value']] = 0
                        result["Всего"]['other'] = 0
                        result["Всего"]['all'] = form.count()

                        for one_form in form:
                            one_form_data = json.loads(one_form.data)

                            for k in form_options:
                                if one_form_data[data['data']['type']]['value'] == k['value']:
                                    result["Всего"][k['value']] += 1
                                    temp_count -= 1
                                result["Всего"]['other'] = temp_count

                else:
                    form = form.filter(ticket__order__state=1)
                    start = form.order_by('ticket__order__date').first().ticket.order.date
                    end = form.order_by('ticket__order__date').last().ticket.order.date + timedelta(days=2)
                    date_generated = [start + datetime.timedelta(days=x) for x in range(0, (end - start).days)]

                    if data['data']['category']:
                        for date in date_generated:
                            form_temp = form.filter(ticket__order__date__year=date.year, ticket__order__date__month=date.month,
                                                    ticket__order__date__day=date.day)

                            if(form_temp.count())>0:
                                temp_count = form_temp.count()
                                result[date.strftime("%Y-%m-%d")] = {}

                                for j in form_options:
                                    result[date.strftime("%Y-%m-%d")][j['value']] = 0
                                result[date.strftime("%Y-%m-%d")]['other'] = 0

                                for one_form in form_temp:
                                    one_form_data=json.loads(one_form.data)

                                    for k in form_options:
                                        if one_form_data[data['data']['type']]['value'] == k['value']:
                                            result[date.strftime("%Y-%m-%d")][k['value']]+=1
                                            temp_count-=1
                                result[date.strftime("%Y-%m-%d")]['other'] = temp_count

                    else:
                        temp_count = form.count()
                        result["Всего"] = {}

                        for j in form_options:
                            result["Всего"][j['value']] = 0
                        result["Всего"]['other'] = 0
                        result["Всего"]['all'] = form.count()

                        for one_form in form:
                            one_form_data = json.loads(one_form.data)

                            for k in form_options:
                                if one_form_data[data['data']['type']]['value'] == k['value']:
                                    result["Всего"][k['value']] += 1
                                    temp_count -= 1
                                result["Всего"]['other'] = temp_count
                return Response({'data': result, 'category':form_options})

        return Response({'data':content, 'category': (TicketsCategorySerializer(category, many = True).data if  data['data']['category'] else None)})


    @list_route(methods=['get'])
    def events(self, request):
        events = Events.objects.exclude(project__status='end')
        content = EventsSerializerView(events, many=True).data
        return Response(data=content)


    @list_route(methods=['get'])
    def statistics(self, request, ):

        projects = Project.objects.all()

        if 'data' in request.GET:
            serverData = json.loads(request.GET['data'])

            if (serverData['sort']['field'] and serverData['sort']['type']):

                if serverData['sort']['type'] == 'desc':
                    serverData['sort']['field'] = '-' + serverData['sort']['field']

                projects = project.order_by(serverData['sort']['field'])

            if 'select' in serverData and serverData['select'].keys().__len__():

                for select in serverData['select']:
                    if type(serverData['select'][select])==list and select.find('__in')<0:

                        serverData['select'][select + '__in'] = serverData['select'][select]
                        del serverData['select'][select]

                projects = projects.filter(**serverData['select'])

            tickets={'activate':[], 'visitation':[], 'base_data':[]}

            for project in projects:
                ticket = Tickets.objects.filter(category__project_id = project.id).exclude(order__state=3).exclude(order__state=0)

                if ticket.count():

                    order_pay = Order.objects.filter(state=1, project__id=project.id)
                    order_summ = 0
                    for order in order_pay:
                        order_summ += order.amount

                    tickets['activate'].append({
                        'name':project.name,
                        'id':project.id,
                        'activate': ticket.filter(state = True, status=True, order__state=1).count(),
                        'noactivate': ticket.filter(state = False, status=True, order__state=1).count(),
                        'reset': ticket.filter(status=False,  order__state=2).count(),
                        'amount_order' : order_summ
                    })

                    tickets['base_data'] .append([{
                            'type': 'money_head',
                            'label': 'Всего',
                            'count': ticket.count(),
                        },{
                            'type':'money_row',
                            'label': 'Оплачено',
                            'count': ticket.count() - ticket.filter(status=False, order__state=2).count(),
                            'percent':str(round((ticket.count() - ticket.filter(status=False, order__state=2).count())*100/ticket.count(), 2)) + ' %',
                        }, {
                            'type': 'money_row',
                            'label': 'Отменено',
                            'count': ticket.filter(status=False).count(),
                            'percent': str(round(ticket.filter(status=False, order__state=2).count() * 100 / ticket.count(),2)) + ' %',

                        }, {
                            'type': 'activate_head',
                            'label': 'Всего оплачено',
                            'count': ticket.count() - ticket.filter(status=False, order__state=2).count(),

                        },{
                            'type': 'activate_row',
                            'label': 'Активировано',
                            'count': ticket.filter(state = True, status=True, order__state=1).count(),
                            'percent':str(round(ticket.filter(state = True, status=True, order__state=1).count()*100/(ticket.count() - ticket.filter(status=False, order__state=2).count()), 2)) + ' %',
                        },{
                            'type': 'activate_row',
                            'label': 'Не активировано',
                            'count': ticket.filter(state = False, status=True, order__state=1).count(),
                            'percent': str(round(ticket.filter(state = False, status=True, order__state=1).count() * 100 / (ticket.count() - ticket.filter(status=False, order__state=2).count()), 2)) + ' %',

                        }])

            return Response(data=tickets)
        return Response(data={})


class SbrList(viewsets.ModelViewSet):

    queryset = SbrAccount.objects.all()
    serializer_class = SbrAccountSerializer
    authentication_class = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def list(self, request, id=None, pk=None):
        if(request.auth):
            data = SbrAccount.objects.all()
            content = SbrAccountSerializerList(data, many=True).data
            return Response(content)
        else:
            b=3

    def retrieve(self, request, id=None, pk=None):
        if (request.auth):
            if pk == 'relate':
                sbr = Project.objects.get(id = id).sbr
                content = SbrAccountSerializer(sbr).data
                return Response(content)

            a = 0
        else:
            b = 3
        return Response(a)
    @list_route(methods=['post'])
    def add(self, request, id=None):
        if (request.auth):
            sbr = request.POST

            if 'old' in sbr:
                project = Project.objects.get(id=id)
                project.sbr = SbrAccount.objects.get(id=sbr['old'])
                project.save()
                obj = SbrAccountSerializer(SbrAccount.objects.get(id=sbr['old'])).data

                return Response(obj)
            else:
                if 'id' in sbr:
                    obj = SbrAccount.objects.get(id =sbr['id'] )
                else:
                    obj = SbrAccount()


                serializer = SbrAccountSerializer(obj, data=sbr)
                if serializer.is_valid():
                    serializer.save()

                project = Project.objects.get(id = id)
                project.sbr = SbrAccount.objects.get(id = serializer.data['id'])
                project.save()

                obj = SbrAccountSerializer(SbrAccount.objects.get(id = serializer.data['id'])).data


                return Response(obj)

        else:
            return Response({})
class FormsList(viewsets.ModelViewSet):
    queryset = Forms.objects.all()
    serializer_class = FormsSerializers
    authentication_class = (JSONWebTokenAuthentication,)
    # permission_classes = (IsAuthenticatedOrReadOnly,)

    def list(self, request, id=None, pk=None):
        # if(request.auth):
        data =Forms.objects.filter(project__id = id)

        if request.GET:
            filter = json.loads(request.GET['data'])
            data = data.filter(**(filter))


        content = FormsSerializers(data, many=True).data
        return Response(content)

    def retrieve(self, request, id=None, pk=None):

        if pk == 'relate':
            sbr = Project.objects.get(id = id).sbr
            content = SbrAccountSerializer(sbr).data
            return Response(content)

        return Response(a)

    @list_route(methods=['get'])
    def options(self, request, id=None):
        data = {}

        type = Forms.TYPE
        data['type'] = []
        for t in type:
            data['type'].append({
                'key':t[0],
                'value':t[1],
            })

        type = Forms.TYPE_QUESTIONS
        data['type_questions'] = []
        for t in type:
            data['type_questions'].append({
                'key':t[0],
                'value':t[1],
            })


        # sewrserrwrwerwerwerwerwewr

        return Response(data)

    @list_route(methods=['get'])
    def get(self, request, id=None):
        data = {}
        if 'data' in request.GET:
            data = Forms.objects.filter(**(json.loads(request.GET['data'])))
            if data.count()>0:
                content = FormsSerializers(data[0]).data
                return Response(content)

        return Response({})

    @list_route(methods=['get'])
    def list_all_project(self, request, id=None):
        filter = json.loads(request.GET['data'])
        data = Forms.objects.filter(**(filter))

        content = FormsSerializers(data, many=True).data
        return Response(content)


    @list_route(methods=['get'])
    def ref_book(self, request, id=None):
        data = {}

        type = TypePost.objects.all()
        data['post'] = []
        for t in type:
            data['post'].append({
                'key':t.id,
                'value':t.name,
            })

        cursor = connections['default'].cursor()

        if 'data' in request.GET:
            text = json.loads(request.GET['data'])
            cursor.execute("SELECT id, subcategories FROM public.projects_activities WHERE  subcategories ILIKE '%" +text['text']+"%'  ORDER BY subcategories LIMIT 20" )
        else:
            cursor.execute("SELECT id, subcategories FROM public.projects_activities ORDER BY subcategories LIMIT 20")

        data['activities'] = []
        for row in cursor.fetchall():
            temp = {}
            temp['key'] = str(row[0])
            temp['value'] = row[1]
            data['activities'].append(temp)



        return Response(data)

    @detail_route(methods=['get', 'post'])
    def add(self, request, id=None, pk=None):
        # aaa = request.data
        #
        # data = json.loads(aaa['data'])
        data = request.data
        data['data'] = json.dumps(data['questions'])

        try:

            if data['type'] == 'registr':
                forms_reg = Forms.objects.filter(project__id=id, type='registr')
                if pk.isnumeric():
                    forms_reg = forms_reg.exclude(id=pk)

                if forms_reg.count()>0:
                    raise ValueError('Нельзя создать 2 формы регистрации')


            try:
                if pk.isnumeric():
                    obj = Forms.objects.get(id=pk)
                else:
                    obj = Forms()
            except Forms.DoesNotExist:
                obj = Forms()

            serializer = FormsSerializers(obj, data)
            if serializer.is_valid():
                serializer.save()

            project = Project.objects.get(id = id)
            project.forms_set.add(serializer.data['id'])

            return Response(serializer.data)

        except Exception as e:
            return Response({'type': 'error', 'message': 'Дублирование анкеты регистрации'}, status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['get'])
    def delete(self, request, id=None, pk=None):

        try:
            obj = Forms.objects.get(id=pk)
            obj.delete()

        except Forms.DoesNotExist:
            pass

        return Response()

    @detail_route(methods=['get'])
    def form_data(self, request, id=None, pk=None):

        try:
            obj = Forms_Data.objects.filter(ticket__order__number=pk)
            serializers = Forms_DataSerializers(obj, many=True)

            order = Order.objects.get(number=pk)

            if order.payment_method == 'expobit':
                form = Forms.objects.get(project__id=id, type='registr_eb_pay')
            else:
                form = Forms.objects.get(project__id=id, type='registr')

            # form = Forms.objects.get(project__id=order.project.id, type = 'registr')
            serializers_form = FormsSerializers(form)


            return Response({'form_data':serializers.data, 'form':serializers_form.data})

        except Forms.DoesNotExist:
            pass

        return Response()

class NewsList(viewsets.ModelViewSet):

    queryset = News.objects.all()
    serializer_class = NewsSerializer
    authentication_class = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def list(self, request, id=None, pk=None):
        try:
            project = Project.objects.get(id = id)
            news = News.objects.filter(project=project)
            content = NewsSerializer(news, many=True).data
            return Response(content)
        except:
            return Response({})


    @detail_route(methods=['get'])
    def data(self, request, id=None, pk=None):
        if pk:
            if pk != 'new':
                try:
                    news = News.objects.get(id=pk)
                    content = NewsSerializer(news).data
                    return Response(content)
                except:
                    pass
        content = NewsSerializer(None, many=True).data
        return Response(content)

    @detail_route(methods=['post'], permission_classes=[IsAuthenticated])
    def add(self, request, id=None, pk=None):

        temp = request.POST.copy()
        if pk=='new':
            obj = News()
        else:
            obj = News.objects.get(id = pk)

        if obj:
            temp.update({'project':id})
            if 'logo' in temp:
                if temp['logo'] == 'delete':
                    obj.logo = None
                temp.pop('logo')

            serializer = NewsSerializer(obj, data=temp)
            serializer._args[0].logo = obj.logo
            if serializer.is_valid():
                serializer.validated_data
                if request.FILES != {}:
                    temp.update(request.FILES)
                    serializer = NewsSerializer(obj, data=temp)
                    if serializer.is_valid():
                        serializer.validated_data['logo']

                obj = serializer.save()
                serializer = NewsSerializer(obj)
                data = serializer.data

                return Response(data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['post'])
    def delete(self, request, id=None, pk=None):
        obj = News.objects.get(id=pk)
        obj.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)

    @list_route(methods=['get'])
    def export(self, request, id=None):
        from django.http import HttpResponse
        try:
            from StringIO import StringIO
        except ImportError:
            from io import BytesIO

        output = BytesIO()
        book = Workbook(output)
        sheet = book.add_worksheet('news')

        head = book.add_format({
            'bold': 1,
            'align': 'center',
            'valign': 'vcenter',
        })

        text = book.add_format({
            'align': 'center',
            'valign': 'vcenter',
        })

        sheet.set_default_row(18)
        sheet.set_column('A:Z', 25)

        news = News.objects.filter(project__id=id)
        news_json = NewsListSerializer(news, many=True).data

        fields = News._meta.get_fields()

        row = 0
        col = 0
        for j in fields:
            if not j.name in ['logo', 'project']:
                sheet.write(row, col, j.verbose_name, head)
                col += 1
        row += 1
        col = 0

        for i in news_json:
            for j in fields:
                if not j.name in ['logo', 'project']:

                    if (j.name in i.keys()):
                        sheet.write(row, col, i[j.name], text)
                    col += 1
            row += 1
            col = 0

        book.close()

        output.seek(0)
        response = HttpResponse(output.read(),
                                content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        response['Content-Disposition'] = "attachment; filename=news.xlsx"

        return response

class EventsList(viewsets.ModelViewSet):

    queryset = Events.objects.all()
    serializer_class = EventsSerializer
    authentication_class = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticatedOrReadOnly,)
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def list(self, request, id=None, pk=None):
        project = Project.objects.get(id = id)
        news = Events.objects.filter(project=project)
        content = EventsSerializer(news, many=True).data
        # return Response(content)
        return JsonResponse(data=content, safe=False)

    def retrieve(self, request, id=None, pk=None):
        if pk:
            if pk != 'new':
                try:
                    news = Events.objects.get(id=pk)
                    data = EventsSerializer(news).data
                    return Response(data)
                except:
                    pass
        data = EventsSerializer(None).data
        data['sites'] = []
        return Response(data)

    @list_route(methods=['get'])
    def report(self, request, id=None):
        from django.http import HttpResponse
        try:
            from StringIO import StringIO
        except ImportError:
            from io import BytesIO

        output = BytesIO()
        book = Workbook(output)
        sheet = book.add_worksheet('test')

        head = book.add_format({
            'bold': 1,
            'align': 'center',
            'valign': 'vcenter',
        })

        text = book.add_format({
            'align': 'center',
            'valign': 'vcenter',
        })

        sheet.set_default_row(18)
        sheet.set_column('A:Z', 25)

        events = Events.objects.filter(project__id=id)
        events_json = EventsSerializerReport(events, many=True).data

        fields = Events._meta.get_fields()

        row = 0
        col = 0

        for j in fields:
            if not j.name in ['logo', 'project', 'date_update']:
                sheet.write(row, col, j.verbose_name, head)
                col += 1
        row += 1
        col = 0

        for i in events_json:
            for j in fields:
                if not j.name in ['logo','project', 'date_update']:
                    if Events._meta.get_field(j.name).get_internal_type()=='ManyToManyField':
                        temp = ''
                        ev = Events.objects.get(id=i['id'])
                        for site in ev.sites.all():
                            temp+=(site.name +', ')

                        sheet.write(row, col, temp, text)
                    else:
                        if(j.name in i.keys()):
                            aaa =  Events._meta.get_field(j.name).get_internal_type()
                            sheet.write(row, col, i[j.name], text)
                    col+=1
            row+=1
            col=0

        book.close()

        output.seek(0)
        response = HttpResponse(output.read(),
                                content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        response['Content-Disposition'] = "attachment; filename=events.xlsx"

        return response


    @list_route(methods=['get'])
    def options(self, request, id=None):
        sites = Sites.objects.all()
        data = {}
        data['sites'] = []
        for row in sites:
            temp = {}
            temp['id'] = row.id
            temp['name'] = row.name
            data['sites'].append(temp)
        return Response(data)

    @detail_route(methods=['get'])
    def data(self, request, id=None, pk=None):

        sites = Sites.objects.all()

        data = {}
        data['sites'] = []
        for row in sites:
            temp = {}
            temp['id'] = row.id
            temp['name'] = row.name
            data['sites'].append(temp)
        if pk:
            if pk != 'new':
                try:
                    news = Events.objects.get(id=pk)
                    data['formData'] = EventsSerializer(news).data
                    return Response(data)
                except:
                    pass
        data['formData'] = EventsSerializer(None).data

        data['formData']['sites'] = []
        return Response(data)

    @detail_route(methods=['post'])
    def add(self, request, id=None, pk=None):

        temp = request.POST.copy()
        if pk=='new':
            obj = Events()
        else:
            obj = Events.objects.get(id = pk)

        if obj:
            temp.update({'project':id})

            if 'logo' in temp:
                if temp['logo'] == 'delete':
                    obj.logo = None
                temp.pop('logo')

            serializer = EventsSerializer(obj, data=temp)
            serializer._args[0].logo = obj.logo
            if serializer.is_valid():
                serializer.validated_data
                # obj = serializer.save()
                if request.FILES != {}:
                    temp.update(request.FILES)
                    serializer = EventsSerializer(obj, data=temp)

                    if serializer.is_valid():
                        serializer.validated_data['logo']
                        # serializer_file.save()
                # else:

                obj = serializer.save()
                obj.sites.clear()
                if 'sites' in temp:
                    if  temp['sites']:
                        Sites.objects.filter(id__in=temp['sites'].split(','))
                        for c in Sites.objects.filter(id__in=temp['sites'].split(',')):
                            obj.sites.add(c)

                serializer = EventsSerializerView(obj)
                data = {}
                data = serializer.data
                # data['active_button'] = 'view'

                return Response(data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['post'])
    def delete(self, request, id=None, pk=None):
        obj = Events.objects.get(id=pk)
        obj.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)

class ExponentsList(viewsets.ModelViewSet):

    queryset = ExponentsDetail.objects.all()
    serializer_class = ExponentsDetailSerializer
    authentication_class = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticatedOrReadOnly,)


    def list(self, request, id=None, pk=None):
        try:
            project = Project.objects.get(id = id)
            data = ExponentsDetail.objects.filter(project=project)
            content = ExponentsDetailListSerializer(data, many=True).data
            return Response(content)
        except:
            return Response({})


    @list_route(methods=['get'])
    def options(self, request, id=None):
        content = {}
        for field in ExponentsDetail._meta.get_fields():
            if field.choices:
                content[field.name] = []
                for choice in field.choices:
                    content[field.name].append({
                        'value': choice[0],
                        'label': choice[1],
                    })

        return Response(content)


    def retrieve(self, request, id=None, pk=None):
        data = ExponentsDetail.objects.get(id=pk)

        content = ExponentsDetailSerializer(data)
        a = content.data
        return Response(a)

    @detail_route(methods=['post'])
    def add(self, request, id=None, pk=None):

        temp = request.POST.copy()
        temp['project'] = id

        if pk == 'new':
            obj = ExponentsDetail()
        else:
            obj = ExponentsDetail.objects.get(id=pk)
        if obj:
            if 'logo' in temp:
                if temp['logo'] == 'null':
                    obj.logo = None
                    temp.pop('logo')

            serializer = ExponentsDetailSerializer(obj, data=temp)
            serializer._args[0].logo = obj.logo
            if serializer.is_valid():
                serializer.validated_data

                if request.FILES != {}:

                    temp['logo'] = request.FILES['logo']
                    serializer = ExponentsDetailSerializer(obj, data=temp)
                    if serializer.is_valid():
                        serializer.validated_data['logo']
                obj = serializer.save()
                obj = ExponentsDetailSerializer(obj)
                return Response(obj.data)

            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'])
    def download(self, request, id=None):
        project = Project.objects.get(id=id)
        cursor = connections['default'].cursor()
        bbb = project.p_expobit
        cursor.execute("SELECT id, client_id, project_id FROM public.workers_membership WHERE project_id = '%s'" % (
            project.p_expobit))

        client = []
        for row in cursor.fetchall():
            client.append(row[1])

        if client:
            sql_val = json.dumps(client).replace('[','(').replace(']',')')

            cursor.execute("SELECT id, name, address FROM public.clients_client WHERE id IN %s" % (
                sql_val))

            columns = ('id_client', 'name', 'address')
            results = []
            for row in cursor.fetchall():
                try:
                    Exponents.objects.get(id_client=row[0])
                    try:
                        Exponents.objects.get(id_client=row[0], project=project)
                    except Exponents.DoesNotExist:
                        exponent = Exponents.objects.get(id_client=row[0])
                        ExponentsDetail.objects.create(project=project, exponent=exponent)

                except Exponents.DoesNotExist:
                    results.append(dict(zip(columns, row)))
                    exponent = Exponents(**dict(zip(columns, row)))
                    exponent.save()
                    ExponentsDetail.objects.create(project=project, exponent=exponent )


            data = ExponentsDetail.objects.filter(project=project)
            content = exponents_projectsSerializerList(data, many=True).data
            return Response(content)
        return Response()

    @detail_route(methods=['get'])
    def data(self, request, id=None, pk=None):
        content = {}
        content['sponsor_type'] = []
        content['state'] = []
        expo_projects = ExponentsDetail()
        keys = ('id', 'name')
        for i in expo_projects.STATUS:
            a = 1
            content['sponsor_type'].append(dict(zip(keys, i)))
        for i in expo_projects.STATE:
            a = 1
            content['state'].append(dict(zip(keys, i)))

        exponents = Exponents.objects.all()

        content['exponents'] = []
        for row in exponents:
            temp = {}
            temp['id'] = row.id
            temp['name'] = row.name
            content['exponents'].append(temp)

        if pk:
            if pk != 'new':
                try:
                    expo_projects = ExponentsDetail.objects.get(id=pk)
                    content['individual'] = exponents_projectsSerializer(expo_projects).data
                    content['base'] = ExponentsSerializer(expo_projects.exponent).data

                    return Response(content)
                except:
                    pass

        content['individual'] = exponents_projectsSerializer(None).data
        content['base'] = ExponentsSerializer(None).data
        return Response(content)

    @detail_route(methods=['post'])
    def delete(self, request, id=None, pk=None):
        try:
            obj = ExponentsDetail.objects.get(id=pk)
            obj.delete()
            return Response({})
        except:
            return Response(status=status.HTTP_204_NO_CONTENT)

    @list_route(methods=['get'])
    def export(self, request, id=None):
        from django.http import HttpResponse
        try:
            from StringIO import StringIO
        except ImportError:
            from io import BytesIO

        output = BytesIO()
        book = Workbook(output)
        sheet = book.add_worksheet('exponents')

        head = book.add_format({
            'bold': 1,
            'align': 'center',
            'valign': 'vcenter',
        })

        text = book.add_format({
            'align': 'center',
            'valign': 'vcenter',
        })

        sheet.set_default_row(18)
        sheet.set_column('A:Z', 25)

        exponents = ExponentsDetail.objects.filter(project__id=id)
        exponents_json = ExponentsDetailListSerializer(exponents, many=True).data

        fields = ExponentsDetail._meta.get_fields()

        row = 0
        col = 0

        for j in fields:
            if not j.name in ['logo', 'project']:
                sheet.write(row, col, j.verbose_name, head)
                col += 1
        row += 1
        col = 0

        for i in exponents_json:
            for j in fields:
                if not j.name in ['logo', 'project']:

                    if (j.name in i.keys()):
                        # aaa = Exponents._meta.get_field(j.name).get_internal_type()
                        sheet.write(row, col, i[j.name], text)
                    col += 1
            row += 1
            col = 0

        book.close()

        output.seek(0)
        response = HttpResponse(output.read(),
                                content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        response['Content-Disposition'] = "attachment; filename=exponents.xlsx"

        return response
