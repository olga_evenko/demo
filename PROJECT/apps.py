from django.apps import AppConfig


class ProjectsConfig(AppConfig):
    name = 'backend.PROJECT'
