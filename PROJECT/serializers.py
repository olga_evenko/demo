from rest_framework import serializers

from .models import *
from datetime import *


class CustomDateTimeField(serializers.DateTimeField):
    def to_representation(self, value):
        tz = timezone.get_default_timezone()
        value = timezone.localtime(value, timezone=tz)
        return super().to_representation(value)


class ProjectSerializer(serializers.ModelSerializer):
    # metrika = serializers.SerializerMethodField("_is_metrika")
    # is_form_type = serializers.SerializerMethodField("_is_form_type")

    class Meta:
        model = Project
        fields = ("id", "name", 'metrika', "p_expobit",  'status', "date_start", "date_end", "description",\
                  'description_event', "time_start", "time_end", "time_last", "type_confirm_user", "logo", \
                  "ticket_logo", "is_business", "is_weekend", 'is_plane', 'is_form', 'is_pay', 'is_form_type', \
                  'is_promocode', 'is_repeat_visit', 'status_is_site', 'status_is_kiosk', 'type_printing',)

    def _is_form_type(self, obj):
        if obj.is_form_type:
            return obj.is_form_type
        return obj.is_form_type

class ProjectSerializerList(serializers.ModelSerializer):
    status = serializers.SerializerMethodField("_is_status")

    class Meta:
        model = Project
        fields = ("id", "name", "p_expobit", "fcmData", 'status', "date_start", "date_end", "description", \
                  'description_event', "time_start", "time_end", "time_last", "type_confirm_user", "logo", \
                  "ticket_logo", "is_business", "is_weekend", "status_is_site", "status_is_kiosk", )

    def _is_status(self, obj):
        if obj.status:
            for i in obj.STATUS:
                if i[0]==obj.status:
                    return i[1]
        return ""


class ProjectSerializerMobil(serializers.ModelSerializer):
    logo = serializers.SerializerMethodField("_is_logo")
    # metrika = serializers.SerializerMethodField("_is_metrika")
    class Meta:
        model = Project
        fields = ("id", "name", 'metrika', "date_start", 'status', "date_end", "description", 'description_event',\
                  "time_start", "time_end", "time_last", "logo", "is_business", "is_weekend", 'is_plane', 'is_form',\
                  'is_pay', 'is_form_type', 'type_confirm_user', 'is_promocode', 'is_repeat_visit',  "status_is_site", "status_is_kiosk", )

    def _is_logo(self, obj):
        if obj.logo:
            aaa= obj.logo
            return obj.logo.url
        return ""

    def _is_metrika(self, obj):
        if obj.metrika:
            return obj.metrika
        return ""

# class SbrAccountSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = SbrAccount
#         fields = ("id", 'is_production', "name", "api", "operator", 'password')

class SbrAccountSerializer(serializers.ModelSerializer):
    count = serializers.SerializerMethodField("_is_count")
    class Meta:
        model = SbrAccount
        fields = ("id", 'is_production', "name", "api", "operator", 'password', 'count')

    def _is_count(self, obj):
        if obj.id:
            projects = Project.objects.filter(sbr__id=obj.id).count()
            return projects
        return 0

class SbrAccountSerializerList(serializers.ModelSerializer):
    count = serializers.SerializerMethodField("_is_count")
    class Meta:
        model = SbrAccount
        fields = ("id", 'is_production', "name", "api", "operator", 'password', 'count')

    def _is_count(self, obj):
        if obj.id:
            projects = Project.objects.filter(sbr__id=obj.id).count()
            return projects
        return 0


class FormsSerializers(serializers.ModelSerializer):

    class Meta:
        model=Forms
        fields = ('id', 'name', 'type', 'data')


class NewsListSerializer(serializers.ModelSerializer):
    date_create = serializers.SerializerMethodField("_is_date_create")
    date_update = serializers.SerializerMethodField("_is_date_update")

    logo = serializers.SerializerMethodField("_is_logo")
    class Meta:
        model = News
        fields = ("id", "name", "project", "date_create", "date_update", "description", "logo" )
    def _is_logo(self, obj):
        if obj.logo:
            aaa= obj.logo
            return obj.logo.url
        return ""

    def _is_date_create(self, obj):

            if obj.date_create:
                return obj.date_create.strftime('%Y-%m-%d %H:%M')
            return ""

    def _is_date_update(self, obj):

            if obj.date_update:
                return obj.date_update.strftime('%Y-%m-%d %H:%M')
            return ""

class NewsSerializer(serializers.ModelSerializer):
    date_create = serializers.SerializerMethodField("_is_date_create")
    date_update = serializers.SerializerMethodField("_is_date_update")
    class Meta:
        model = News
        fields = ("id", "name", "project", "date_create", "date_update", "description", "logo" )

    def _is_date_create(self, obj):

            if obj.date_create:
                return obj.date_create.strftime('%Y-%m-%d %H:%M')
            return ""

    def _is_date_update(self, obj):

            if obj.date_update:
                return obj.date_update.strftime('%Y-%m-%d %H:%M')
            return ""

# class GlobalNewsSerializer(serializers.ModelSerializer):
#     date_create = serializers.SerializerMethodField("_is_date_create")
#     date_update = serializers.SerializerMethodField("_is_date_update")
#     class Meta:
#         model = News
#         fields = ("id", "name", "is_business", "date_create", "date_update", "description", "logo" )
#
#     def _is_date_create(self, obj):
#
#             if obj.date_create:
#                 return obj.date_create.strftime('%Y-%m-%d %H:%M')
#             return ""
#
#     def _is_date_update(self, obj):
#
#             if obj.date_update:
#                 return obj.date_update.strftime('%Y-%m-%d %H:%M')
#             return ""


class EventsSerializer(serializers.ModelSerializer):
    sites = serializers.SerializerMethodField('_is_sites')

    class Meta:
        model = Events
        fields = ("id", "name", "project", "sites", "stand", "description", "logo", "date_start", "date_end", "time_start", "time_end", )


    def _is_sites(self, obj):
        bb = Events.objects.get(id=obj.id).sites.all()
        temp =[]
        for i in bb: temp.append(i.id)

        return temp

class EventsSerializerReport(serializers.ModelSerializer):
    sites = serializers.SerializerMethodField('_is_sites')

    class Meta:
        model = Events
        fields = ("id", "name", "project", "sites", "stand", "description", "logo",'date_create', "date_start", "date_end", "time_start", "time_end", )


    def _is_sites(self, obj):
        bb = Events.objects.get(id=obj.id).sites.all()
        temp =[]
        for i in bb: temp.append(i.id)

        return temp


class EventsSerializerView(serializers.ModelSerializer):
    logo = serializers.SerializerMethodField("_is_logo")
    sites = serializers.SerializerMethodField('_is_sites')

    class Meta:
        model = Events
        fields = ("id", "name", "project", "sites", "stand", "description", "logo", "date_start", "date_end", "time_start", "time_end",)

    def _is_logo(self, obj):
        if obj.logo:
            aaa = obj.logo
            return obj.logo.url
        return ""

    def _is_sites(self, obj):
        bb = Events.objects.get(id=obj.id).sites.all()
        temp = []
        for i in bb: temp.append(i.id)

        return temp


class ExponentsDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExponentsDetail
        fields = ("id", 'project', "name", "address", "phone", "email", 'web_site', "logo", "description", 'is_sponsor', 'sponsor_type', 'priority')



class ExponentsDetailListSerializer(serializers.ModelSerializer):
    sponsor_type = serializers.SerializerMethodField('_is_sponsor_type')
    is_sponsor = serializers.SerializerMethodField('_is_is_sponsor')
    class Meta:
        model = ExponentsDetail
        fields = ("id", 'project', "name", "address", "phone", "email", 'web_site', "logo", "description", 'is_sponsor', 'sponsor_type', 'priority')

    def _is_sponsor_type(self, obj):
        if obj.sponsor_type:

            for i in ExponentsDetail._meta.get_field('sponsor_type').choices:
                if obj.sponsor_type==i[0]:
                 return i[1]

                continue
        return ""

    def _is_is_sponsor(self, obj):
        if obj.is_sponsor:
            return 'Да'
        return ""

class exponents_projectsSerializerList(serializers.ModelSerializer):

    # sponsor = serializers.SerializerMethodField("_is_sponsor")
    sponsor_type = serializers.SerializerMethodField("_is_sponsor_type")
    name = serializers.SerializerMethodField("_is_exponent")
    address = serializers.SerializerMethodField("_is_address")
    phone = serializers.SerializerMethodField("_is_phone")
    email = serializers.SerializerMethodField("_is_email")
    web_site = serializers.SerializerMethodField("_is_web_site")
    description = serializers.SerializerMethodField("_is_description")
    logo = serializers.SerializerMethodField("_is_logo")

    class Meta:
        model = ExponentsDetail
        fields = ("id", 'priority', "name", 'address', 'phone', 'email', 'web_site', 'description', "project", "sponsor", "sponsor_type",  "logo")

    def _is_sponsor(self, obj):
        if obj.sponsor:
            return "Да"
        return "Нет"

    def _is_sponsor_type(self, obj):
        if obj.sponsor_type:
            for i in obj.STATUS:
                if i[0] == obj.sponsor_type:
                    return i[1]
        return ''

    def _is_exponent(self, obj):
        exp = ExponentsDetail.objects.get(id=obj.exponent_id)
        return exp.name

    def _is_address(self, obj):
        exp = ExponentsDetail.objects.get(id=obj.exponent_id)
        return exp.address

    def _is_phone(self, obj):
        exp = ExponentsDetail.objects.get(id=obj.exponent_id)
        return exp.phone

    def _is_email(self, obj):
        exp = ExponentsDetail.objects.get(id=obj.exponent_id)
        return exp.email

    def _is_web_site(self, obj):
        exp = ExponentsDetail.objects.get(id=obj.exponent_id)
        return exp.web_site

    def _is_logo(self, obj):
        if obj.special_logo:
            return obj.special_logo.url
        else:
            exp = ExponentsDetail.objects.get(id=obj.exponent_id)
            if exp.logo:
                return exp.logo.url
            else:
                return ""

    def _is_description(self, obj):
        if obj.special_description:
            return obj.special_description
        else:
            exp = ExponentsDetail.objects.get(id=obj.exponent_id)
            return exp.description


class exponents_projectsSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExponentsDetail
        fields = ("id", "project", "exponent", "sponsor", "sponsor_type", "special_logo", "special_description")



