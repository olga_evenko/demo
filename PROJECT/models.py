from django.db import models
from plus_administration.models import *
from simple_history.models import HistoricalRecords

from datetime import *

from PIL import Image
# import numpy as np
from django.contrib.postgres.fields import JSONField

ACTION = (
    ('create', 'Создание'),
    ('update', 'Изменение')
)

class SbrAccount(models.Model):
    name = models.CharField(
        max_length=300, verbose_name='Наименование',
        blank=True)
    api = models.CharField(
        max_length=30, verbose_name='API',
        blank=True)
    operator = models.CharField(
        max_length=30, verbose_name='Оператор',
        blank=True)
    password = models.CharField(
        max_length=30, verbose_name='Пароль',
        blank=True)

    is_production = models.BooleanField(verbose_name='Боевые настройки', default=False)

    url = models.CharField(
        max_length=300, verbose_name='ссылка',
        blank=True)

    history = HistoricalRecords()

    class Meta:
        db_table = 'PROJECT_sbr'

    def save(self, *args, **kwargs):
        if(self.is_production):
            self.url = "https://securepayments.sberbank.ru"
        else:
            self.url = "https://3dsec.sberbank.ru"
        super(SbrAccount, self).save(*args, **kwargs)

class Sites(models.Model):
    name = models.CharField(max_length=64, verbose_name='Наименование площадки')
    area = models.CharField(max_length=8, verbose_name='Площадь', blank=True, null=True)
    capacity = models.IntegerField(verbose_name='Вместимость', blank=True, null=True)
    building_time = models.BooleanField(default=False)
    class Meta:
        managed = False
        verbose_name = 'Площадка'
        verbose_name_plural = 'Площадки'
        db_table = 'projects_sites'
    def __str__(self):
        return self.name


class Project(models.Model):
    STATUS = (
        ('plane', 'Планируемый'),
        ('working', 'Ожидаемый'),
        ('sale', 'В продаже'),
        ('end', 'Завершенный')
    )
    TYPE_CONFIRM=(
        ('none', 'Без подтверждения'),
        ('email', 'По e-mail'),
        # ('phone', 'По телефону'),
        # ('ph_mail', 'По телефону и e-mail'),
        # ('choice', 'На выбор пользователя'),
    )

    IS_FORM_TYPE = (
        ('n_buy', 'Обязательно до покупки'),
        ('n_in', 'Обязательно до посещения'),
        ('not', 'Не обязательно'),

        # ('not', 'Не обязательно'),
        # ('online', 'Только онлайн покупка'),
        # ('cash', 'Только в кассе'),
        # ('all', 'Обязательно'),
    )

    TYPE_PRINTING = (
        ('0', 'На чековой бумаге'),
        ('1', 'На билетной бумаге'),
    )
    name = models.CharField(
        max_length=250, verbose_name='Название проекта',
        blank=True)
    p_expobit = models.CharField(
        max_length=5, verbose_name='Связанный проект в ExpoBit',
        blank=True)
    fcmData = models.ForeignKey(
        FcmData, on_delete=models.CASCADE,
        verbose_name='Мобильное приложение',
        blank=True, null=True
    )
    sbr = models.ForeignKey(
        SbrAccount, on_delete=models.CASCADE,
        verbose_name='Настройки сбербанка',
        blank=True, null=True
    )
    metrika = models.CharField(
        max_length=100, verbose_name='Yandex метрика', null=True,
        blank=True)
    date_start = models.DateField(
        verbose_name='Дата начала',
        blank=True, null=True)
    date_end = models.DateField(
        verbose_name='Дата окончания',
        blank=True, null=True)
    time_start = models.TimeField(
        blank=True, null=True,
        verbose_name='Время начала')
    time_end = models.TimeField(
        blank=True, null=True,
        verbose_name='Время окончания')
    description = models.CharField(
        max_length=1500, verbose_name='Описание проекта',
        blank=True,)
    description_event = models.CharField(
        max_length=1500, verbose_name='Описание проекта',
        blank=True, )
    time_last = models.TimeField(
        blank=True, null=True,
        verbose_name='Время окончания в последний день')
    type_confirm_user = models.CharField(
        max_length=10, verbose_name='тип подтверждения пользователя',
        choices=TYPE_CONFIRM,
        blank=True, )
    logo = models.ImageField(
        blank=True,
        upload_to='./logo/', help_text='150x150px',
        verbose_name='Логотип')
    ticket_logo = models.ImageField(
        blank=True,
        upload_to='./ticket_logo/', help_text='150x150px',
        verbose_name='Логотип')

    type_printing = models.CharField(
        max_length=10, verbose_name='Вид печати',
        choices=TYPE_PRINTING, default='0',
        blank=True, )

    is_business = models.BooleanField(verbose_name='Проекты бизнесс-драйв', default=False)

    is_weekend = models.BooleanField(verbose_name='Разделение на выходной и будний', default=False)

    is_plane = models.BooleanField(verbose_name='Планировка зала', default=False)

    is_form = models.BooleanField(verbose_name='Анкета', default=False)

    is_pay= models.BooleanField(verbose_name='Оплата входа', default=False)

    is_promocode= models.BooleanField(verbose_name='Промокоды', default=False)

    is_repeat_visit = models.BooleanField(verbose_name='Учет повторного посещения', default=False)

    is_form_type = models.CharField(
        max_length=10, verbose_name='Вид анкетирования',
        choices=IS_FORM_TYPE,
        blank=True, null=True)

    status = models.CharField(
        max_length=10, verbose_name='Статус проекта',
        default='plane',
        choices=STATUS,
        blank=True,)

    status_is_site = models.BooleanField(verbose_name='Сайт', default=False)
    status_is_kiosk = models.BooleanField(verbose_name='Киоск', default=False)
    status_is_cash = models.BooleanField(verbose_name='Касса', default=False)

    # kiosk = models.ManyToManyField(ExtUser)
    kiosk_buy = models.ManyToManyField(ExtUser,  through='KioskProject')

    history = HistoricalRecords()

    class Meta:
        # ordering = ['id']
        db_table = 'PROJECT_projects'

    def save(self, *args, **kwargs):
        super(Project, self).save(*args, **kwargs)

        if self.logo:

            size = [
                {"id": 'xl', 'val': 700},
                {"id": 'm',  'val': 500},
                {"id": 'xs', 'val': 120},
            ]

            for s in size:

                img = Image.new('RGB', (60, 30), color='red')
                url = 'media/'+self.logo.name.replace('.', "_" + s['id'] + '.')
                img.save(url)

                image = Image.open(self.logo.path)
                image.thumbnail((s['val'], s['val']))
                image.save(url)


class KioskProject(models.Model):
    TYPE_PRINTING = (
        ('0', 'На чековой бумаге'),
        ('1', 'На билетной бумаге'),
    )

    user = models.ForeignKey(ExtUser, on_delete=models.CASCADE, null=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    type_printing = models.CharField(
        max_length=10, verbose_name='Вид печати',
        choices=TYPE_PRINTING, default='0',
        blank=True, )



class Forms(models.Model):
    TYPE=(
        ('registr','Регистрация посетителя'),
        ('registr_eb_pay','Анкета посетителя (если выставляется счет)'),
        ('sat_visit','Удовлетворенность посетителя'),
        ('sat_exp','Удовлетворенность экспонента'),
    )

    TYPE_QUESTIONS = (
        ('text', 'Текст'),
        ('number', 'Число'),
        ('phone', 'Телефон'),
        ('email', 'E-mail'),
        ('choice', 'Выбор'),
        ('city', 'Город'),
        ('date', 'Дата'),
        # ('rate', 'Рейтинг'),
        # ('choice_activities', 'Выбор вида деятельности'),
        # ('choiсe_many', 'Множественный выбор'),
        # ('choiсe_text', 'Выбор с текстовым полем'),
        # ('choiсe_many_text', 'Множественный выбор с текстовым полем'),
    )
    name = models.CharField(
        verbose_name='Наименование',
        max_length=200,
    )
    project = models.ManyToManyField(Project)
    type = models.CharField(
        choices=TYPE, verbose_name='Тип анкеты',
        max_length=20,
    )
    data = JSONField()
    history = HistoricalRecords()

    class Meta:
        db_table = 'PROJECT_forms'
        # ordering = ['date_create']

class News(models.Model):

    name = models.CharField(
        max_length=250, verbose_name='Заголовок',
        blank=True,
    )
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE,
        verbose_name='Проект',
        null=False, blank=False,
    )
    date_create = models.DateTimeField(
        verbose_name='Дата создания',
        blank=True, null=True)
    date_update= models.DateTimeField(
        verbose_name='Дата обновления',
        blank=True, null=True)
    description = models.CharField(
        max_length=2000, verbose_name='Текст новости',
        blank=True,)
    logo = models.ImageField(
        blank=True,
        upload_to='./news/logo/',
        verbose_name='Логотип')

    history = HistoricalRecords()


    class Meta:
        db_table = 'PROJECT_news'
        ordering = ['date_create']

    def save(self, *args, **kwargs):
        try:
            obj = News.objects.get(id=self.id)
            self.date_update = datetime.now()
            super(News, self).save(*args, **kwargs)
        except News.DoesNotExist:
            self.date_create = datetime.now()
            self.date_update = datetime.now()
            super(News, self).save(*args, **kwargs)


class Events(models.Model):

    name = models.CharField(
        max_length=250, verbose_name='Заголовок',
        blank=True,
    )
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE,
        verbose_name='Проект',
        null=False, blank=False,
    )

    stand = models.CharField(
        max_length=100, verbose_name='Стенды',
        blank=True, 
    )
    date_create = models.DateField(
        verbose_name='Дата создания',
        blank=True, null=True
    )
    date_update = models.DateField(
        verbose_name='Дата обновления',
        blank=True, null=True
    )
    date_start = models.DateField(
        verbose_name='Дата начала',
        blank=True, null=True
    )
    date_end = models.DateField(
        verbose_name='Дата окончания',
        blank=True, null=True
    )
    time_start = models.TimeField(
        blank=True, null=True,
        verbose_name='Время начала'
    )
    time_end = models.TimeField(
        blank=True, null=True,
        verbose_name='Время окончания'
    )
    description = models.CharField(
        max_length=2500, verbose_name='Описание',
        blank=True,
    )
    logo = models.ImageField(
        blank=True,
        upload_to='./event/logo/',
        verbose_name='Логотип'
    )

    sites = models.ManyToManyField(Sites,
       verbose_name='Место проведения',
       blank=True
    )

    history = HistoricalRecords()


    class Meta:
        ordering = ['date_start']
        db_table = 'PROJECT_events'

class ExponentsDetail(models.Model):
    STATUS = (
        (
            ('80', 'Генеральный спонсор/партнер выставки'),
            ('70', 'Официальный спонсор/партнер выставки'),
            ('60', 'Информационный спонсор/партнер выставки'),
            ('50', 'Технический спонсор/партнер выставки'),
            ('40', 'Спонсор регистрации выставки'),
            ('30', 'Генеральный спонсор/партнер мероприятия (Чемпионата)'),
            ('20', 'Официальный спонсор/партнер мероприятия (Чемпионата)'),
            ('10', 'Спонсор регистрации мероприятия (Чемпионата)'),
        )
    )

    project = models.ForeignKey(Project, related_name='project', on_delete=models.PROTECT, verbose_name='Проект')
    # exponent = models.ForeignKey(Exponents, related_name='exponent', on_delete=models.PROTECT, verbose_name='Глобальные данные')

    name = models.CharField(
        max_length=250, verbose_name='Наименование',
        blank=True,
    )

    address = models.CharField(
        max_length=250, verbose_name='Адрес',
        blank=True,
    )
    phone = models.CharField(
        max_length=200, verbose_name='Телефон',
        blank=True,
    )
    email = models.EmailField(
        verbose_name='E-mail',
        blank=True,
    )
    web_site = models.CharField(
        verbose_name='Web site',
        max_length=250,
        blank=True,
    )
    logo = models.ImageField(
        blank=True,
        upload_to='./exponent/logo/',
        verbose_name='Логотип'
    )
    description = models.CharField(
        max_length=1000, verbose_name='Адрес',
        blank=True,
    )

    history = HistoricalRecords()

    is_sponsor = models.NullBooleanField(
        default=False, verbose_name='Является спонсором',
    )
    sponsor_type = models.CharField(
        blank=True,
        choices=STATUS,
        verbose_name='Тип спонсорства',
        max_length=10
    )
    priority = models.IntegerField(
        verbose_name='Приоритет отображения', null=True,
    )

    history = HistoricalRecords()

    class Meta:
        db_table = 'PROJECT_ExponentsDetail'
        ordering = ['-priority']

    def save(self, *args, **kwargs):
        if(self.sponsor_type):
            self.priority = int(self.sponsor_type)
            self.is_sponsor= True
        else:
            self.priority = 1
            self.is_sponsor = False
        super(ExponentsDetail, self).save(*args, **kwargs)







