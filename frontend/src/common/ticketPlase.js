
var select = false

function haveIntersection(r1, r2) {
    return !(
        r2.x > r1.x + r1.width ||
        r2.x + r2.width < r1.x ||
        r2.y > r1.y + r1.height ||
        r2.y + r2.height < r1.y
    );
}

function Numerate(jsonTickets, formData) {
  var plase_fact = 1
  var plase_real = 1
  var row_real = 1
  for(var i in jsonTickets){
    if(jsonTickets[i].category){
      jsonTickets[i].plase_real=plase_real
      plase_real+=1
    }else{
      jsonTickets[i].plase_real=''
    }
    plase_fact+=1

    if(plase_fact>formData.plase[0]){
      for(var j = i-formData.plase[0]+1; j<=i; j++){
        if(jsonTickets[i].category){
          jsonTickets[j].row_real=row_real
          plase_real+=1
        }else{
          jsonTickets[j].row_real =  ''
        }
      }
      if(plase_real!=1){
        row_real+=1
      }

      plase_fact = 1
      plase_real = 1
      
    }
  }
}

function UpdatePlane(node, node_text, color, color_text) {
    node.fill(color)
    node.draw()
    if(node.parent.parent.parent.attrs.jsonTickets.plase_real){
      node_text.text(node.parent.parent.parent.attrs.jsonTickets.plase_real)
    }else{
      node_text.text(node.parent.parent.parent.attrs.jsonTickets.plase)
    }
    node_text.fill(color_text)
    node_text.draw()
}

export function createHall (data, jsonTickets, jsonHall, typeTickets, selectTypeTicket, edit) {

	var row = data.row[0]
	var plase = data.plase[0]
	var width = (plase*(20+5)+20) > 800 ? (plase*(20+5)+20) :800 ;
  var height = (row*(20+5)+120) > 400 ? (row*(20+5)+120) :400;

	var stage = new Konva.Stage ({ 
    temp: data,
		jsonTickets:jsonTickets,
		jsonHall:jsonHall,
		selectTypeTicket:selectTypeTicket,
		typeTickets:typeTickets,
		container: 'container', 
		width: width, 
		height: height,
	});

	var layer = new Konva.Layer({id:'editLayer'});
	// layer.setZIndex(3)
	var rect1 = new Konva.Rect({
      x: 0,
      y: 0,
      name:'layerHall',
      width: (plase*(20+5)+20) > 800 ? (plase*(20+5)+20) :800,
      height: (row*(20+5)+120) > 400 ? (row*(20+5)+120) :400,
      // fill: '',
      stroke: 'black',
      strokeWidth: 4,
    });


  if(edit){
   	stage.on('mousedown', function (e, jsonTickets) {
   		if (this.attrs.selectTypeTicket[0] && !this.attrs.temp.editFreePlace){
  	 		if(select){
  	 			this.findOne('#selectLayer').destroy()
  	 		}
  	    	select = true
  	    	var layer = new Konva.Layer({id:'selectLayer'});
  	      	var select_rect = new Konva.Rect({
  		      x: e.evt.offsetX+1,
  		      y: e.evt.offsetY+1,
  		      id:'selectRect',
  		      width: 0,
  		      height: 0,
  		      fill: '#ffffff00',
  		      stroke: 'black',
  		      strokeWidth: 1,
  		    });
  		  layer.add(select_rect)
  			stage.add(layer);
  			this.find('#selectLayer').draw()
  		}
    });
    stage.on('mouseup', function (e) {
    	if (this.attrs.selectTypeTicket[0] && !this.attrs.temp.editFreePlace){
        Numerate;
        jsonTickets;
        data;

      	select=false
      	this.findOne('#selectLayer').destroy()
        this.draw()
      	var editLayer = this.findOne('#editLayer')
      	this.findOne('#layerPlase').findOne('#groupPlase').children.each(function (element) {
            editLayer.parent.attrs.jsonTickets[element.attrs.id].category = element.attrs.category
        });
        Numerate(jsonTickets, data)
        var stage = e.target.parent.parent
        if(e.target.parent.parent.parent){
          stage = e.target.parent.parent.parent
        }

        for(var i in stage.children[1].children[0].children){
          if(!isNaN(Number(i))){

            if(!!stage.attrs.selectTypeTicket.length){
              if(stage.attrs.jsonTickets[i].plase_real){

                if(stage.children[1].children[0].children[i].attrs.category == stage.attrs.selectTypeTicket[0].id){
                  var color = stage.attrs.selectTypeTicket[0].color
                }else{
                  var color = stage.children[1].children[0].children[i].fill()
                }

                var color_text = '#000'
                var text_data = stage.attrs.jsonTickets[i].plase_real
              }else{
                var color = '#bdbdbd'
                var color_text = '#dfdfdf'
                var text_data = stage.attrs.jsonTickets[i].plase
              }
            }else{
              if(stage.attrs.jsonTickets[i].plase_real){
                if(stage.attrs.jsonTickets[i].pay){
                  var color = '#56585d'
                }else{
                  var color = stage.children[1].children[0].children[i].fill()
                }
                var color_text = '#000'
                var text_data = stage.attrs.jsonTickets[i].plase_real
              }else{
                var color = '#bdbdbd'
                var color_text = '#dfdfdf'
                var text_data = stage.attrs.jsonTickets[i].plase
              }
            }

            stage.children[1].children[0].children[i].fill(color)
            stage.children[1].children[0].children[i].draw()

            stage.children[1].children[1].children[i].text(text_data)
            stage.children[1].children[1].children[i].fill(color_text)

            stage.children[1].children[1].children[i].draw()
          }   
        }
      }
    });
    stage.on('mousemove', function(e) {
      Numerate;
      jsonTickets;
      data;
      UpdatePlane;


      if(select){
        if(e.target.parent.parent.attrs.temp){
          var stage = e.target.parent.parent
        }else{
          var stage = e.target.parent.parent.parent
        }

        // if(stage.attrs.temp.editFreePlace)

        if(this.attrs.selectTypeTicket[0] && !stage.attrs.temp.editFreePlace){
          this.findOne('#selectLayer').findOne('#selectRect').attrs.width = e.evt.offsetX -this.findOne('#selectLayer').findOne('#selectRect').attrs.x
          this.findOne('#selectLayer').findOne('#selectRect').attrs.height = e.evt.offsetY - this.findOne('#selectLayer').findOne('#selectRect').attrs.y
          this.find('#selectLayer').draw()

          var editLayer = this.findOne('#selectLayer').findOne('#selectRect')

          this.findOne('#layerPlase').findOne('#groupPlase').children.each(function (element) {
            this
                  var targetRect = editLayer.getClientRect();
                  
                  if (haveIntersection(element.getClientRect(), targetRect)) {
                    if(editLayer.parent.parent.attrs.jsonTickets[element.attrs.id].pay){
                        element.fill('#56585d');
                        Numerate(jsonTickets, data)
                        element.parent.parent.children[1].children[element.attrs.id].text(jsonTickets[element.attrs.id].plase_real)
                        element.parent.parent.children[1].children[element.attrs.id].fill('#000')
                    }else{
                      element.attrs.category = editLayer.parent.parent.attrs.selectTypeTicket[0].id
                      element.fill(editLayer.parent.parent.attrs.selectTypeTicket[0].color);

                      Numerate(jsonTickets, data)
                      element.parent.parent.children[1].children[element.attrs.id].text(jsonTickets[element.attrs.id].plase)
                      element.parent.parent.children[1].children[element.attrs.id].fill('#909399')
                    }

                  } else {
                      element.attrs.category = editLayer.parent.parent.attrs.jsonTickets[element.attrs.id].category;
                      // debugger
                      if(editLayer.parent.parent.attrs.jsonTickets[element.attrs.id].pay){
                        element.fill('#56585d');
                        Numerate(jsonTickets, data)
                        element.parent.parent.children[1].children[element.attrs.id].text(jsonTickets[element.attrs.id].plase_real)
                        element.parent.parent.children[1].children[element.attrs.id].fill('#000')

                      }else{
                        if(editLayer.parent.parent.attrs.jsonTickets[element.attrs.id].category){
                          element.fill(editLayer.parent.parent.attrs.typeTickets[element.attrs.category].color);
                          Numerate(jsonTickets, data)
                          element.parent.parent.children[1].children[element.attrs.id].text(jsonTickets[element.attrs.id].plase_real)
                          element.parent.parent.children[1].children[element.attrs.id].fill('#000')                      
                        }else{
                          element.attrs.category = '';
                          element.fill('#BDBDBD')
                          Numerate(jsonTickets, data)
                          element.parent.parent.children[1].children[element.attrs.id].text(jsonTickets[element.attrs.id].plase)
                          element.parent.parent.children[1].children[element.attrs.id].fill('#dfdfdf')
                        }
                      }
                      
                  }
                  element.draw()
                  element.parent.parent.children[1].children[element.attrs.id].draw()
              });

        }  

      }
  	
    });
  }

  var complexText = new Konva.Text({
    x: width/2-100,
    y: 10,
    text: 'Сцена',
    fontSize: 18,
    fontFamily: 'Calibri',
    fill: '#555',
    width: 200,
    padding: 20,
    align: 'center'
  });


  var rect2 = new Konva.Rect({
    x: width/2-100,
    y: 10,
    name:'layerHall',
    stroke: '#555',
    strokeWidth: 5,
    fill: '#ddd',
    width: 200,
    height: complexText.getHeight(),
    shadowColor: 'black',
    shadowBlur: 10,
    shadowOffset: [10, 10],
    shadowOpacity: 0.2,
    // cornerRadius: 10
  });

  layer.add(rect1);
  layer.add(rect2);
  layer.add(complexText);

	stage.add(layer);
	// debugger
	createPlase(stage,data, jsonTickets, jsonHall, typeTickets)
}



function createPlase (stage,data, jsonTickets, jsonHall, typeTickets) {
  // debugger
  var row = data.row[0]
  var plase = data.plase[0] 
  
  var layer = new Konva.Layer({
    id:'layerPlase'
  });

  var group = new Konva.Group({
    id:'groupPlase',
    // draggable: true 
    });
  var grouptext = new Konva.Group({
    id:'groupPlaseText',
    // draggable: true 
    });

    if(!jsonTickets.length){
      var y = 110
      for(var r=1; r<=row; r++){
        var x = stage.attrs.width/2 -((25*plase)-3)/2
        for(var p=1; p<=plase; p++){

          jsonTickets.push({
            id:jsonTickets.length,
            row:r, 
            plase:p,
            category: '',
            pay: false,
          })

          var rect = new Konva.Rect({
                id:jsonTickets.length-1,
                x: x,
                y: y,
                width: 22,
                height: 22,
                old_color: jsonTickets[jsonTickets.length-1].category? jsonTickets[jsonTickets.length-1].color : '#BDBDBD',
                fill: jsonTickets[jsonTickets.length-1].category? jsonTickets[jsonTickets.length-1].color : '#BDBDBD',
                category:'',
                name:'ticketPlase'
          });

          group.add(rect)
          var text = new Konva.Text({
            x: x,
            y: y,
            text: p,
            fontSize: 18,
            fontFamily: 'Calibri',
            fill: '#000',
            width: 22,
            padding: 2,
            align: 'center',
            name:'ticketPlaseText',
            listening:false,
          });
          grouptext.add(text)
          x+=25
        }
        y+=25
      }
    }else{
      var y = 85
      var x = stage.attrs.width/2 -((25*plase)-3)/2 - 25
        for(var i=0; i<jsonTickets.length; i++){
          if(jsonTickets[i].pay){
            var color = '#56585d'
            if(jsonTickets[i].reserv_plase){
              var color = '#909399'
            }
          }else{
            var color  = jsonTickets[i].category? typeTickets[jsonTickets[i].category].color : '#BDBDBD'
          }
          if(jsonTickets[i].category){
            var color_text = '#000'
          }else{
            var color_text  = '#dfdfdf'
          }
          var rect = new Konva.Rect({
              id:jsonTickets[i].id,
              x: x + 25 * jsonTickets[i].plase,
              y: y + 25 * jsonTickets[i].row,
              width: 22,
              height: 22,
              old_color: jsonTickets[i].category? typeTickets[jsonTickets[i].category].color : '#BDBDBD',
              fill: color,
              category:jsonTickets[i].category,
              name:'ticketPlase'
          });
          group.add(rect)

          var text = new Konva.Text({
              x: x + 25 * jsonTickets[i].plase,
              y: y + 25 * jsonTickets[i].row,
              text: jsonTickets[i].plase_real ? jsonTickets[i].plase_real : jsonTickets[i].plase,
              fontSize: 18,
              fontFamily: 'Calibri',
              fill: color_text,
              width: 22,
              padding: 2,
              align: 'center',
              name:'ticketPlaseText',
              listening:false,
          });
          grouptext.add(text)          
        }
    }

  layer.add(group);
  layer.add(grouptext);

  group.on('click', function(e) {
      select;
      Numerate;
      jsonTickets;
      data;

      if(e.target.getName() == 'ticketPlase'){
        if(e.target.parent.parent.parent.attrs.temp.editFreePlace){
          debugger
          if(e.target.parent.parent.parent.attrs.jsonTickets[e.target.attrs.id].pay){

            if(e.target.parent.parent.parent.attrs.temp.editFreePlace == 'free_plase' 
              && e.target.attrs.fill == '#56585d' 
              && !e.target.parent.parent.parent.attrs.jsonTickets[e.target.attrs.id].reserv_plase){
                e.target.parent.parent.parent.attrs.jsonTickets[e.target.attrs.id].pay = false
                e.target.parent.parent.parent.attrs.jsonTickets[e.target.attrs.id].false = true
                e.target.fill(e.target.attrs.old_color)
            }else if(e.target.parent.parent.parent.attrs.temp.editFreePlace == 'reserv' && e.target.parent.parent.parent.attrs.jsonTickets[e.target.attrs.id].reserv_plase){
              debugger
              e.target.parent.parent.parent.attrs.jsonTickets[e.target.attrs.id].pay = false
              e.target.parent.parent.parent.attrs.jsonTickets[e.target.attrs.id].false = true
              e.target.fill(e.target.attrs.old_color)
            }

            // e.target.parent.parent.parent.attrs.jsonTickets[e.target.attrs.id].pay = false
            // e.target.fill(e.target.attrs.old_color)
            
          }else if(!e.target.parent.parent.parent.attrs.jsonTickets[e.target.attrs.id].pay){
            if(e.target.parent.parent.parent.attrs.temp.editFreePlace == 'free_plase'){
              e.target.parent.parent.parent.attrs.jsonTickets[e.target.attrs.id].pay = true
              e.target.fill('#56585d')
            }else{
              e.target.parent.parent.parent.attrs.jsonTickets[e.target.attrs.id].pay = true
              e.target.parent.parent.parent.attrs.jsonTickets[e.target.attrs.id].reserv_plase = true
              e.target.fill('#909399')
            }
            
          }
          e.target.draw()
          e.target.parent.parent.children[1].children[e.target.attrs.id].text(e.target.parent.parent.parent.attrs.jsonTickets[e.target.attrs.id].plase_real)
          e.target.parent.parent.children[1].children[e.target.attrs.id].fill('#000')
          e.target.parent.parent.children[1].children[e.target.attrs.id].draw()
          
        }else{
          if(e.target.parent.parent.parent.attrs.temp.edit){
              if(e.target.parent.parent.parent.attrs.selectTypeTicket[0]){
                var stage = e.target.parent.parent.parent 
                if(! e.target.attrs.category || e.target.attrs.category!=stage.attrs.selectTypeTicket[0].id){
                 
                  e.target.attrs.category = stage.attrs.selectTypeTicket[0].id
                  e.target.parent.parent.parent.attrs.jsonTickets[e.target.attrs.id].category = e.target.attrs.category
                  Numerate(jsonTickets, data)

                }else{

                  e.target.attrs.category = ''
                  e.target.parent.parent.parent.attrs.jsonTickets[e.target.attrs.id].category =''
                  Numerate(jsonTickets, data)
                }

                for(var i in e.target.parent.parent.children[0].children){
                    if(!isNaN(Number(i))){
                      if(e.target.parent.parent.parent.attrs.jsonTickets[i].plase_real){
                        if(e.target.parent.parent.children[0].children[i].attrs.category == stage.attrs.selectTypeTicket[0].id){
                          var color = stage.attrs.selectTypeTicket[0].color
                        }else{
                          var color = e.target.parent.parent.children[0].children[i].fill()
                        }
                        
                        var color_text = '#000'
                        var text_data = e.target.parent.parent.parent.attrs.jsonTickets[i].plase_real
                      }else{
                        var color = '#bdbdbd'
                        var color_text = '#dfdfdf'
                        var text_data = e.target.parent.parent.parent.attrs.jsonTickets[i].plase
                      }

                      e.target.parent.parent.children[0].children[i].fill(color)
                      e.target.parent.parent.children[0].children[i].draw()

                      e.target.parent.parent.children[1].children[i].text(text_data)
                      e.target.parent.parent.children[1].children[i].fill(color_text)

                      e.target.parent.parent.children[1].children[i].draw()
                    }
                    
                  }
              }
          }
        }
      }
    
  })

  stage.add(layer);

}

