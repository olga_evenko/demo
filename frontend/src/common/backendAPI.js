import * as types from '../store/mutation-types'
import axios from 'axios'
import $ from 'jquery'
import store from '../store'
import router from '../router'


// axios.defaults.xsrfCookieName = 'csrftoken'
// axios.defaults.xsrfHeaderName = 'X-CSRFToken'

// axios.interceptors.request.use(
//   config => {
//     debugger
//     if (store.state.token) {
//        config.headers.Authorization = `JWT ${store.state.token}`
//     }
//     return config
//   },
//   err => {
//     return Promise.reject(err)
//   })
// debugger


// axios.defaults.headers.common['csrftoken'] = window.csrf_token;
// // Used by Rails to check if it is a valid XHR request
// axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

// debugger
 if(!!$('.app_init').length){

  axios.interceptors.request.use(
  config => {

    if (store.state.token) {
       config.headers.Authorization = `JWT ${store.state.token}`
    }
    return config
  },
  err => {
    return Promise.reject(err)
  })

 }

 if(!!$('.quality_init').length){

  axios.interceptors.request.use(
  config => {

    if (store.state.token) {
       config.headers.Authorization = `JWT ${store.state.token}`
    }
    return config
  },
  err => {
    return Promise.reject(err)
  })

 }


axios.interceptors.response.use(
  response => {
    return response
  },
  error => {
    console.log('[Response error!]', error.response)
    if (error.response) {
      switch (error.response.status) {
        case 401:
          store.commit(types.LOGOUT, {
            router: router,
            redirect: router.currentRoute.fullPath
          })
          break
        case 500:
          console.log(error.response.statusText)
          break
      }
    }
    return Promise.reject(error.response.data)
  })

export function base () {
  return 'http://' + window.location.hostname + ':8000/'
}

export function UserProfile (userID) {
  return axios.get(base() + 'users/' + userID + '/')
}

export function get (url, data=null) {
  if(data){
    return axios.get(url,  {
      params: {
        data: data
      }
    })
  }else{
    return axios.get(url)
  }
}


export function get_local (url) {
  return axios.get( url, {
        // headers: {
        //     'Content-Type': 'multipart/form-data',
        //     'Access-Control-Allow-Origin': '*',
        //   'Access-Control-Allow-Methods': 'GET, OPTIONS'
        // }
      })
}


export function post (url, data) {
  // debugger
  return axios.post(url, data,)
}

export function file (url, data) {
  debugger
  return axios.post(url, data, 'blob'
    )
}

export function update (url, data) {
  return axios.post( url, data,
    {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
}
export function del (url) {
  return axios.post( url,
        {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
      }
    )
}