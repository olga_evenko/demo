import * as types from '../store/mutation-types'
import axios_t from 'axios'
import store from '../store/tickets.js'
import router from '../router/tickets.js'

import $ from 'jquery'

debugger
axios_t.interceptors.request.use(
  config => {
    debugger
    debugger
    if (store.state.token) {
       config.headers.Authorization = `JWT ${store.state.token}`
    }
    return config
  },
  err => {
    return Promise.reject(err)
  })


axios_t.interceptors.response.use(
  response => {
    return response
  },
  error => {
    console.log('[Response error!]', error.response)
    if (error.response) {
      switch (error.response.status) {
        case 401:
          store.commit(types.LOGOUT, {
            router: router,
            redirect: router.currentRoute.fullPath
          })
          break
        case 500:
          console.log(error.response.statusText)
          break
      }
    }
    return Promise.reject(error.response.data)
  })

export function base () {
  return 'http://' + window.location.hostname + ':8000/'
}

export function UserProfile (userID) {
  return axios_t.get(base() + 'users/' + userID + '/')
}

export function get (url, data=null) {
  if(data){
    return axios_t.get(url,  {
      params: {
        data: data
      }
    })
  }else{
    return axios_t.get(url)
  }
}


export function post (url, data) {
  // debugger
  return axios_t.post(url, data,)
}

export function file (url, data) {
  debugger
  return axios_t.post(url, data, 'blob'
    )
}

export function update (url, data) {
  return axios_t.post( url, data,
    {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
}
export function del (url) {
  return axios_t.post( url,
        {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
      }
    )
}