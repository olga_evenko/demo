// debugger
import Vue from 'vue';
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import BootstrapVue from 'bootstrap-vue'
import 'es6-promise/auto';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';

import vMediaQuery from 'v-media-query';
import ElTableWrapper from 'el-table-wrapper'
import VueResource from 'vue-resource'
import VueGoodTable from 'vue-good-table';
import Notifications from 'vue-notification'
import VModal from 'vue-js-modal'
import $ from 'jquery'
import VueKonva from 'vue-konva'
import VueFrame from 'vue-frame'

 //select c поиском
// import Breabcrumbs from 'vue-2-breadcrumbs';
import LongPress from 'vue-directive-long-press'


import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-good-table/dist/vue-good-table.css'


import * as backendAPI from './common/backendAPI'
import * as ticketPlase from './common/ticketPlase'
// import * as backendAPP from './common/backendAPP'
Vue.use(Vuetify)
import { VueSpinners } from '@saeris/vue-spinners'

Vue.use(vMediaQuery);
Vue.use(VueResource);
Vue.use(VueGoodTable);
Vue.use(Notifications)
Vue.use(VModal)
Vue.use(VueKonva)

// Vue.use(VueTelInput)
Vue.use(BootstrapVue)
Vue.use(VueSpinners)
Vue.directive( 'long-press', LongPress )


const http=Vue.http
Vue.config.productionTip = false

// debugger
// if(!!$('.app_init').length){
// 	Vue.prototype.$api = backendAPI
// }
// if(!!$('.tickets_init').length){
Vue.prototype.$api = backendAPI
// }

Vue.prototype.$ticketPlase = ticketPlase



import App_admin from './AppAdmin';
//import App_tick from './AppTicket';
import App_quality from './AppQuality';
import router_admin from './router/index.js';
//import router_tick from './router/tickets.js';
import router_quality from './router/quality.js';
import store_admin from './store/index.js';
//import store_tick from './store/tickets.js';

// import toastr from './toastr';


import * as types from './store/mutation-types'
var el_f = function() {
	if(!!$('.app_init').length){
		return '.app_init'
	}
//	if(!!$('.tickets_init').length){
//		return '#tickets_init'
//	}
	if(!!$('.quality_init').length){
		return '#quality_init'
	}
}

var store_f = function() {
	if(!!$('.app_init').length){
		return store_admin
	}
//	if(!!$('.tickets_init').length){
//		return store_tick
//	}
	if(!!$('.quality_init').length){
		return store_admin
	}
}
var router_f = function() {
	if(!!$('.app_init').length){
		return router_admin
	}
//	if(!!$('.tickets_init').length){
//		return router_tick
//	}
	if(!!$('.quality_init').length){
		return router_quality
	}
}

var App_f = function() {
	if(!!$('.app_init').length){
		return {App_admin}
	}
//	if(!!$('.tickets_init').length){
//		return {App_tick, VueFrame}
//	}
	if(!!$('.quality_init').length){
		return {App_quality, VueFrame}
	}
}
var template_f = function() {
	if(!!$('.app_init').length){
		return '<App_admin/>'
	}
//	if(!!$('.tickets_init').length){
//		return '<App_tick/>'
//	}
	if(!!$('.quality_init').length){
		return '<App_quality/>'
	}
}



export default new Vue({
  router:router_f(),
  store:store_f(),
  el: el_f(),
  template: template_f(),
  components: App_f(),
});
