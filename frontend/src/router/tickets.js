import Vue from 'vue';
import Router from 'vue-router';

import Ticket_Project from '../views/Tickets/Forms/Buy';
import Lost from '../views/Lost';


// import axios from 'axios'
import * as types from '../store/mutation-types'
import store from '../store/tickets.js';

import VueResource from 'vue-resource'
Vue.use(VueResource)
Vue.use(Router)

import ProjectsList from '../views/Tickets/ProjectsList';

const validateUrl = (to, from, next) => {

  var str = to.path.replace(/\//g, "")

  if(!!Number(str)){
    next();
  }else{
    next('');
  }
};

const routes = [
    {
      path: '/',
      component: ProjectsList,
      props: true,
      
    },
    {
      path: '/:id',
      component: Ticket_Project,
      beforeEnter: validateUrl,
      props: true,
    },

    {
      path: '*',
      component: ProjectsList,
      props: true,
    },

  ]

const router = new Router({
  routes
})

export default new Router({
  routes,
});
