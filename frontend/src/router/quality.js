import Vue from 'vue';
import Router from 'vue-router';
// import Ticket_Project from '../views/Tickets/TicketsProject';
// import Ticket_Project from '../views/Tickets/Forms/Buy';
// import Admin_Projects_News from '../views/Admin/News';
// import Admin_Projects_Events from '../views/Admin/Events';
// import Admin_Projects_Exponents from '../views/Admin/Exponents';

// import Login from '../views/Login';
// import Lost from '../views/Lost';


import QualityHome from '../views/Quality/QualityHome';
import Login from '../views/Login';


import axios from 'axios'
import * as types from '../store/mutation-types'
import store from '../store/index.js';

import VueResource from 'vue-resource'
Vue.use(VueResource)
Vue.use(Router)

import ProjectsList from '../views/Tickets/ProjectsList';


const requireAuthenticated = (to, from, next) => {
  // debugger
  
  var data = {
    token: store.state.token,
  }
  store.dispatch(types.REFRESH, {
    data: data,
  }).then(() => {
      if (!store.state.token) {
        next('/login');
      } else {

        if(verificatePerm()){
          next();
        }else{
          next();
          // window.location.hash = '#/'

        }
      }
    });
};


const verificatePerm = () => {
  // debugger
  store.state
  // if(store.state.user.permission.includes('kiosk_plus')){
  //   return false
  // }
  return true
  
};


const requireAuthenticatedLoginUrl = (to, from, next) => {

  if (!store.state.token) {
      next() 
  }
  else{
    window.location.hash = window.location.hash.replace('login','')
  }
};








// const validateUrl = (to, from, next) => {

//   var str = to.path.replace(/\//g, "")

//   if(!!Number(str)){
//     next();
//   }else{
//     next('');
//   }
// };

const routes = [
    {
      path: '/login',
      name:'login',
      component: Login,
      beforeEnter: requireAuthenticatedLoginUrl,
    },
    {
      path: '/',
      component: QualityHome,
      beforeEnter: requireAuthenticated,
      // beforeEnter: validateUrl1,
      props: true,    
    },
    // {
    //   path:'/basket',
    //       component: Ticket_Basket,
    //       props: true,
    // },
    // {
    //   path: '/:id',
    //   component: Ticket_Project,
    //   beforeEnter: validateUrl,
    //   props: true,
    // },

    {
      path: '*',
      component: ProjectsList,
      props: true,
    },

  ]

const router = new Router({
  routes
})

export default new Router({
//  mode: 'history',
  routes,
});
