import Vue from 'vue';
import Router from 'vue-router';
import Admin_Projects from '../views/Admin/Lists/Projects';
import Admin_Project from '../views/Admin/Forms/Project';


import Admin_Projects_Orders from '../views/Admin/Head/Orders';
import Admin_Projects_Forms_Data from '../views/Admin/Head/FormData';
// import Admin_Projects_News from '../views/Admin/News';
import Admin_Projects_Forms from '../views/Admin/Head/Forms';
// import Admin_Projects_Events from '../views/Admin/Events';
import Admin_Projects_Visitors from '../views/Admin/Head/Visitors';
// import Admin_Projects_Exponents from '../views/Admin/Exponents';
import Admin_Projects_Tickets from '../views/Admin/Head/Tickets';
import Admin_Projects_Reports from '../views/Admin/Head/Reports';
import Admin_Projects_Tickets_Plane from '../views/Admin/Forms/TicketsPlane';
import InoutHome_Activation from '../views/Admin/Inout/Barcode_Activation';


// import Admin_Projects_Business from '../components/Admin/ProjectsBusiness';
// import Admin_External_Users from '../views/Admin/ExternalUsers';
import InoutHome from '../views/Admin/Inout/InoutHome';
// import Admin_Cashier from '../components/Admin/Cashier';

// import Admin_News_Global from '../views/Admin/NewsGlobal';
// import Admin_Sponsor_Global from '../views/Admin/SponsorGlobal';
// import Admin_Book_Global from '../views/Admin/Book';


import Home from '../views/Admin/Home';

import Login from '../views/Login';
import Lost from '../views/Lost';
import KioskAdmin from '../views/Admin/Kiosk/KioskAdmin';
import QualityAdmin from '../views/Quality/QualityAdmin';
import QualityAnswer from '../views/Quality/QualityAnswer';


import axios from 'axios'
import * as types from '../store/mutation-types'
import store from '../store/index.js';

import VueResource from 'vue-resource'
Vue.use(VueResource)
Vue.use(Router)



const requireAuthenticated = (to, from, next) => {
  
  var data = {
    token: store.state.token,
  }
  store.dispatch(types.REFRESH, {
    data: data,
  }).then(() => {

      if (!store.state.token) {
        next('/login');
      } else {
        if(store.state.user.permission_name == 'quality'){
          window.location.href = window.location.origin + '/quality/#/'
        }else{
          next();
        }  
      }
    });
};


const verificatePerm = () => {
  store.state
  if(store.state.user.permission.includes('kiosk_plus')){
    return false
  }
  return true
  
};


const requireAuthenticatedLoginUrl = (to, from, next) => {
debugger
  if (!store.state.token) {
      next() 
  }
  else{
    window.location.hash = window.location.hash.replace('login','')
  }
};

const routes = [
    {
      path: '/login',
      name:'login',
      component: Login,
      beforeEnter: requireAuthenticatedLoginUrl,
    },
    {
      path: '/',
      component: Home,
      beforeEnter: requireAuthenticated,
    },
    {
      path: '/kiosk',
      component: KioskAdmin,
      beforeEnter: requireAuthenticated,
    },
    {
      path: '/quality/tune',
      component: QualityAdmin,
      beforeEnter: requireAuthenticated,
      props: true,
    },
    {
      path: '/quality/answer',
      component: QualityAnswer,
      beforeEnter: requireAuthenticated,
      props: true,
    },
    {
      path: '/project',
      component: Admin_Projects,
      beforeEnter: requireAuthenticated,
      props: true,
      children: [
        // {
        //   path:'news',
        //   component: Admin_News_Global,
        //   beforeEnter: requireAuthenticated,
        //   props: true,
        // },
        // {
        //   path:'sponsor',
        //   component: Admin_Sponsor_Global,
        //   beforeEnter: requireAuthenticated,
        //   props: true,
        // },
        // {
        //   path: 'book',
        //   component: Admin_Book_Global,
        //   beforeEnter: requireAuthenticated,
        //   props: true,
        // },
        {
          path: ':id',
          name:'id',
          component: Admin_Project,
          beforeEnter: requireAuthenticated,
          props: true,
          children: [
            // {
            //   path: 'news',
            //   component: Admin_Projects_News,
            //   beforeEnter: requireAuthenticated,
            //   props: true,},
            {
              path: 'forms',
              component: Admin_Projects_Forms,
              beforeEnter: requireAuthenticated,
              props: true,
            },
            // {
            //   path: 'events',
            //   component: Admin_Projects_Events,
            //   beforeEnter: requireAuthenticated,
            //   props: true,},
            {
              path: 'visitors',
              component: Admin_Projects_Visitors,
              beforeEnter: requireAuthenticated,
              props: true,},
            {
              path: 'orders',
              component: Admin_Projects_Orders,
              beforeEnter: requireAuthenticated,
              props: true,
            },
            {
              path: 'forms_data',
              component: Admin_Projects_Forms_Data,
              beforeEnter: requireAuthenticated,
              props: true,
            },
            // {
            //   path: 'exponents',
            //   component: Admin_Projects_Exponents,
            //   beforeEnter: requireAuthenticated,
            //   props: true,},
            {
              path: 'tickets',
              component: Admin_Projects_Tickets,
              beforeEnter: requireAuthenticated,
              props: true,
              children: [
                {
                  path: 'plane',
                  component: Admin_Projects_Tickets_Plane,
                  props: true,},
              ]},
            {
              path: 'reports',
              component: Admin_Projects_Reports,
              beforeEnter: requireAuthenticated,
              props: true,
            },
          ]
        },
      ]
    },
    // {
    //   path: '/external_users',
    //   component: Admin_External_Users,
    //   beforeEnter: requireAuthenticated,
    // },

    // {
    //   path: '/ticket_sale',
    //   component: Admin_Cashier,
    //   beforeEnter: requireAuthenticated,
    //   props: true,
    //   // children: [
    //   //   {
    //   //     path: ':id',
    //   //     component: InoutHome_Activation,
    //   //     beforeEnter: requireAuthenticated,
    //   //     props: true,}
    //   //   ]

    // },

    {
      path: '/barcode/:id',
      component: InoutHome_Activation,
      beforeEnter: requireAuthenticated,
      props: true,
      

    },
   
    {
      path: '/home',
      component: Home,
      beforeEnter: requireAuthenticated,
    },

    {
      path: '*',
      component: Lost,
    },
  ]

const router = new Router({
  routes
})

export default new Router({
//  mode: 'history',
  routes,
});
