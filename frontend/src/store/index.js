import Vuex from 'vuex'
import Vue from 'vue'
import * as types from './mutation-types'
import axios from 'axios'
import createPersistedState from 'vuex-persistedstate'
import router from '../router/index.js'

Vue.use(Vuex)

const colors=[
  {id:'#E53935', name:'red darken-1'},
  {id:'#D81B60', name:'pink darken-1'},
  {id:'#8E24AA', name:'purple darken-1'},
  {id:'#5E35B1', name:'deep-purple darken-1'},
  {id:'#3949AB', name:'indigo darken-1'},
  {id:'#1E88E5', name:'blue darken-1'},
  {id:'#039BE5', name:'light-blue darken-1'},
  {id:'#00ACC1', name:'cyan darken-1'},
  {id:'#00897B', name:'teal darken-1'},
  {id:'#43A047', name:'green darken-1'},
  {id:'#7CB342', name:'light-green darken-1'},
  {id:'#C0CA33', name:'lime darken-1'},
  {id:'#FB8C00', name:'orange darken-1'},
  {id:'#F4511E', name:'deep-orange darken-1'},
  {id:'#6D4C41', name:'brown darken-1'},
]

const state = {
  user: {},
  token: null,
  type_project: false,
  colors:colors,
}

const mutations = {
  [types.LOGIN]: (state, payload) => {
    state.token = payload.token
    state.user = payload.user
    router.push(payload.redirect)
  },
  [types.LOGOUT]: (state, payload) => {
    state.token = null
    state.user = {}
    router.push(payload.redirect)
  },
  [types.TITLE]: (state, data) => {
    state.title = data
  },
  [types.REFRESH]: (state, payload) => {
    state.token = payload.token
  },
  [types.TYPE_PROJECT]: (state, value) => {
    state.value_project = value
  },
  [types.BUSINESS_PROJECT]: (state, value) => {
    state.is_business = value
  },
}

const actions = {
  [types.LOGIN] ({ commit }, payload) {

    payload.credential.user['csrfmiddlewaretoken'] = window.csrf_token
    // debugger

    axios.post('../api/token/login/', payload.credential.user)
    .then(response => {
      
      if (response.data.user.token) {
        var mutationPayload = {}
        mutationPayload.token = response.data.user.token
        mutationPayload.user = JSON.parse(atob(response.data.user.token.split('.')[1]))
        mutationPayload.user.permission = response.data.user.permission
        mutationPayload.user.authenticated = true
        mutationPayload.user.error = false
        mutationPayload.redirect = payload.redirect

        if(response.data.user.permission.indexOf('admin_plus')>=0){
          mutationPayload.user.permission_name = 'admin'
        }else if(response.data.user.permission.indexOf('expo_mg_plus')>=0){
          mutationPayload.user.permission_name = 'expo'
        }else if(response.data.user.permission.indexOf('business_mg_plus')>=0){
          mutationPayload.user.permission_name = 'business'
        }else if(response.data.user.permission.indexOf('base_user_plus')>=0){
          mutationPayload.user.permission_name = 'base'
        }else if(response.data.user.permission.indexOf('kiosk_plus')>=0){
          mutationPayload.user.permission_name = 'kiosk'
        }else if(response.data.user.permission.indexOf('inout_plus')>=0){
          mutationPayload.user.permission_name = 'inout'
        }else if(response.data.user.permission.indexOf('quality_plus')>=0){
          mutationPayload.user.permission_name = 'quality'
        }

        commit(types.LOGIN, mutationPayload)
      }
    })
    .catch(e => {
      var mutationPayload = {}
      mutationPayload.user = {}
      mutationPayload.user.authenticated = false
      mutationPayload.user.error = true
      commit(types.LOGIN, mutationPayload)
    })

  },
    [types.LOGOUT] ({ commit }, payload) {
      if (payload.data) {
        var mutationPayload = {}
        console.log('mutationPayload', mutationPayload)
        mutationPayload.redirect = '/login'
        commit(types.LOGOUT, mutationPayload)
      }
  },

  [types.REFRESH] ({ commit }, payload) {
    axios.post('../api/token_refresh/', payload.data)
    .then(response => {
      if (response.data.token) {
        var mutationPayload = {}
        mutationPayload.token = response.data.token
        commit(types.REFRESH, mutationPayload)
      }
    })
    .catch(e => {
        var mutationPayload = {}
        mutationPayload.redirect = '/login'
        commit(types.LOGOUT, mutationPayload)
    })
  },

  [types.TYPE_PROJECT] ({ commit }, value) {
    commit(types.TYPE_PROJECT, value)
    
  },

  [types.BUSINESS_PROJECT] ({ commit }, value) {
    commit(types.BUSINESS_PROJECT, value)
    
  }
}

const store = new Vuex.Store({
  state,
  mutations,
  actions,
  plugins: [
    createPersistedState()
  ]
})

export default store
