import Vuex from 'vuex'
import Vue from 'vue'
import * as types from './mutation-types'
import axios from 'axios'
import createPersistedState from 'vuex-persistedstate'
import router from '../router/tickets.js'

Vue.use(Vuex)
// debugger
const state = {
  user: {},
  token: null,
  type_project: false,
}

const mutations = {
  [types.AUTH]: (state, payload) => {
    debugger
    // state.token = payload.token
    // state.user = payload.user
    // router.push(payload.redirect)
  },
  [types.LOGIN]: (state, payload) => {
    debugger
    state.token = payload.token
    state.user = payload.user
    router.push(payload.redirect)
  },
  [types.LOGOUT]: (state, payload) => {
    state.token = null
    state.user = {}
    router.push(payload.redirect)
  },
  [types.TITLE]: (state, data) => {
    state.title = data
  },
  [types.REFRESH]: (state, payload) => {
    state.token = payload.token
  },
  [types.TYPE_PROJECT]: (state, value) => {
    state.type_project = value[0]
    state.value_project = value[1]
  },

}

const actions = {
  [types.LOGIN] ({ commit }, payload) {
    // debugger
    axios.post('../api/token/login/', payload.credential.user)
    .then(response => {
      // debugger
      if (response.data.user.token) {

        var mutationPayload = {}
        mutationPayload.token = response.data.user.token
        mutationPayload.user = JSON.parse(atob(response.data.user.token.split('.')[1]))
        // mutationPayload.user.username = response.data.user.username
        mutationPayload.user.authenticated = true
        console.log('mutationPayload', mutationPayload)
        mutationPayload.redirect = payload.redirect
        commit(types.LOGIN, mutationPayload)
      }
    })
    .catch(e => {
      console.log(e)
    })
  },
  [types.AUTH] ({ commit }, payload) {
    
    axios.post('../api/token/auth/', payload.credential)
    .then(response => {
   
      // if (response.data.user.token) {

      //   var mutationPayload = {}
      //   mutationPayload.token = response.data.user.token
      //   mutationPayload.user = JSON.parse(atob(response.data.user.token.split('.')[1]))
      //   // mutationPayload.user.username = response.data.user.username
      //   mutationPayload.user.authenticated = true
      //   console.log('mutationPayload', mutationPayload)
      //   mutationPayload.redirect = payload.redirect
      //   commit(types.LOGIN, mutationPayload)
      // }
    })
    .catch(e => {
      console.log(e)
    })
  },
    [types.LOGOUT] ({ commit }, payload) {
      if (payload.data) {
        var mutationPayload = {}
        console.log('mutationPayload', mutationPayload)
        mutationPayload.redirect = '/login'
        commit(types.LOGOUT, mutationPayload)
      }
  },
  [types.REFRESH] ({ commit }, payload) {
    // debugger
    axios.post('../api/token_refresh/', payload.data)
    .then(response => {
      // debugger
      if (response.data.token) {
        var mutationPayload = {}
        mutationPayload.token = response.data.token
        commit(types.REFRESH, mutationPayload)
      }
    })
    .catch(e => {
      // debugger
        var mutationPayload = {}
        mutationPayload.redirect = '/login'
        commit(types.LOGOUT, mutationPayload)
    })
  },
  [types.TYPE_PROJECT] ({ commit }, value) {
    commit(types.TYPE_PROJECT, value)
    
  }
}


const store = new Vuex.Store({
  state,
  mutations,
  actions,
  plugins: [
    createPersistedState()
  ]
})

export default store
