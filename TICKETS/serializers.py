from rest_framework import serializers
from .models import *
from datetime import *
from django.utils import timezone


class CustomDateTimeField(serializers.DateTimeField):
    def to_representation(self, value):
        tz = timezone.get_default_timezone()
        value = timezone.localtime(value, timezone=tz)
        return super().to_representation(value)


class TicketsCategorySerializer(serializers.ModelSerializer):
    count_all = serializers.IntegerField(allow_null=True)
    # ticket_img = serializers.SerializerMethodField("_is_ticket_img")

    class Meta:
        model = TicketsCategory
        fields = ('id', 'name', 'is_weekend', 'type_in', 'count', 'count_all', 'price', 'color', 'project', 'description', 'discount', 'ticket_img')

    # def _is_ticket_img(self, obj):
    #     if obj.ticket_img:
    #         aaa = obj.ticket_img
    #         return obj.ticket_img.url
    #     return ""


class TicketsCategorySerializerList(serializers.ModelSerializer):
    count_all = serializers.IntegerField(allow_null=True)
    type_in = serializers.SerializerMethodField("_is_type_in")
    type_in_option = serializers.CharField(read_only=True, source="type_in")
    ticket_img = serializers.SerializerMethodField("_is_ticket_img")

    class Meta:
        model = TicketsCategory
        fields = ('id', 'name', 'is_weekend', 'type_in', 'type_in_option', 'count', 'count_all', 'price', 'color', 'project', 'description', 'discount', 'ticket_img')

    def _is_type_in(self, obj):
        if obj.type_in:
            for i in TicketsCategory.TYPE_IN:
                if obj.type_in == i[0]:
                    return i[1]
            return ''

    def _is_ticket_img(self, obj):
        if obj.ticket_img:
            aaa = obj.ticket_img
            return obj.ticket_img.url
        return ""


class ReceptionsSerializer(serializers.ModelSerializer):

    user = serializers.SerializerMethodField("_is_user")
    ticket_category_list = serializers.SerializerMethodField("_is_ticket_category_list")

    class Meta:
        model = Receptions
        fields = ('id', 'name', 'project', 'user', 'ticket_category', 'ticket_category_list')
        # fields = ('id', 'name', 'project', 'date_start', 'date_end', 'time_start', 'time_end', 'user', 'ticket_category', 'ticket_category_list')

    def _is_user(self, obj):
        if obj.user:
            return obj.user.username
        return ""

    def _is_ticket_category_list(self, obj):
        if obj.ticket_category:
            ticket_category = obj.ticket_category.all()
            ticket_category_list =[]
            for tk in ticket_category:
                ticket_category_list.append(tk.name)


            return ticket_category_list
        return ""

class ReceptionsSerializerSave(serializers.ModelSerializer):

    class Meta:
        model = Receptions
        fields = ('id', 'name', 'project', 'user', )
        # fields = ('id', 'name', 'project', 'date_start', 'date_end', 'time_start', 'time_end', 'user', )


class ReceptionsSerializerForm(serializers.ModelSerializer):

    # user = serializers.SerializerMethodField("_is_user")

    class Meta:
        model = Receptions
        fields = ('id', 'name', 'project',  'user', 'ticket_category')
        # fields = ('id', 'name', 'project', 'date_start', 'date_end', 'time_start', 'time_end', 'user', 'ticket_category')

    def _is_user(self, obj):
        if obj.user:
            return obj.user.username
        return ""


class TicketsPlaneSerializer(serializers.ModelSerializer):

    class Meta:
        model = TicketsPlane
        fields = ('id', 'project', 'edit', 'row', 'plase', 'json_tickets')


class TicketsSerializer(serializers.ModelSerializer):
    ticket_graph = serializers.SerializerMethodField("_is_ticket_graph")
    ticket_check = serializers.SerializerMethodField('_is_ticket_check')

    class Meta:
        model = Tickets
        fields = ('uuid', 'number', 'code', 'category', 'ticket_graph', 'ticket_check', 'count_printing' )

    def _is_ticket_graph(self, obj):
        if obj.ticket_graph:
            return obj.ticket_graph.url
        return ""

    def _is_ticket_check(self, obj):
        if obj.ticket_check:
            return obj.ticket_check.url
        return ""


class TicketsActivateSerializer(serializers.ModelSerializer):
    buyer = serializers.SerializerMethodField("_is_buyer")
    category = serializers.SerializerMethodField("_is_category")
    state = serializers.SerializerMethodField("_is_state")
    # date_activate = serializers.SerializerMethodField("_is_date_activate")

    class Meta:
        model = Tickets
        fields = ('number', 'code', 'category', 'state', 'date_activate', 'buyer')


    def _is_buyer(self, obj):
        if obj.order:
            return obj.order.buyer
        return ''

    def _is_category(self, obj):
        if obj.category:
            return obj.category.name
        return ""

    def _is_state(self, obj):
        if obj.state:
            return 'Активирован'
        return "Не активирован"

    def _is_date_activate(self, obj):
        if obj.date_activate:
            return obj.date_activate.strftime("%d-%m-%Y\n %H:%M:%S")
        return ""

class TicketsReportSerializer(serializers.ModelSerializer):
    buyer = serializers.SerializerMethodField("_is_buyer")
    category = serializers.SerializerMethodField("_is_category")
    state = serializers.SerializerMethodField("_is_state")
    status = serializers.SerializerMethodField("_is_status")
    # date_activate = serializers.SerializerMethodField("_is_date_activate")

    class Meta:
        model = Tickets
        fields = ('number', 'code', 'category', 'state','status', 'date_create', 'date_activate', 'buyer')


    def _is_buyer(self, obj):
        if obj.order:
            return obj.order.buyer
        return ''

    def _is_category(self, obj):
        if obj.category:
            return obj.category.name
        return ""

    def _is_state(self, obj):
        if obj.state:
            return 'Активирован'
        return "Не активирован"

    def _is_status(self, obj):
        if obj.status:
            return 'Действующий'
        return "Отменен"

    # def _is_date_activate(self, obj):
    #     if obj.date_activate:
    #         return obj.date_activate.strftime("%d-%m-%Y\n %H:%M:%S")
    #     return ""



class PromocodeSerializer(serializers.ModelSerializer):
    # state = serializers.SerializerMethodField("_is_state")
    # state_email = serializers.SerializerMethodField("_is_state_email")
    # count_ticket = serializers.SerializerMethodField("_is_count_ticket")
    # payment_method = serializers.SerializerMethodField("_is_payment_method")
    # date = CustomDateTimeField(read_only=True)

    tickets_category = serializers.SerializerMethodField("_is_tickets_category")
    contact = serializers.SerializerMethodField("_is_contact")
    date_create = CustomDateTimeField(read_only=True)
    # date_start = CustomDateTimeField(read_only=True)
    # date_end = CustomDateTimeField(read_only=True)

    class Meta:
        model = Promocode
        fields = ('id', 'code', 'project', 'date_create', 'date_activate', 'date_start', 'date_end', 'type', 'value', 'contact', 'count', 'summ_all', 'tickets_category')

    def _is_tickets_category(self, obj):
        if obj.id and obj.tickets_category:
            temp = []
            for categ in obj.tickets_category.all():
                temp.append(categ.id)

            return temp

        return []
    def _is_contact(self, obj):
        if obj.contact:
            return obj.contact

        return []



class PromocodeSerializerUpdate(serializers.ModelSerializer):
    date_create = CustomDateTimeField(read_only=True)

    class Meta:
        model = Promocode
        fields = ('id', 'code', 'project', 'date_create', 'date_activate', 'date_start', 'date_end', 'type', 'value', 'contact', 'count', 'summ_all')



class OrderSerializer(serializers.ModelSerializer):
    state = serializers.SerializerMethodField("_is_state")
    state_email = serializers.SerializerMethodField("_is_state_email")
    count_ticket = serializers.SerializerMethodField("_is_count_ticket")
    payment_method = serializers.SerializerMethodField("_is_payment_method")
    date = CustomDateTimeField(read_only=True)

    class Meta:
        model = Order
        fields = ('id', 'number', 'project', 'date', 'uuid', 'state', 'state_email', 'buyer', 'pan', 'cardAuthInfo', 'amount', 'count_ticket', 'payment_method')

    def _is_state(self, obj):
        if obj.state:
            if obj.state==2:
                return 'Отменен'
            return 'Оплачен'
        return "Не оплачен"

    def _is_state_email(self, obj):
        if obj.state_email:
            if obj.state_email==1:
                return 'Отправлен'
            if obj.state_email==2:
                return 'Повтор'
            # return 'Не отправлен'
        return ""

    def _is_count_ticket(self, obj):
        if obj.id:
            try:
                count = Tickets.objects.filter(order__id = obj.id).count()
                return count
            except Tickets.DoesNotExist:
                pass
        return 0

    def _is_payment_method(self, obj):
        if obj.payment_method:
            for p in Order.PAYMENT_METHOD:
                if obj.payment_method == p[0]:
                    return p[1]
        return ''



class OrderSerializerData(serializers.ModelSerializer):
    # date = serializers.DateTimeField()
    date = CustomDateTimeField(read_only=True)
    # date = serializers.SerializerMethodField("_is_date")

    class Meta:
        model = Order
        fields = ('id', 'number', 'project', 'date', 'uuid', 'state', 'buyer', 'pan', 'cardAuthInfo', 'amount', 'surcharge')

    # def _is_date(self, obj):
    #     a = 0


class VisitorsSerializer(serializers.ModelSerializer):
    order = serializers.CharField(read_only=True, source="order.number")
    state = serializers.BooleanField()
    state_text = serializers.SerializerMethodField("_is_state_text")
    order_state = serializers.SerializerMethodField("_is_order_state")

    buyer = serializers.SerializerMethodField("_is_buyer")
    category = serializers.SerializerMethodField("_is_category")
    category_id = serializers.CharField(read_only=True, source="category.id")
    category_price = serializers.CharField(read_only=True, source="category.price")
    category_weekend_text = serializers.SerializerMethodField('_is_weekend')
    category_weekend = serializers.BooleanField(source="category.is_weekend")
    barcode = serializers.SerializerMethodField('_is_barcode')


    class Meta:
        model = Tickets
        fields = '__all__'
        # fields = ('code', 'order', 'order_state',  'number', 'category', 'category_price', 'buyer', 'state', 'barcode')

    def _is_order_state(self, obj):
        if obj.order.state:
            if obj.order.state==2:
                return 'Отменен'
            return 'Оплачен'
        return "Не оплачен"

    def _is_weekend(self, obj):
        if obj.category.is_weekend:
            return 'Выходной'
        return "Будний"

    def _is_barcode(self, obj):
        if obj.code:
            return '/media/barcode/barcode_%s.png' %obj.code
        return ''

    def _is_buyer(self, obj):
        if obj.order:
            return obj.order.buyer
        return ""
    def _is_state_text(self, obj):
        if obj.state:
            return 'Активирован'
        return "Не активирован"

    def _is_category(self, obj):
        if obj.category:

            return obj.category.name
        return ""


class VisitorsSerializerList(serializers.ModelSerializer):
    order = serializers.CharField(read_only=True, source="order.number")
    state = serializers.SerializerMethodField("_is_state")
    order_state = serializers.SerializerMethodField("_is_order_state")

    buyer = serializers.SerializerMethodField("_is_buyer")
    category = serializers.SerializerMethodField("_is_category")
    category_id = serializers.CharField(read_only=True, source="category.id")
    category_price = serializers.CharField(read_only=True, source="category.price")
    category_weekend_text = serializers.SerializerMethodField('_is_weekend')
    category_weekend = serializers.BooleanField(source="category.is_weekend")
    barcode = serializers.SerializerMethodField('_is_barcode')
    plase = serializers.SerializerMethodField('_is_plase')
    order__payment_method = serializers.SerializerMethodField('_is_payment_method')
    date_create = CustomDateTimeField(read_only=True)


    class Meta:
        model = Tickets
        fields = '__all__'
        # fields = ('code', 'order', 'order_state',  'number', 'category', 'category_price', 'buyer', 'state', 'barcode')

    def _is_order_state(self, obj):
        if obj.order.state:
            if obj.order.state==2:
                return 'Отменен'
            return 'Оплачен'
        return "Не оплачен"

    def _is_weekend(self, obj):
        if obj.category.is_weekend:
            return 'Выходной'
        return "Будний"

    def _is_barcode(self, obj):
        if obj.code:
            return '/media/barcode/barcode_%s.png' %obj.code
        return ''

    def _is_buyer(self, obj):
        if obj.order:
            return obj.order.buyer
        return ""
    def _is_state(self, obj):
        if obj.state:
            return 'Активирован'
        return "Не активирован"

    def _is_category(self, obj):
        if obj.category:

            return obj.category.name
        return ""
    def _is_plase(self, obj):
        if obj.plase and obj.plase!='{}':
            return '%s/%s' %(obj.plase['row'], obj.plase['plase'] )
        return ""

    def _is_payment_method(self, obj):
        for i in Order.PAYMENT_METHOD:
            if obj.order.payment_method == i[0]:
                return i[1]
        return ''



class VisitorsKioskSerializerList(serializers.ModelSerializer):
    order = serializers.CharField(read_only=True, source="order.number")
    state = serializers.BooleanField()
    state_text = serializers.SerializerMethodField("_is_state_text")
    order_state = serializers.SerializerMethodField("_is_order_state")
    buyer = serializers.SerializerMethodField("_is_buyer")
    category = serializers.SerializerMethodField("_is_category")
    category_id = serializers.CharField(read_only=True, source="category.id")
    category_price = serializers.CharField(read_only=True, source="category.price")
    plase = serializers.SerializerMethodField('_is_plase')
    payment_method = serializers.SerializerMethodField('_is_payment_method')
    pan = serializers.SerializerMethodField('_is_pan')
    date_create = CustomDateTimeField(read_only=True)
    date_activate = CustomDateTimeField(read_only=True)


    class Meta:
        model = Tickets
        fields = '__all__'
        # fields = ('code', 'order', 'order_state',  'number', 'category', 'category_price', 'buyer', 'state', 'barcode')

    def _is_order_state(self, obj):
        if obj.order.state:
            if obj.order.state==2:
                return 'Отменен'

            for i in Order.PAYMENT_METHOD:
                if obj.order.payment_method == i[0]:
                    return 'Оплачен ' + i[1]
            return 'Оплачен'
        return "Не оплачен"

    def _is_weekend(self, obj):
        if obj.category.is_weekend:
            return 'Выходной'
        return "Будний"

    def _is_buyer(self, obj):
        if obj.order:
            return obj.order.buyer
        return ""

    def _is_state_text(self, obj):
        if obj.state:
            return 'Активирован'
        return ""

    def _is_category(self, obj):
        if obj.category:
            return obj.category.name
        return ""

    def _is_plase(self, obj):
        if obj.plase and obj.plase!='{}':
            return '%s/%s' %(obj.plase['row'], obj.plase['plase'] )
        return ""

    def _is_payment_method(self, obj):
        for i in Order.PAYMENT_METHOD:
            if obj.order.payment_method == i[0]:
                return i[1]
        return ''

    def _is_pan(self, obj):
        if obj.order.pan:
            return obj.order.pan
        return ""


class Forms_DataSerializers(serializers.ModelSerializer):
    class Meta:
        model=Forms_Data
        fields = ('id', 'ticket', 'date', 'data')

class Forms_DataSerializersView(serializers.ModelSerializer):
    date = CustomDateTimeField(read_only=True)
    class Meta:
        model=Forms_Data
        fields = ('id', 'ticket', 'date', 'data')