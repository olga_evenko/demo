from django.urls import path, include
from rest_framework import routers
from TICKETS import views

from django.conf.urls.static import static
from rest_framework.urlpatterns import format_suffix_patterns
from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView


from .views import *
from TICKETS.views import *
from TICKETS import urls as t_url

router = DefaultRouter()
router.register(r'category', TicketsCategoryList, 'all')
router.register(r'buy', TicketsBuy, 'all')
router.register(r'print', TicketsPrint, 'all')

urlpatterns = [

    path(r'', include(router.urls)),
]

