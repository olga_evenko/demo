import logging
import random

import barcode
from django.urls import reverse
from django.core.mail import send_mail
from django.contrib.auth import get_user_model

from django.core.mail import EmailMultiAlternatives
from .models import *
from django.conf import settings

from django.core.mail import send_mail
import barcode
from barcode.writer import ImageWriter
from PIL import ImageFont
from PIL import Image
from PIL import ImageDraw

from celery.task import periodic_task, task
from expoBitPlus.celery import app
from .models import Order
import json
import requests
from django.conf import settings


@task(name="sum_two_numbers")
def add(x):
    return x

@task
def send_code_auth(email, code ):

    theme = 'Подтверждение e-mail'
    body_letter = '''Здравствуйте! Ваш код авторизации %s .''' %(code)

    send_mail(theme, body_letter, settings.EMAIL_HOST_USER, [email])

    # subject, from_email, to = 'Пожалуйста, подтвердите свой почтовый адрес', 'robot@donexpocentre.ru', email
    # html_content = '''
    #     <div id='content style="width:600px">
    #         <h4  style="text-align: center; color:#009688;">Здравствуйте!</h4>
    #         <h5>Пожалуйста, подтвердите свой почтовый адрес. Ваш код подтверждения %s</h5>
    #
    #         <span style='font-size:10px'>Письмо сформировано автоматически. Если вы не пытались получить билет, просто проигнорируйте данное сообщение.</span>
    #
    #     </div>''' % (code)
    # msg = EmailMultiAlternatives(subject, '', from_email, [to])
    # msg.attach_alternative(html_content, "text/html")

    # msg.send()


@task
def send_ticket(orderId, email, host):

    C128 = barcode.get_barcode_class('code128')
    errorSratus = {'code': 0}

    base_dir = host
    # base_dir = 'tickets.donexpocentre.ru'

    try:

        order = Order.objects.get(uuid=orderId)
        tickets = Tickets.objects.filter(order_id=order)
        subject, from_email, to = 'Билеты', 'robot@donexpocentre.ru', email
        msg = EmailMultiAlternatives(subject, '', from_email, [to])

        html_content='''
        <div id='content'>
            <div style="text-align: center;">
                <h3 style="color:#5e2c81">Благодарим Вас за покупку!</h3>
                <h4>Заказ номер №{1} был оплачен</h4>
                <h4>Для посещения мероприятия необходимо предъявить билеты в распечанном или электронном виде! Версия для печати: <a href="https://{0}/ticket/print/{1}">тут</a></h4>
            </div>
        
            <div style="text-align: center;">
                <h4>Билеты</h4>
            '''.format(base_dir, order.number)

        for t in tickets:

            if t.category.ticket_img:
                url_ticket = t.category.ticket_img.name
            else:
                url_ticket = t.category.project.ticket_logo.name
            ticket_category = TicketsCategory.objects.get(id=t.category_id)

            # ticket_kiosk.delay(ticket_category, t)

            writer = ImageWriter()
            writer.quiet_zone = 0.1
            writer.dpi = 400
            writer.module_width = 0.5

            c128 = C128(t.code, writer=writer)
            c128.save(settings.BASE_DIR +'/media/barcode/barcode_' + t.code)

            fnt = ImageFont.truetype(settings.BASE_DIR + '/static/fonts/DejaVuSansMono.ttf', 22)
            fnt28 = ImageFont.truetype(settings.BASE_DIR + '/static/fonts/DejaVuSansMono.ttf', 28)
            kiosk_tick = Image.new("RGB", size=(500, 500), color=(255, 255, 255, 0))

            if url_ticket:

                result = Image.new("RGB", size=(1500, 1500), color=(255, 255, 255, 0))
                background = Image.open(settings.BASE_DIR +'/media/' + url_ticket)
                background.thumbnail((1500, 1500))
                w_background, h_background = background.size

                draw = ImageDraw.Draw(background)
                if ticket_category.project.is_weekend:
                    text = '%s %s, %s руб. %s №%s' %(ticket_category.name[:30], ('Выходной день' if ticket_category.type_in=='weekend' else  'Будний день'), str(ticket_category.price), email[:30], t.code )
                else:
                    text = '%s  %s руб. %s №%s' %(ticket_category.name[:30], str(ticket_category.price), email[:30], t.code )

                if t.plase and type(t.plase)!=str:
                    text =  text +' ряд: %s место: %s' %( t.plase['row'], t.plase['plase'])
                draw.multiline_text((10, 5), text,font=fnt, fill=(0, 0, 0),align="left")
                result.paste(background, (0, 0))

                result = result.rotate(-90)
                result = result.crop((result.size[1]-h_background, 0,  result.size[0], result.size[1],))

            else:
                result = Image.new("RGB", size=(350, 500), color=(255, 255, 255, 0))


            im1 = Image.open(settings.BASE_DIR +'/media/barcode/barcode_' + t.code + ".png")

            result.paste(im1, (result.size[0]//2 - im1.size[0]//2, result.size[1] - im1.size[1]+45))
            result.save(settings.BASE_DIR +'/media/barcode/c128_' + t.code + ".png")

            html_content +='''
                    <img style="margin:10px;max-width: 400px; max-height: 450px; border: 1px solid #692b82;" src='https://{0}/media/barcode/c128_{1}.png'/>
                '''.format(base_dir, t.code)

        html_content +='''</div>
                <div>
                    <div style="padding: 20px;margin: 10px;
                                max-width: 650px;margin-left: auto;
                                margin-right: auto; border: 5px dotted #c31a84;">
                        <h4>Обратите внимание</h4>
                        <ul type="square">
                            <li><span>Штрих-код, указанный на билете, действителен только для однократного прохода на мероприятие.					
                                </span></li>
                            <li><span>Не допускайте перепечатки и копирования билета посторонними лицами, так как они могут воспользоваться им раньше Вас!	
                                </span></li>
                            <li><span>Не выкладывайте фото Электронного билета в социальные сети и не пересылайте их третьим лицам, во избежание мошеннических действий.
                                </span></li>	
                        </ul>
                    </div>
                </div>
                <div style='padding: 10px; background: #eeeeee; height: 100px;
                        max-width: 650px;margin-left: auto;
                        margin-right: auto; border: 5px dotted #5e2c80;'>
                    <p style="color: #5e2c81; text-align: center;">
                        <span style=''>ПО ВОПРОСАМ ПОСЕЩЕНИЯ: +7(863)268–77-59 или <a class="link" href="mailto:marketing@donexpocentre.ru">marketing@donexpocentre.ru </a> </span>	
                    </p>
                    <p style='text-align: center;'>Организатор: © 2018.  <a class="link-underline" href="https://donexpocentre.ru" target="_blank">ДОНЭКСПОЦЕНТР</a>
                    </p>  
                </div> 
        </div>'''
        msg.attach_alternative(html_content, "text/html")

        if order.state_email==0:
            order.state_email = 1
        else:
            order.state_email = 2
        order.save()
        errorStatus = {'code': 0, 'e': 'Успешно'}

    except Exception as e:
        print(e)
        if order:
            order.state_email = 0
            order.save()
        errorStatus = {'code':1, 'e':e }
        subject, from_email, to = 'Билеты', 'robot@donexpocentre.ru', email
        html_content = '<p>Во время покупки билета произошла ошибка, для получения билета свяжитесь с менеджером по телефону +7 (863) 268–77-59 или напишите на почту marketing@donexpocentre.ru </p>'
        msg = EmailMultiAlternatives(subject, '', from_email, [to])
        msg.attach_alternative(html_content, "text/html")

    msg.send()
    return errorStatus


@task
def send_ticket_free(id, email, host):
    C128 = barcode.get_barcode_class('code128')
    errorSratus = {'code': 0}

    base_dir = host
    # base_dir = 'tickets.donexpocentre.ru'

    try:

        order = Order.objects.get(id=id)
        # assert False
        tickets = Tickets.objects.filter(order_id=order)
        subject, from_email, to = 'Билеты', 'robot@donexpocentre.ru', email
        msg = EmailMultiAlternatives(subject, '', from_email, [to])

        html_content = '''
        <div id='content'>
            <div style="text-align: center;">
                <h3 style="color:#5e2c81">Благодарим Вас за регистрацию!</h3>
                <h4>Для посещения мероприятия необходимо предъявить билеты в распечанном или электронном виде! Версия для печати: <a href="https://{0}/ticket/print/{1}">тут</a></h4>
            </div>

            <div style="text-align: center;">
                <h4>Билеты</h4>
            '''.format(base_dir, order.number)

        for t in tickets:

            url_ticket = t.category.project.ticket_logo.name
            ticket_category = TicketsCategory.objects.get(id=t.category_id)

            writer = ImageWriter()
            writer.quiet_zone = 0.1
            writer.dpi = 400
            writer.module_width = 0.5

            c128 = C128(t.code, writer=writer)
            c128.save(settings.BASE_DIR + '/media/barcode/barcode_' + t.code)

            fnt = ImageFont.truetype(settings.BASE_DIR + '/static/fonts/DejaVuSansMono.ttf', 22)

            # ticket_kiosk.delay(ticket_category, t)

            if url_ticket:
                result = Image.new("RGB", size=(1500, 1500), color=(255, 255, 255, 0))
                background = Image.open(settings.BASE_DIR + '/media/' + url_ticket)
                background.thumbnail((1500, 1500))
                w_background, h_background = background.size

                draw = ImageDraw.Draw(background)
                if ticket_category.project.is_weekend:
                    text = '%s %s, %s №%s' % (
                    ticket_category.name[:30], ('Выходной день' if ticket_category.type_in=='weekend' else 'Будний день'), email[:30], t.code)
                else:
                    text = '%s  %s №%s' % (
                    ticket_category.name[:30], email[:30], t.code)

                if t.plase and type(t.plase) != str:
                    text = text + ' ряд: %s место: %s' % (t.plase['row'], t.plase['plase'])
                draw.multiline_text((10, 5), text, font=fnt, fill=(0, 0, 0), align="left")
                result.paste(background, (0, 0))

                result = result.rotate(-90)
                result = result.crop((result.size[1] - h_background, 0, result.size[0], result.size[1],))

            else:
                result = Image.new("RGB", size=(350, 500), color=(255, 255, 255, 0))

            im1 = Image.open(settings.BASE_DIR + '/media/barcode/barcode_' + t.code + ".png")

            result.paste(im1, (result.size[0] // 2 - im1.size[0] // 2, result.size[1] - im1.size[1] + 45))
            result.save(settings.BASE_DIR + '/media/barcode/c128_' + t.code + ".png")

            html_content += '''
                    <img style="margin:10px;max-width: 400px; max-height: 450px; border: 1px solid #692b82;" src='https://{0}/media/barcode/c128_{1}.png'/>
                '''.format(base_dir, t.code)

        html_content += '''</div>
                <div>
                    <div style="padding: 20px;margin: 10px;
                                max-width: 650px;margin-left: auto;
                                margin-right: auto; border: 5px dotted #c31a84;">
                        <h4>Обратите внимание</h4>
                        <ul type="square">
                            <li><span>Штрих-код, указанный на билете, действителен только для однократного прохода на мероприятие.					
                                </span></li>
                            <li><span>Не допускайте перепечатки и копирования билета посторонними лицами, так как они могут воспользоваться им раньше Вас!	
                                </span></li>
                            <li><span>Не выкладывайте фото Электронного билета в социальные сети и не пересылайте третьим лицам, во избежание мошеннических действий.
                                </span></li>	
                        </ul>
                    </div>
                </div>
                <div style='padding: 10px; background: #eeeeee; height: 100px;
                        max-width: 650px;margin-left: auto;
                        margin-right: auto; border: 5px dotted #5e2c80;'>
                    <p style="color: #5e2c81; text-align: center;">
                        <span style=''>ПО ВОПРОСАМ ПОСЕЩЕНИЯ: +7(863)268–77-59 или <a class="link" href="mailto:marketing@donexpocentre.ru">marketing@donexpocentre.ru </a> </span>	
                    </p>
                    <p style='text-align: center;'>Организатор: © 2018.  <a class="link-underline" href="https://donexpocentre.ru" target="_blank">ДОНЭКСПОЦЕНТР</a>
                    </p>  
                </div> 
        </div>'''
        msg.attach_alternative(html_content, "text/html")

        if order.state_email == 0:
            order.state_email = 1
        else:
            order.state_email = 2
        order.save()
        errorStatus = {'code': 0, 'e': 'Успешно'}

    except Exception as e:
        print(e)
        order = Order.objects.get(id=id)
        if order:
            order.state_email = 0
            order.save()
        errorStatus = {'code': 1, 'e': e}
        subject, from_email, to = 'Билеты', 'robot@donexpocentre.ru', email
        html_content = '<p>Во время регистрации произошла ошибка, для получения билета свяжитесь с менеджером по телефону +7 (863) 268–77-59 или напишите на почту marketing@donexpocentre.ru </p>'
        msg = EmailMultiAlternatives(subject, '', from_email, [to])
        msg.attach_alternative(html_content, "text/html")

    msg.send()
    return errorStatus


@task
def send_ticket_expobit(id, email, host):
    C128 = barcode.get_barcode_class('code128')
    errorSratus = {'code': 0}

    base_dir = host
    # base_dir = 'tickets.donexpocentre.ru'

    try:
        order = Order.objects.get(id=id)
        tickets = Tickets.objects.filter(order_id=order)
        subject, from_email, to = 'Билеты ', 'robot@donexpocentre.ru', email
        msg = EmailMultiAlternatives(subject, '', from_email, [to])

        html_content = '''
        <div id='content'>
            <div style="text-align: center;">
                <h3 style="color:#5e2c81">Благодарим Вас за регистрацию!</h3>
                <h4>Для посещения мероприятия необходимо предъявить билеты в распечанном или электронном виде! Версия для печати: <a href="https://{0}/ticket/print/{1}">тут</a></h4>
            </div>

            <div style="text-align: center;">
                <h4>Билеты</h4>
            '''.format(base_dir, order.number)

        for t in tickets:

            url_ticket = t.category.project.ticket_logo.name
            ticket_category = TicketsCategory.objects.get(id=t.category_id)

            writer = ImageWriter()
            writer.quiet_zone = 0.1
            writer.dpi = 400
            writer.module_width = 0.5

            c128 = C128(t.code, writer=writer)
            c128.save(settings.BASE_DIR + '/media/barcode/barcode_' + t.code)

            fnt = ImageFont.truetype(settings.BASE_DIR + '/static/fonts/DejaVuSansMono.ttf', 22)

            # ticket_kiosk.delay(ticket_category, t)

            if url_ticket:
                result = Image.new("RGB", size=(1500, 1500), color=(255, 255, 255, 0))
                background = Image.open(settings.BASE_DIR + '/media/' + url_ticket)
                background.thumbnail((1500, 1500))
                w_background, h_background = background.size

                draw = ImageDraw.Draw(background)
                if ticket_category.project.is_weekend:
                    text = '%s %s, %s №%s' % (
                    ticket_category.name[:30], ('Выходной день' if ticket_category.type_in=='weekend' else 'Будний день'), email[:30], t.code)
                else:
                    text = '%s  %s №%s' % (
                    ticket_category.name[:30], email[:30], t.code)

                if t.plase and type(t.plase) != str:
                    text = text + ' ряд: %s место: %s' % (t.plase['row'], t.plase['plase'])
                draw.multiline_text((10, 5), text, font=fnt, fill=(0, 0, 0), align="left")
                result.paste(background, (0, 0))

                result = result.rotate(-90)
                result = result.crop((result.size[1] - h_background, 0, result.size[0], result.size[1],))

            else:
                result = Image.new("RGB", size=(350, 500), color=(255, 255, 255, 0))

            im1 = Image.open(settings.BASE_DIR + '/media/barcode/barcode_' + t.code + ".png")

            result.paste(im1, (result.size[0] // 2 - im1.size[0] // 2, result.size[1] - im1.size[1] + 45))
            result.save(settings.BASE_DIR + '/media/barcode/c128_' + t.code + ".png")

            html_content += '''
                    <img style="margin:10px;max-width: 400px; max-height: 450px; border: 1px solid #692b82;" src='https://{0}/media/barcode/c128_{1}.png'/>
                '''.format(base_dir, t.code)

        html_content += '''</div>
                <div>
                    <div style="padding: 20px;margin: 10px;
                                max-width: 650px;margin-left: auto;
                                margin-right: auto; border: 5px dotted #c31a84;">
                        <h4>Обратите внимание</h4>
                        <ul type="square">
                            <li><span>Штрих-код, указанный на билете, действителен только для однократного прохода на мероприятие.					
                                </span></li>
                            <li><span>Не допускайте перепечатки и копирования билета посторонними лицами, так как они могут воспользоваться им раньше Вас!	
                                </span></li>
                            <li><span>Не выкладывайте фото Электронного билета в социальные сети и не пересылайте третьим лицам, во избежание мошеннических действий.
                                </span></li>	
                        </ul>
                    </div>
                </div>
                <div style='padding: 10px; background: #eeeeee; height: 100px;
                        max-width: 650px;margin-left: auto;
                        margin-right: auto; border: 5px dotted #5e2c80;'>
                    <p style="color: #5e2c81; text-align: center;">
                        <span style=''>ПО ВОПРОСАМ ПОСЕЩЕНИЯ: +7(863)268–77-59 или <a class="link" href="mailto:marketing@donexpocentre.ru">marketing@donexpocentre.ru </a> </span>	
                    </p>
                    <p style='text-align: center;'>Организатор: © 2018.  <a class="link-underline" href="https://donexpocentre.ru" target="_blank">ДОНЭКСПОЦЕНТР</a>
                    </p>  
                </div> 
        </div>'''
        msg.attach_alternative(html_content, "text/html")

        if order.state_email == 0:
            order.state_email = 1
        else:
            order.state_email = 2
        order.save()
        errorStatus = {'code': 0, 'e': 'Успешно'}

    except Exception as e:
        print(e)
        order = Order.objects.get(id=id)
        if order:
            order.state_email = 0
            order.save()
        errorStatus = {'code': 1, 'e': e}
        subject, from_email, to = 'Билеты', 'robot@donexpocentre.ru', email
        html_content = '<p>Во время регистрации произошла ошибка, для получения билета свяжитесь с менеджером по телефону +7 (863) 268–77-59 или напишите на почту marketing@donexpocentre.ru </p>'
        msg = EmailMultiAlternatives(subject, '', from_email, [to])
        msg.attach_alternative(html_content, "text/html")

    msg.send()
    return errorStatus

@task
def send_ticket_kiosk(id):
    C128 = barcode.get_barcode_class('code128')
    errorStatus = {'code': 0}


    order = Order.objects.get(id=id)
    tickets = Tickets.objects.filter(order_id=order)

    for t in tickets:

        url_ticket = t.category.project.ticket_logo.name
        ticket_category = TicketsCategory.objects.get(id=t.category_id)

        writer = ImageWriter()
        writer.quiet_zone = 0.1
        writer.dpi = 400
        writer.module_width = 0.5

        c128 = C128(t.code, writer=writer)
        c128.save(settings.BASE_DIR + '/media/barcode/barcode_' + t.code)

        # ticket_kiosk.delay(ticket_category, t)


    return errorStatus


@task
def ferma(order, type):
    try:
        ferma_data = settings.FERMA
        # авторизация
        data = json.dumps({'Login': ferma_data['Login'], 'Password': ferma_data['Password']})
        headers = {'Content-type': 'application/json', 'Content-Length': str(len(data)), 'charset': 'utf-8'}
        answer = requests.post('https://ferma.ofd.ru/api/Authorization/CreateAuthToken', data=data, headers=headers)

        # создание чека
        resp = answer.json()

        if resp['Status'] == 'Success':

            Items = []
            order = Order.objects.get(uuid=order)
            tickets = Tickets.objects.filter(order = order)

            for ticket in tickets:

                Items.append({
                    "Label": "Билет "+ ticket.category.name[:25]+ ('Вых.' if ticket.category.is_weekend else ''),
                    "Price": ticket.total_cost,
                    "Quantity": 1,
                    "Amount": ticket.total_cost*1,
                    "Vat": ferma_data['Vat']
                })
                if order.project.is_plane:
                    plane = TicketsPlane.objects.get(project=order.project)
                    plane.edit=False

                    for i in plane.json_tickets:
                        if ticket.plase['row'] == str(i['row_real']) and ticket.plase['plase'] == str(i['plase_real']) :
                            i['pay'] = True
                            i['reservation'] = None
                            plane.save()


            data = json.dumps({
                'Request': {
                    "Inn": ferma_data['Inn'],
                    "Type": type,
                    "InvoiceId": str(order.uuid)+'_'+type,
                    "LocalDate": order.date.strftime('%Y-%m-%dT%H:%M:%S'),
                    # "LocalDate": "2018-11-20T14:13:24",
                    "CustomerReceipt": {
                        "TaxationSystem": ferma_data["TaxationSystem"],
                        "Email": order.buyer,
                        "Items": Items
                    }
                }
            })
            headers = {'Content-type': 'application/json', 'Content-Length': str(len(data)), 'charset': 'utf-8'}
            answer_receiptid = requests.post('https://ferma.ofd.ru/api/kkt/cloud/receipt?AuthToken='+resp['Data']['AuthToken'], data=data, headers=headers)

            # сохранение заказа
            resp_receiptid = answer_receiptid.json()
            if resp_receiptid['Status'] == 'Success':

                if order.receipt_id:
                    order.receipt_id[type] = {'date': order.date.strftime('%Y-%m-%dT%H:%M:%S'),data: resp_receiptid['Data']['ReceiptId']}
                else:
                    order.receipt_id = {}
                    order.receipt_id[type] = {'date':order.date.strftime('%Y-%m-%dT%H:%M:%S'), data:resp_receiptid['Data']['ReceiptId']}


                order.save()

            errorStatus = 'Чек отправлен'

        else:
            errorStatus = 'Чек. Ошибка при авторизации. Статус:' + resp['Status']

    except Exception as e:
        errorStatus = e

    print(errorStatus)

@task
def ticket_kiosk(ticket_category, t):
    # билет для киоска на чековой бумаге

    fnt = ImageFont.truetype(settings.BASE_DIR + '/static/fonts/DejaVuSansMono.ttf', 22)
    fnt28 = ImageFont.truetype(settings.BASE_DIR + '/static/fonts/DejaVuSansMono.ttf', 28)

    kiosk_tick = Image.new("RGB", size=(500, 900), color=(255, 255, 255, 0))
    draw_kiosk_tick = ImageDraw.Draw(kiosk_tick)

    text ='***********************************   \n'
    text+= (ticket_category.project.name.replace(' ', '\n'))
    text += '\n***********************************   \n'

    text += 'Билет: №%s \n ' % (t.code,)
    if ticket_category.project.is_weekend:
        text += '%s %s, \n ' % (
            ticket_category.name[:30], ('Выходной день' if ticket_category.type_in=='weekend' else 'Будний день'),)
    else:
        text += '%s \n ' % ( ticket_category.name[:30])

    if ticket_category.project.is_pay:
        text += 'Стоимость: %s руб. \n ' % (ticket_category.price,)


    if t.order.buyer:
        text += 'Покупатель: %s. \n ' % (t.order.buyer,)

    if t.plase and type(t.plase) != str:
        text +='ряд: %s место: %s \n' % (t.plase['row'], t.plase['plase'])


    text += 'Дата: %s' %(datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S"))

    draw_kiosk_tick.multiline_text((0, 10), text, font=fnt, fill=(0, 0, 0), spacing=10, align="center")

    n_count = 1
    for i in text:
        if i=='\n':
            n_count+=1

    #
    im1 = Image.open(settings.BASE_DIR + '/media/barcode/barcode_' + t.code + ".png")
    kiosk_tick.paste(im1, (0, 400))


    text = '***********************************   \n'
    text+= 'ДОНЭКСПОЦЕНТР\n'
    text+= 'donexpocentre.ru\n'
    draw_kiosk_tick.multiline_text((0, 800), text, font=fnt, fill=(0, 0, 0), spacing=10, align="center")
    kiosk_tick.save(settings.BASE_DIR +'/media/barcode/kioskticket_' + t.code + ".png")

    t.ticket_check = 'barcode/kioskticket_' + t.code + ".png"
    t.save()