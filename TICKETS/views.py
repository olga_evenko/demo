from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework import status, viewsets
from rest_framework.permissions import IsAuthenticated, AllowAny, BasePermission
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route, permission_classes

from TICKETS.models import *
from TICKETS.serializers import *
from plus_administration.models import *
from plus_administration.serializers import *

from PROJECT.models import *
from PROJECT.serializers import *
from django.core.paginator import Paginator

from time import mktime as mktime
from datetime import *
from .tasks import *
from django.utils import timezone
import math
from django.db.models import Q
from django.db.models.query import QuerySet
from django.conf import settings
import time
import uuid
import urllib.request

def index(request):

    context = {
        'data': 'value',
    }
    request.path = '/'
    return render(request, 'tickets.html', context)


def print_ticket(request, code):
    try:
        order = Order.objects.get(number=code)

        tickets = Tickets.objects.filter(order=order)

        url_ticket = []

        for t in tickets:
            url_ticket.append('../../../media/barcode/c128_' + t.code + '.png')

        context = {
            'url_ticket': url_ticket,
        }

        return render(request, 'ticket_print.html', context)


    except:

        return render(request, 'ticket_print.html', )


class TicketsCategoryList(viewsets.ModelViewSet):
    queryset = TicketsCategory.objects.all()
    serializer_class = TicketsCategorySerializer
    authentication_class = (JSONWebTokenAuthentication,)

    # permission_classes = (IsAuthenticated,)
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def list(self, request, id=None, pk=None):
        try:
            project = Project.objects.get(id=id)
            if request.auth:
                data = TicketsCategory.objects.filter(project=project).order_by('-price', 'name')
                if request.user.is_kiosk and project.is_weekend:
                    response = urllib.request.urlopen('https://isdayoff.ru/%s?cc=ru' %( datetime.datetime.today().strftime('%Y%m%d')))
                    date = response.read()
                    if int(date):
                        data = data.exclude(type_in='weekdays')
                    else:
                        data = data.exclude(type_in='weekend')

            else:

                if project.is_pay:
                    data = TicketsCategory.objects.filter(project=project,
                                                          project__date_end__gte=datetime.datetime.now(),
                                                          price__gt=0).order_by('-price', 'name')
                else:
                    data = TicketsCategory.objects.filter(project=project,
                                                          project__date_end__gte=datetime.datetime.now(),
                                                          price=0).order_by('-price', 'name')
            content = TicketsCategorySerializerList(data, many=True).data

            for category in content:
                if (category['count_all']):
                    ticket = Tickets.objects.filter(category=category['id']).count()
                    category['count_all'] = category['count_all'] - ticket
            return Response(content)
        except:
            return Response({})

    @list_route(methods=['get'])
    def options(self, request, id=None, pk=None):
        data = {}
        type_discount = TicketsCategory.TYPE_DISCOUNT
        data['type_discount'] = []
        for t in type_discount:
            data['type_discount'].append({
                'key': t[0],
                'value': t[1],
            })
        type_discount = TicketsCategory.TYPE_VALUE_DISCOUNT
        data['type_value_discount'] = []
        for t in type_discount:
            data['type_value_discount'].append({
                'key': t[0],
                'value': t[1],
            })

        type_in = TicketsCategory.TYPE_IN
        data['type_in'] = []
        for t in type_in:
            data['type_in'].append({
                'key': t[0],
                'value': t[1],
            })
        return Response(data)

    def retrieve(self, request, id=None, pk=None):
        if pk:
            try:
                news = TicketsCategory.objects.get(id=pk)
                content = TicketsCategorySerializer(news).data
                return Response(content)
            except:
                pass
        content = TicketsCategorySerializer(None).data
        return Response(content)

    @list_route(methods=['get'])
    def receptions(self, request, id=None):

        receptions = Receptions.objects.filter(project__id=id)
        content = ReceptionsSerializer(receptions, many=True).data
        return Response(content)

    @detail_route(methods=['get'])
    def reception(self, request, id=None, pk=None):
        if pk:
            try:
                reception = Receptions.objects.get(id=pk)
                content = ReceptionsSerializerForm(reception).data
                return Response(content)
            except:
                pass
        content = ReceptionsSerializerForm(None).data
        return Response(content)

    @list_route(methods=['get'])
    def select_receptions(self, request, id=None):

        user = ExtUser.objects.filter(is_receptions=True)
        users = ExtUserSerializer(user, many=True).data

        ticket_category = TicketsCategory.objects.filter(project_id=id)
        tickets_category = TicketsCategorySerializer(ticket_category, many=True).data

        return Response({'user': users, 'ticket_category': tickets_category})

    @detail_route(methods=['post'])
    def add_reception(self, request, id=None, pk=None):
        temp = request.POST.copy()
        if pk == 'new':
            obj = Receptions()
        else:
            try:
                obj = Receptions.objects.get(id=pk)
            except:
                obj = None

        if obj:
            try:
                rec = Receptions.objects.filter(project__id=id, user__id=temp['user'])

                if pk != 'new':
                    reception = rec.exclude(id=temp['id'])
                else:
                    reception = rec

                receptions = reception.count()

                if receptions:
                    return Response({'error': 'Повторное назначение аккаунта'}, status=status.HTTP_400_BAD_REQUEST)
            except:
                pass

            temp.update({'project': id})
            if temp['user'] == 'null':
                temp['user'] = ''
            serializer = ReceptionsSerializerSave(obj, data=temp)
            if serializer.is_valid():
                obj = serializer.save()

                reception = Receptions.objects.get(id=obj.id)
                ticket_category = temp['ticket_category'].split(',')
                if list(temp['ticket_category']).__len__():
                    for i in ticket_category:
                        category = TicketsCategory.objects.get(id=i)
                        reception.ticket_category.add(category)

                    # obj.add()
                reception.save()
                return Response({})

        return Response({'error': 'ошибка сохранения'}, status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['post'])
    def delete_reception(self, request, id=None, pk=None):
        try:
            obj = Receptions.objects.get(id=pk)
            obj.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Receptions.DoesNotExist:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['get'])
    def data(self, request, id=None, pk=None):
        if pk:
            try:
                news = TicketsCategory.objects.get(id=pk)
                content = TicketsCategorySerializer(news).data
                return Response(content)
            except:
                pass
        content = TicketsCategorySerializer(None).data
        return Response(content)

    @detail_route(methods=['post'])
    def add(self, request, id=None, pk=None):

        temp = request.POST.copy()
        if pk == 'new':
            obj = TicketsCategory()
        else:
            obj = TicketsCategory.objects.get(id=pk)

        if obj:
            project = Project.objects.get(id=id)
            # obj.project = project
            temp.update({'project': id})
            if 'count_all' in temp:
                if temp['count_all'] == 'null':
                    temp.update({'count_all': None})
            else:
                temp.update({'count_all': None})

            if 'ticket_img' in temp:
                if temp['ticket_img'] == 'delete':
                    obj.logo = None
                temp.pop('ticket_img')

            serializer = TicketsCategorySerializer(obj, data=temp)
            # serializer._args[0].logo = obj.logo
            if serializer.is_valid():
                serializer.validated_data

                if request.FILES != {}:
                    temp.update(request.FILES)
                    # temp['ticket_img'] = temp['ticket_img'][0]
                    serializer = TicketsCategorySerializer(obj, data=temp)

                    if serializer.is_valid():
                        # serializer.validated_data['logo']
                        serializer.validated_data
                obj = serializer.save()

                serializer = TicketsCategorySerializerList(obj)
                data = {}
                data['formData'] = serializer.data
                data['active_button'] = 'view'

                return Response(data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['post'])
    def delete(self, request, id=None, pk=None):
        try:
            obj = TicketsCategory.objects.get(id=pk)
            obj.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except TicketsCategory.DoesNotExist:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['get'])
    def get_plane(self, request, id=None):
        obj = TicketsPlane.objects.get(project_id=id)
        if not request.session.session_key:
            request.session.save()
        # session_id = request.session.session_key
        content = {
            'tickets': TicketsPlaneSerializer(obj).data,
            'session_key': request.session.session_key
        }

        return Response(content)

    @list_route(methods=['post'])
    def save_plane(self, request, id=None):
        json_data = request.data['json_tickets']
        data = json.loads(json_data)

        try:
            obj = TicketsPlane.objects.get(project_id=id)
        except TicketsPlane.DoesNotExist:
            obj = TicketsPlane()

        for d in data:
            if 'reservation' in d:
                if d['reservation']:
                    delta = datetime.datetime.now() - datetime.datetime.fromtimestamp(
                        d['reservation']['date'] / 1e3)

                    if delta.total_seconds() < 600:
                        if 'key' in d['reservation']:
                            d['reservation'] = {
                                'date': d['reservation']['date'],
                                'key': d['reservation']['key']
                            }
                        else:
                            d['reservation'] = {
                                'date': d['reservation']['date'],
                                'key': request.session.session_key
                            }
                    else:
                        d['reservation'] = None

        temp = {
            'project': id,
            'json_tickets': data,
            'row': request.data['row'],
            'plase': request.data['plase'],
        }

        serializer = TicketsPlaneSerializer(obj, data=temp)
        if serializer.is_valid():
            serializer.validated_data
            serializer.save()

            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def save_plane_websocket(self, data, id=None, session_key=None):
        json_data = data['json_tickets']

        try:
            obj = TicketsPlane.objects.get(project_id=data['project'])
        except TicketsPlane.DoesNotExist:
            obj = TicketsPlane()

        for d in data['json_tickets']:
            if 'reservation' in d:
                if d['reservation']:
                    delta = datetime.datetime.now() - datetime.datetime.fromtimestamp(
                        d['reservation']['date'] / 1e3)

                    if delta.total_seconds() < 600:
                        if 'key' in d['reservation']:
                            d['reservation'] = {
                                'date': d['reservation']['date'],
                                'key': d['reservation']['key']
                            }
                            # d['reservation'] = None
                        else:
                            d['reservation'] = {
                                'date': d['reservation']['date'],
                                'key': session_key
                            }
                    else:
                        d['reservation'] = None

        temp = {
            'project': data['project'],
            'json_tickets': data['json_tickets'],
            'row': data['row'],
            'plase': data['plase'],
            # 'row_real': request.data['row'],
            # 'plase_real': request.data['plase']

        }

        serializer = TicketsPlaneSerializer(obj, data=temp)

        if serializer.is_valid():
            serializer.validated_data
            serializer.save()

            return serializer.data

        return serializer.errors

    @list_route(methods=['post', 'get'])
    def find_ticket(self, request, id=None):
        a = 0
        if 'id' in request.data:

            plane = TicketsPlane.objects.get(project_id=id)
            list_plane = plane.json_tickets

            for i in request.data['id'].split(','):

                plase = list_plane[int(i)]

                if 'reservation' in plase:
                    if plase['reservation']:
                        delta = datetime.datetime.now() - datetime.datetime.fromtimestamp(
                            plase['reservation']['date'] / 1e3)

                        if delta.total_seconds() < 600:
                            if plase['reservation']['key'] == request.session.session_key:
                                return Response(plase, status=status.HTTP_400_BAD_REQUEST)
                            else:
                                return Response(plase, status=status.HTTP_400_BAD_REQUEST)

            return Response({})


class TicketsActivate(viewsets.ModelViewSet):
    queryset = Tickets.objects.all()
    serializer_class = TicketsSerializer
    authentication_class = (JSONWebTokenAuthentication,)

    def list(self, request, id=None, pk=None):
        # content = []
        # receptions = Receptions.objects.filter(user_id=id)
        #
        # for res in receptions:
        #     content.append({'reception': ReceptionsSerializer(res).data,
        #                     'project': ProjectSerializer(res.project).data, })

        last_month = datetime.date.today().month - 1 or 12
        next_month = (datetime.date.today().month + 1) % 12 or 12
        date_start = datetime.datetime(datetime.date.today().year, last_month, datetime.date.today().day)
        date_end = datetime.datetime(datetime.date.today().year, next_month, datetime.date.today().day)

        projects = Project.objects.filter(date_start__gte=date_start, date_start__lte=date_end)





        return Response(ProjectSerializer(projects, many=True).data)

    # except:
    #     return Response({ProjectSerializer(res.project).data})

    @detail_route(methods=['post'])
    def check(self, request, id=None, pk=None):

        # TODO данный код работает, пока нет разделения по залам. Переделать как появится другая логика

        if 'ticket_category' in request.POST:
            ticket_category = request.POST['ticket_category'].split(',')

            message = {
                'number': pk,
                'date': datetime.datetime.now().strftime("%d-%m-%Y\n %H:%M:%S"),
            }

            try:
                ticket = Tickets.objects.get(code=pk)
                if ticket.order.state == 0:
                    message['message'] = 'Билет не оплачен'
                    message['status'] = 'error'
                    return Response({message['status']: message['message'], 'message': message},
                                    status=status.HTTP_400_BAD_REQUEST)

                if not ticket.status:
                    message['message'] = 'Билет был отменен'
                    message['status'] = 'error'
                    return Response({message['status']: message['message'], 'message': message},
                                    status=status.HTTP_400_BAD_REQUEST)

                # if str(ticket.category_id) in ticket_category:

                if not ticket.state:

                    response = urllib.request.urlopen(
                        'https://isdayoff.ru/%s?cc=ru' % (datetime.datetime.today().strftime('%Y%m%d')))
                    date = response.read()
                    if int(date):
                        week_day = 'weekend'
                    else:
                        week_day = 'weekdays'

                    # week_day = 'weekdays' if timezone.now().weekday() < 5 else 'weekend'
                    # # week_day = 'weekend' if timezone.now().weekday()<5 else 'weekdays'

                    if week_day == ticket.category.type_in or ticket.category.project.is_weekend == False or ticket.category.type_in == 'all':

                        if ticket.category.count > 1:
                            tickets_list = Tickets.objects.filter(order_id=ticket.order_id, state=1).order_by(
                                'date_activate')

                            if tickets_list:
                                # если групповые билеты, то активация в один день
                                if (timezone.now() - tickets_list[0].date_activate).days > 0:
                                    message['message'] = 'Ошибка активации'
                                    message['status'] = 'error'
                                    message['commens'] = 'Активация %s' % (
                                        tickets_list[0].date_activate.strftime('%d-%m-%Y'))
                                    return Response({message['status']: message['message'], 'message': message},
                                                    status=status.HTTP_400_BAD_REQUEST)

                        ticket.state = 1;
                        ticket.date_activate = datetime.datetime.now()
                        ticket.save()
                        message['message'] = 'Активировано!'
                        message['status'] = 'success'
                        return Response({message['status']: message['message'], 'message': message})
                    else:
                        if week_day == 'weekdays':
                            message['message'] = 'Посещение только в будний'
                            message['status'] = 'error'
                            message['activate'] = True

                        else:
                            message['message'] = 'Посещение только в выходной'
                            message['status'] = 'error'
                            message['activate'] = True

                        return Response({message['status']: message['message'], 'message': message},
                                        status=status.HTTP_400_BAD_REQUEST)

                else:
                    if (timezone.now() - ticket.date_activate).days < 1:
                        message['message'] = 'Повторная активация!'
                        message['status'] = 'warn'

                    else:
                        message['message'] = 'Билет был активирован ранее'
                        message['status'] = 'error'
                    return Response({message['status']: message['message'], 'message': message},
                                    status=status.HTTP_400_BAD_REQUEST)

                # message['message'] = 'Вход недоступен для данной категории билета'
                # message['status'] = 'error'
                # return Response({message['status']: message['message'], 'message': message},
                #                 status=status.HTTP_400_BAD_REQUEST)

            except Tickets.DoesNotExist:
                message['message'] = 'Билет не найден'
                message['status'] = 'error'
                return Response({message['status']: message['message'], 'message': message},
                                status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['get'])
    def find(self, request, id=None):

        ticket = Tickets.objects.filter(category__project__id=id)

        if request.query_params['data']:
            query = json.loads(request.query_params['data'])

            key = []
            for i in query:
                if not query[i]:
                    key.append(i)
            for i in key:
                query.pop(i)

            ticket = ticket.filter(**query)
        content = TicketsActivateSerializer(ticket, many=True).data

        # content['receptions'] = ReceptionsSerializer(receptions, many=True).data

        return Response(content)

        a = 0


class TicketsBuy(viewsets.ModelViewSet):
    queryset = Tickets.objects.all()
    serializer_class = TicketsSerializer
    authentication_class = (JSONWebTokenAuthentication,)

    @list_route(methods=['get'])
    def redirect_after_pay(self, request, id=None):
        try:
            if 'orderId' in request.GET:
                order = Order.objects.get(uuid=request.GET['orderId'])
                if order.state == 0:
                    sbr_data_account = order.project.sbr

                    url_status = '%s/payment/rest/getOrderStatusExtended.do?' \
                                 'userName=%s&password=%s&language=ru&' \
                                 'orderId=%s' % (
                                     sbr_data_account.url, sbr_data_account.api, sbr_data_account.password, order.uuid)
                    request_sbr = requests.post(url_status)
                    content_status = json.loads(request_sbr._content.decode())
                    order.pan = content_status['cardAuthInfo']['pan']
                    order.cardholderName = content_status['cardAuthInfo']['cardholderName']
                    order.state = 1
                    order.amount = content_status['amount'] / 100

                    if order.loyalty_promocode:
                        if order.loyalty_promocode.contact and order.loyalty_promocode.contact.__len__() > 0:
                            for cont in order.loyalty_promocode.contact:
                                if cont['data'] == order.buyer and not 'activate' in cont:
                                    cont['activate'] = True
                                    order.loyalty_promocode.save()
                                    a = 0
                    order.save()

                    if order.project.is_plane:
                        plane = TicketsPlane.objects.get(project_id=order.project.id)
                        if plane.edit:
                            plane.edit = False
                        for t in Tickets.objects.filter(order=order):
                            plane.json_tickets[int(t.plase['id'])]['pay'] = True
                            plane.save()

                    send_ticket.delay(request.GET['orderId'], order.buyer, request.META['HTTP_HOST'])
                    ferma.delay(order.uuid, 'Income')
                    # ferma(order.uuid,  'Income')
                if ('ticket_basket' in request.session):
                    temp = request.session['ticket_basket']
                    temp.pop('proj_' + request.GET['id'].replace('/', ''), None)
                    request.session['ticket_basket'] = temp

            return render(request, 'ticket.html', {'email': order.buyer, 'metrika': order.project.metrika})
        except Exception as e:

            return render(request, 'ticket_error.html')

    # повторная отправка e-mail
    @list_route(methods=['post'])
    def resend_email(self, request, id=None):

        data = request.data
        try:
            order = Order.objects.get(uuid=data['uuid'], state=1)
            sbr_data_account = order.project.sbr
            url_status = '%s/payment/rest/getOrderStatusExtended.do?' \
                         'userName=%s&password=%s&language=ru&' \
                         'orderId=%s' % (
                             sbr_data_account.url, sbr_data_account.api, sbr_data_account.password,
                             order.uuid)
            request_sbr = requests.post(url_status)
            content_status = json.loads(request_sbr._content.decode())
            if content_status['errorCode'] == '0':
                # send_ticket(data['uuid'], order.buyer, request.META['HTTP_HOST'])
                send_ticket.delay(data['uuid'], order.buyer, request.META['HTTP_HOST'])
                return Response({'message': 'Сообщение отправлено'})
            else:
                return Response({'message': 'Повторное сообщение не может быть отправлено'},
                                status=status.HTTP_400_BAD_REQUEST)
        except Order.DoesNotExist:
            return Response({'message': 'Заказ не найден'}, status=status.HTTP_400_BAD_REQUEST)

        except:
            return Response({'message': 'Ошибка'}, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['get', 'post'])
    def create_basket(self, request):
        # request.session['ticket_basket'] = {}
        if request.data:
            if ('tickets_basket' in request.data):
                request.session['ticket_basket'] = {}
            else:
                request.session['ticket_basket'] = request.data
                for i in request.session['ticket_basket']:
                    request.session['ticket_basket'][i]['uuid'] = request.session.session_key
            return Response(request.session['ticket_basket'])
        else:
            # return Response({})
            if ('ticket_basket' in request.session):
                return Response(request.session['ticket_basket'])
            else:
                return Response({})

    @list_route(methods=['post'])
    def ticket_update(self, request, id=None):

        data = request.data

        try:
            order = Order.objects.get(number=data['id'])

            # order = Order.objects.get(id=ticket.order_id)
            order.surcharge = order.surcharge + (data['total_cost'] - order.amount)

            tickets = Tickets.objects.filter(order=order)

            category = TicketsCategory.objects.get(id=data['category_id'])

            for t in tickets:
                ticket = Tickets.objects.get(code=t.code)

                ticket.is_weekend = data['is_weekend']
                ticket.category = category
                ticket.total_cost = category.price
                ticket.edit_cost = True
                ticket.save()

            order.save()

            return Response({})

        except Order.DoesNotExist:
            return Response({'message': 'Билет не найден'}, status=status.HTTP_400_BAD_REQUEST)

        # except:
        #     return Response({'message': 'Ошибка'}, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'])
    def validate_email(self, request, id=None):
        if ('email' in request.data) and ('code_email' in request.session):
            if request.session['code_email']['email'] == request.data['email']:
                if 'confirm' in request.session['code_email'] and request.session['code_email']['confirm'] == True:
                    return Response(request.session['code_email'])

        return Response({}, status=status.HTTP_400_BAD_REQUEST)

    # проверка e-mail
    @list_route(methods=['post'])
    def email(self, request, id=None):
        if 'email' in request.data and not 'code' in request.data:
            if 'code_email' in request.session:
                if 'date' in request.session['code_email'] and datetime.datetime.now().timestamp() - \
                        request.session['code_email']['date'] < 120:
                    return Response({}, status=status.HTTP_400_BAD_REQUEST)
            random.seed()
            code = str(random.randint(10000, 99999))
            send_code_auth.delay(request.data['email'], code)
            request.session['code_email'] = {'email': request.data['email'], 'code': code,
                                             'date': datetime.datetime.now().timestamp()}
            # return Response(request.session['code_email'])

        if 'code' in request.data:
            if request.data['code'] == request.session['code_email']['code']:
                if 'email' in request.data:
                    if request.data['email'] != request.session['code_email']['email']:
                        return Response({'status': 'false', 'message': 'Unauthorized'}, status=401)

                temp = request.session['code_email']
                request.session['code_email'] = {'email': request.session['code_email']['email'],
                                                 'code': request.session['code_email']['code'],
                                                 'confirm': True}
            else:
                return Response({'status': 'false', 'message': 'Unauthorized'}, status=401)

        return Response(request.session['code_email']['email'])

    # закрытие кассового дня в киоске
    @list_route(methods=['post', 'get'])
    def close_change(self, request, id=None):
        change = CashChange()
        change.save()
        return Response({})

    # состояние продаж в киоске
    @list_route(methods=['post', 'get'])
    def select_change_data(self, request, id=None):
        change = CashChange.objects.latest('date_close')
        order = Order.objects.filter(date__gt=change.date_close)

        content = {
            'count': 0,
            'summ': 0,
        }
        for i in order:
            if i.payment_method == 'kiosk':
                content['summ'] += i.amount
                content['count'] += 1

        return Response(content)

    @staticmethod
    def ferma(order, type):
        try:
            ferma_data = settings.FERMA
            # авторизация
            data = json.dumps({'Login': ferma_data['Login'], 'Password': ferma_data['Password']})
            headers = {'Content-type': 'application/json', 'Content-Length': str(len(data)), 'charset': 'utf-8'}
            answer = requests.post('https://ferma.ofd.ru/api/Authorization/CreateAuthToken', data=data, headers=headers)

            # создание чека
            resp = answer.json()

            if resp['Status'] == 'Success':

                Items = []
                order = Order.objects.get(uuid=order)
                tickets = Tickets.objects.filter(order=order)

                for ticket in tickets:
                    Items.append({
                        "Label": "Билет " + ticket.category.name[:25] + ('Вых.' if ticket.category.is_weekend else ''),
                        "Price": ticket.total_cost,
                        "Quantity": 1,
                        "Amount": ticket.total_cost * 1,
                        "Vat": ferma_data['Vat']
                    })
                    if order.project.is_plane:
                        plane = TicketsPlane.objects.get(project=order.project)
                        plane.edit = False

                        for i in plane.json_tickets:
                            if ticket.plase['row'] == str(i['row_real']) and ticket.plase['plase'] == str(i['plase_real']):
                                i['pay'] = True
                                i['reservation'] = None
                                plane.save()

                data = json.dumps({
                    'Request': {
                        "Inn": ferma_data['Inn'],
                        "Type": type,
                        "InvoiceId": str(order.uuid) + '_' + type,
                        "LocalDate": order.date.strftime('%Y-%m-%dT%H:%M:%S'),
                        # "LocalDate": "2018-11-20T14:13:24",
                        "CustomerReceipt": {
                            "TaxationSystem": ferma_data["TaxationSystem"],
                            # "Email": 'o.v.evenko@mail.ru',
                            "Items": Items
                        }
                    }
                })
                headers = {'Content-type': 'application/json', 'Content-Length': str(len(data)), 'charset': 'utf-8'}
                answer_receiptid = requests.post(
                    'https://ferma.ofd.ru/api/kkt/cloud/receipt?AuthToken=' + resp['Data']['AuthToken'], data=data,
                    headers=headers)

                # сохранение заказа
                resp_receiptid = answer_receiptid.json()
                if resp_receiptid['Status'] == 'Success':

                    if order.receipt_id:
                        order.receipt_id[type] = {'date': order.date.strftime('%Y-%m-%dT%H:%M:%S'),
                                                  'data': resp_receiptid['Data']['ReceiptId']}
                    else:
                        order.receipt_id = {}
                        order.receipt_id[type] = {'date': order.date.strftime('%Y-%m-%dT%H:%M:%S'),
                                                  'data': resp_receiptid['Data']['ReceiptId']}

                    order.save()

                errorStatus = 'Чек отправлен'

            else:
                errorStatus = 'Чек. Ошибка при авторизации. Статус:' + resp['Status']

        except Exception as e:
            errorStatus = e


        print(errorStatus)


    @list_route(methods=['post', 'get'])
    def create_order(self, request, id=None):
        data = request.data

        if request.data['type_pay'] == 'free_online':
            data = request.data

            if 'email' in data:
                email = request.data['email']

                project = Project.objects.get(id=id)
                orderNumber = int(mktime(datetime.datetime.now().timetuple()))

                order = Order(number=orderNumber,
                              project=project,
                              payment_method='free_online',
                              buyer=email)

                # order.save()
                order.amount = 0
                order.save()

                tickets_arr = Tickets.objects.filter(category__project_id=id).order_by('-number')
                number, code = TicketsBuy.GenerateCodeTicket(id, tickets_arr)

                category = TicketsCategory.objects.filter(project__id=id, price=0)[0]

                ticket_obj = Tickets(number=number, category=category, code=code, order=order, total_cost=0,
                                     first_cost=0, plase='{}')

                ticket_obj.save()

                order.state = 1
                order.save()

                # send_ticket_free(order.id, order.buyer, request.META['HTTP_HOST']  )
                send_ticket_free.delay(order.id, order.buyer, request.META['HTTP_HOST'])

            return Response({'order': order.id})

        if request.data['type_pay'] == 'kiosk':
            data = request.data

            project = Project.objects.get(id=id)
            orderNumber = int(mktime(datetime.datetime.now().timetuple()))

            # генерация uuid для купленного в киоске билета
            uuid_code = str(uuid.uuid4())
            while Order.objects.filter(uuid=uuid_code).count() > 0:
                uuid_code = str(uuid.uuid4())

            order = Order(
                uuid=uuid_code,
                number=orderNumber,
                project=project,
                payment_method='kiosk', )

            order.amount = data['tickets']['data']['amount']
            order.save()

            for ticket in data['tickets']['data']['tickets']:
                temp = data['tickets']['data']['tickets'][ticket]

                while temp > 0:
                    temp -= 1

                    tickets_arr = Tickets.objects.filter(category__project_id=id).order_by('-number')
                    number, code = TicketsBuy.GenerateCodeTicket(id, tickets_arr)

                    category = TicketsCategory.objects.get(id=ticket)

                    ticket_obj = Tickets(number=number, category=category, code=code, order=order,
                                         total_cost=category.price, first_cost=category.price, plase='{}')

                    ticket_obj.save()

            order.state = 1
            order.save()

            TicketsBuy.ferma(order.uuid, 'Income')
            url_check = TicketsPrint.create_ticket_check(order.id, request.user.id)

            return Response({'order': order.number,
                             'check':url_check})

        if request.data['type_pay'] == 'online':
            if 'email' in request.data:
                email = request.data['email']
                tickets = request.data['tickets']

                url_abs = request.build_absolute_uri().replace('create_order','redirect_after_pay?id=' + str(id)).replace('http','https')
                # url_abs = request.build_absolute_uri().replace('create_order', 'redirect_after_pay?id='+str(id)).replace('http', 'http')
                content = TicketsBuy.sbr_online(self, id, email, tickets, url_abs)

                return Response(content)

        if request.data['type_pay'] == 'expobit':

            if 'tickets' in data:
                order_arr = []

                for ticket in data['tickets']:
                    if ticket['contact']:
                        email = ticket['contact']
                        project = Project.objects.get(id=id)

                        ticket_create = True
                        plase = {}
                        if project.is_plane:

                            plane = TicketsPlane.objects.get(project_id=project.id)

                            for i in plane.json_tickets:
                                if int(ticket['row']) == i['row_real'] and int(ticket['place']) == i['plase_real']:
                                    plase = {
                                        'id': i['id'],
                                        'row': i['row_real'],
                                        'plase': i['plase_real'],
                                    }

                                    if plane.edit:
                                        plane.edit = False

                                    if plane.json_tickets[i['id']]['pay'] == True:
                                        ticket_create = False
                                    if 'reservation' in plane.json_tickets[i['id']] :
                                        if plane.json_tickets[i['id']]['reservation']:
                                            ticket_create = False

                                    if ticket_create:
                                        plane.json_tickets[i['id']]['pay'] = True
                                        plane.save()

                        if ticket_create:

                            orderNumber = int(mktime(datetime.datetime.now().timetuple()))

                            order = Order(number=orderNumber,
                                          project=project,
                                          payment_method='expobit',
                                          buyer=email)

                            order.amount = ticket['count'] * ticket['amount']
                            order.save()

                            for i in range(0, ticket['count']):
                                tickets_arr = Tickets.objects.filter(category__project_id=id).order_by('-number')
                                number, code = TicketsBuy.GenerateCodeTicket(id, tickets_arr)
                                category = TicketsCategory.objects.get(project__id=id, id=ticket['category'])

                            ticket_obj = Tickets(number=number, category=category, code=code, order=order,
                                                 total_cost=0, first_cost=ticket['amount'], plase=plase)
                            ticket_obj.save()

                            order.state = 1
                            order.save()

                            order_arr.append(order.id)

                            send_ticket_expobit(order.id, order.buyer, request.META['HTTP_HOST'])
                        # send_ticket_expobit.delay(order.id, order.buyer, request.META['HTTP_HOST'])

            return Response({'order': order_arr})

    @list_route(methods=['post'])
    def order_update(self, request, id=None):

        order_data = request.data['order']

        try:
            order = Order.objects.get(uuid=order_data['uuid'])
            order.buyer = order_data['buyer']
            order.save()
            return Response({})

        except Order.DoesNotExist:
            return Response({'message': 'Заказ не найден'}, status=status.HTTP_400_BAD_REQUEST)

        except:
            return Response({'message': 'Ошибка'}, status=status.HTTP_400_BAD_REQUEST)

    def validate_loyalty(self, id, params):
        try:
            promocode = Promocode.objects.get(project__id=id, code=params['promocode'], )

            if (promocode.contact != None and promocode.contact.__len__() > 0):
                # индивидуальный промокод
                for contact in promocode.contact:
                    if contact['data'] == params['data']:
                        if 'activate' in contact:
                            return {'error': 'Промокод активирован'}
                        else:
                            content = PromocodeSerializer(promocode).data
                            return content
                return {'error': 'Неверный промокод'}

            date = datetime.datetime.now()
            if promocode.count and Order.objects.filter(loyalty_promocode=promocode).count() >= promocode.count:
                return {'error': 'Превышен лимит использования'}

            if date<datetime.datetime.combine(promocode.date_start, datetime.datetime.min.time()) or \
                    date > datetime.datetime.combine(promocode.date_end, datetime.datetime.min.time()):
                return {'error': 'Срок действия промокода истек'}

            content = PromocodeSerializer(promocode).data

            return content
        except:
            return {'error': 'Неверный промокод'}

    @list_route(methods=['post', 'get'])
    def activate_loyalty(self, request, id=None, tickets=None):
        if request.data:
            params = request.data['params']

            content = TicketsBuy.validate_loyalty(self, id, params)
            if 'error' in content:
                return Response(content, status=status.HTTP_400_BAD_REQUEST)
            else:

                return Response(content)

    @list_route(methods=['post', 'get'])
    def create_forms(self, request, id=None):

        data = request.data

        if 'order' in data:
            tickets = Tickets.objects.filter(order__id=data['order'])
            i = 0
            # for form in data['forms']:
            f = Forms_Data(ticket=tickets[i], data=json.dumps(data['form']))
            f.save()
            # temp = {
            #     'ticket': tickets[i],
            #     'data':  json.dumps(data['forms'][form]['result'])
            # }
            # serializer = Forms_DataSerializers(f, temp)
            # if serializer.is_valid():
            #     serializer.save()
            # i+=1

        if 'order_id' in data:
            tickets = Tickets.objects.filter(order__uuid=data['order_id'])
            i = 0

            f = Forms_Data(ticket=tickets[i], data=json.dumps(data['form']))
            f.save()

        return Response({})

        if 'confirm' in request.data:
            if request.data['confirm']:
                # с подтверждением
                a = 0
            else:
                if 'email' in request.data:
                    email = request.data['email']
                    # modalData = request.data['modalData']
                    # countSelectTicket = request.data['countSelectTicket']
                    tickets = request.data['tickets']

                    url_abs = request.build_absolute_uri().replace('create_order', 'redirect_after_pay')

                    content = TicketsBuy.sbr(self, id, email, tickets, url_abs)

                    return Response(content)

    def sbr(self, id, email, tickets, url_abs):

        json_email = email
        project = Project.objects.get(id=id)

        orderNumber = int(mktime(datetime.datetime.now().timetuple()))

        order = Order(number=orderNumber,
                      project=project,
                      payment_method='online',
                      buyer=email['data'])

        order.save()
        order.amount = 0

        if 'promocode' in json_email:
            promocode = TicketsBuy.validate_loyalty(self, id, json_email)
        else:
            promocode = None

        tickets_content = []

        for ticket in tickets:
            for i in range(int(ticket['count'])):
                tickets_arr = Tickets.objects.filter(category__project_id=id).order_by('-number')
                number, code = TicketsBuy.GenerateCodeTicket(id, tickets_arr)
                if isinstance(ticket['data']['id'], str):
                    temp_category = ticket['data']['id'].split('_')
                    ticket['data']['id'] = temp_category[0]

                category = TicketsCategory.objects.get(id=ticket['data']['id'])
                total_cost = category.price

                if promocode:
                    if not 'error' in promocode:
                        if promocode['type'] == 'percent':
                            total_cost = category.price - category.price * int(promocode['value']) / 100
                        if promocode['type'] == 'number':
                            total_cost = category.price - int(promocode['value'])

                if type(category.discount) == list:
                    for discount in category.discount:
                        if discount['type'] == 'online':
                            total_cost_temp = category.price
                            if discount['type_value'] == 'percent':
                                total_cost_temp = category.price - category.price * int(discount['value']) / 100
                            if discount['type_value'] == 'number':
                                total_cost_temp = category.price - int(discount['value'])
                            if total_cost_temp < total_cost:
                                total_cost = total_cost_temp

                ticket_obj = Tickets(number=number, category=category, code=code, order=order, total_cost=total_cost,
                                     first_cost=total_cost, plase='{}')

                if project.is_plane:
                    ticket_obj.plase = {
                        'row': temp_category[1],
                        'plase': temp_category[2],
                    }

                ticket_obj.save()
                tickets_content.append(ticket_obj.code)

                order.amount += total_cost
        if promocode:
            if 'id' in promocode:
                order.loyalty = 'promocode'
                promocode_query = Promocode.objects.get(id=promocode['id'])
                order.loyalty_promocode = promocode_query

        order.save()

        # оплата
        sbr_data_account = project.sbr

        url = '%s/payment/rest/register.do?' \
              'userName=%s&password=%s' \
              '&amount=%d&description=%s&currency=643&language=ru&orderNumber=%d&' \
              'returnUrl=%s&pageView=DESKTOP&email=%s' % (
                  sbr_data_account.url, sbr_data_account.api, sbr_data_account.password,
                  order.amount * 100, project.name, orderNumber, url_abs,
                  json_email['data'])
        request_sbr = requests.post(url)
        content = json.loads(request_sbr._content.decode())

        url_status = '%s/payment/rest/getOrderStatusExtended.do?' \
                     'userName=%s&password=%s&language=ru&' \
                     'orderId=%s' % (
                         sbr_data_account.url, sbr_data_account.api, sbr_data_account.password, content['orderId'])
        request_sbr_status = requests.post(url_status)
        content_status = json.loads(request_sbr_status._content.decode())

        order.uuid = content['orderId']
        order.state = content_status['orderStatus']

        order.save()

        content['tickets'] = tickets_content

        return content

    def sbr_online(self, id, email, tickets, url_abs):

        project = Project.objects.get(id=id)
        orderNumber = int(mktime(datetime.datetime.now().timetuple()))

        order = Order(number=orderNumber,
                      project=project,
                      payment_method='online',
                      buyer=email['data'])

        order.save()
        order.amount = 0

        if 'promocode' in email:
            promocode = TicketsBuy.validate_loyalty(self, id, email)
        else:
            promocode = None

        tickets_content = []

        for ticket in tickets['tickets']:
            ticket = tickets['tickets'][ticket]

            for i in range(int(ticket['count'])):
                tickets_arr = Tickets.objects.filter(category__project_id=id).order_by('-number')
                number, code = TicketsBuy.GenerateCodeTicket(id, tickets_arr)
                if isinstance(ticket['data']['id'], str):
                    temp_category = ticket['data']['id'].split('_')
                    ticket['data']['id'] = temp_category[0]

                plase = {}
                if project.is_plane:
                    plane = TicketsPlane.objects.get(project_id=id).json_tickets
                    plase = {
                        'id': ticket['data']['id'],
                        'row': plane[int(ticket['data']['id'])]['row_real'] if plane[int(ticket['data']['id'])][
                            'row_real'] else plane[int(ticket['data']['id'])]['row'],
                        'plase': plane[int(ticket['data']['id'])]['plase_real'] if plane[int(ticket['data']['id'])][
                            'plase_real'] else plane[int(ticket['data']['id'])]['plase'],
                    }
                    category = TicketsCategory.objects.get(id=plane[int(ticket['data']['id'])]['category'])

                else:
                    category = TicketsCategory.objects.get(id=ticket['data']['id'])
                total_cost = category.price

                if promocode:
                    if not 'error' in promocode:
                        if promocode['type'] == 'percent':
                            total_cost = category.price - category.price * int(promocode['value']) / 100
                        if promocode['type'] == 'number':
                            total_cost = category.price - int(promocode['value'])

                if type(category.discount) == list:
                    for discount in category.discount:
                        if discount['type'] == 'online':
                            total_cost_temp = category.price
                            if discount['type_value'] == 'percent':
                                total_cost_temp = category.price - category.price * int(discount['value']) / 100
                            if discount['type_value'] == 'number':
                                total_cost_temp = category.price - int(discount['value'])
                            if total_cost_temp < total_cost:
                                total_cost = total_cost_temp

                ticket_obj = Tickets(number=number, category=category, code=code, order=order, total_cost=total_cost,
                                     first_cost=total_cost, plase=plase)

                ticket_obj.save()
                tickets_content.append(ticket_obj.code)

                order.amount += total_cost
        if promocode:
            if 'id' in promocode:
                order.loyalty = 'promocode'
                promocode_query = Promocode.objects.get(id=promocode['id'])
                order.loyalty_promocode = promocode_query

        order.save()

        # оплата
        sbr_data_account = project.sbr
        url = '%s/payment/rest/register.do?' \
              'userName=%s&password=%s' \
              '&amount=%d&description=%s&currency=643&language=ru&orderNumber=%d&' \
              'returnUrl=%s&pageView=DESKTOP&email=%s' % (
                  sbr_data_account.url, sbr_data_account.api, sbr_data_account.password,
                  order.amount * 100, project.name, orderNumber, url_abs,
                  email['data'])
        request_sbr = requests.post(url)
        content = json.loads(request_sbr._content.decode())

        url_status = '%s/payment/rest/getOrderStatusExtended.do?' \
                     'userName=%s&password=%s&language=ru&' \
                     'orderId=%s' % (
                         sbr_data_account.url, sbr_data_account.api, sbr_data_account.password, content['orderId'])
        request_sbr_status = requests.post(url_status)
        content_status = json.loads(request_sbr_status._content.decode())

        order.uuid = content['orderId']
        order.state = content_status['orderStatus']

        order.save()
        content['tickets'] = tickets_content

        return content

    def GenerateCodeTicket(id, tickets):
        if tickets.__len__():
            number = tickets[0].number + 1
        else:
            number = 1
        rnd = 13 - (int(math.log10(id)) + 1) - (int(math.log10(id + number)) + 1) - 2
        if rnd < 1: rnd = 1

        temp = random.randint(10 ** (rnd - 1), 10 ** (rnd) - 1)
        code = '%d0%d%d%d' % (id, temp, number + id, int(math.log10(number + id)) + 1)
        return number, code

    def pay_sbr(self, id, email, modalData, countSelectTicket, url_abs):

        json_email = email
        project = Project.objects.get(id=modalData['project'])
        orderNumber = int(mktime(datetime.datetime.now().timetuple()))

        order = Order(number=orderNumber,
                      project=project,
                      payment_method='online',
                      buyer=email['data'])

        order.save()
        order.amount = 0

        for count in range(int(countSelectTicket)):
            tickets = Tickets.objects.filter(category__project_id=id).order_by('-number')
            number, code = TicketsBuy.GenerateCodeTicket(id, tickets)
            category = TicketsCategory.objects.get(id=modalData['id'])

            total_cost = category.price
            if type(category.discount) == list:
                for discount in category.discount:
                    if discount['type'] == 'online':
                        total_cost_temp = category.price
                        if discount['type_value'] == 'percent':
                            total_cost_temp = category.price - category.price * int(discount['value']) / 100
                        if discount['type_value'] == 'number':
                            total_cost_temp = category.price - int(discount['value'])
                        if total_cost_temp < total_cost:
                            total_cost = total_cost_temp

            ticket = Tickets(number=number, category=category, code=code, order=order, total_cost=total_cost,
                             first_cost=total_cost)
            ticket.save()
            order.amount += total_cost

        order.save()

        # оплата

        sbr_data_account = project.sbr
        url = '%s/payment/rest/register.do?' \
              'userName=%s&password=%s' \
              '&amount=%d&description=%s&currency=643&language=ru&orderNumber=%d&' \
              'returnUrl=%s&pageView=DESKTOP&email=%s' % (
              sbr_data_account.url, sbr_data_account.api, sbr_data_account.password,
              order.amount * 100, project.name, orderNumber, url_abs,
              json_email['data'])
        request_sbr = requests.post(url)
        content = json.loads(request_sbr._content.decode())

        url_status = '%s/payment/rest/getOrderStatusExtended.do?' \
                     'userName=%s&password=%s&language=ru&' \
                     'orderId=%s' % (
                     sbr_data_account.url, sbr_data_account.api, sbr_data_account.password, content['orderId'])
        request_sbr_status = requests.post(url_status)
        content_status = json.loads(request_sbr_status._content.decode())

        order.uuid = content['orderId']
        order.state = content_status['orderStatus']
        order.save()

        return content

    @list_route(methods=['post', 'get'])
    def pay(self, request, id=None):

        modalData = request.data['modalData']
        countSelectTicket = request.data['countSelectTicket']

        project = Project.objects.get(id=modalData['project'])
        orderNumber = int(mktime(datetime.datetime.now().timetuple()))

        url_abs = request.build_absolute_uri().replace('pay', 'redirect_after_pay?id=1')
        email = {"email": request.session['code_email']['email']}
        json_email = email

        sbr_data_account = project.sbr
        url = '%s/payment/rest/register.do?' \
              'userName=%s&password=%s&' \
              'amount=%d&description=%s&currency=643&language=ru&orderNumber=%d&' \
              'returnUrl=%s&pageView=DESKTOP&email=%s' % (
              sbr_data_account.url, sbr_data_account.api, sbr_data_account.password,
              int(countSelectTicket) * modalData['price'] * 100, project.name, orderNumber, url_abs,
              json_email['email'])
        request_sbr = requests.post(url)
        content = json.loads(request_sbr._content.decode())

        url_status = '%s/payment/rest/getOrderStatusExtended.do?' \
                     'userName=%s&password=%s&language=ru&' \
                     'orderId=%s' % (
                         sbr_data_account.url, sbr_data_account.api, sbr_data_account.password,
                         content['orderId'])
        request_sbr_status = requests.post(url_status)
        content_status = json.loads(request_sbr_status._content.decode())

        order = Order(number=content_status['orderNumber'],
                      project=project,
                      uuid=content['orderId'],
                      state=content_status['orderStatus'],
                      buyer=request.session['code_email']['email'])
        order.save()

        for count in range(int(countSelectTicket)):
            tickets = Tickets.objects.filter(category__project_id=id).order_by('-number')
            if tickets.__len__():
                number = tickets[0].number + 1
            else:
                number = 1

            rnd = 13 - (int(math.log10(id)) + 1) - (int(math.log10(id + number)) + 1) - 2
            if rnd < 1: rnd = 1

            temp = random.randint(10 ** (rnd - 1), 10 ** (rnd) - 1)
            code = '%d0%d%d%d' % (id, temp, number + id, int(math.log10(number + id)) + 1)

            category = TicketsCategory.objects.get(id=modalData['id'])
            ticket = Tickets(number=number, category=category, code=code, order=order)
            ticket.save()

        return Response(content)

    @list_route(methods=['post'])
    def register(self, request, id=None):

        if 'code_email' in request.session:
            if 'email' in request.session['code_email']:
                return Response(request.session['code_email'])
        return Response({})

class TicketsPrint(viewsets.ModelViewSet):
    queryset = Tickets.objects.all()
    serializer_class = TicketsSerializer
    authentication_class = (JSONWebTokenAuthentication,)

    @staticmethod
    def ferma_get_data(order):
        try:
            url = ''
            data = json.dumps({'Login': settings.FERMA['Login'], 'Password': settings.FERMA['Password']})
            headers = {'Content-type': 'application/json', 'Content-Length': str(len(data)), 'charset': 'utf-8'}
            answer = requests.post('https://ferma.ofd.ru/api/Authorization/CreateAuthToken', data=data, headers=headers)
            if answer.json()['Status'] == 'Success':
                time.sleep(2)
                order = Order.objects.get(uuid=order)
                data = json.dumps({
                    'Request': {"ReceiptId": order.receipt_id['Income']['data']}
                })
                headers = {'Content-type': 'application/json', 'Content-Length': str(len(data)), 'charset': 'utf-8'}
                answer_receiptid = requests.post(
                    'https://ferma.ofd.ru/api/kkt/cloud/list?AuthToken=' + answer.json()['Data']['AuthToken'], data=data,
                    headers=headers)

                resp_receiptid = answer_receiptid.json()
                if resp_receiptid['Status'] == 'Success':

                    url = '%s/rec/%s/%s/%s/%s/%s' %(
                        'https://check-demo.ofd.ru' if settings.ENV=='DEBUG' else 'https://check.ofd.ru',
                        '7714784748' if settings.ENV=='DEBUG' else settings.FERMA['Inn'],
                        resp_receiptid['Data'][0]['Receipt']['cashboxInfoHolder']['RNM'],
                        resp_receiptid['Data'][0]['Receipt']['cashboxInfoHolder']['FN'],
                        resp_receiptid['Data'][0]['Receipt']['cashboxInfoHolder']['FDN'],
                        resp_receiptid['Data'][0]['Receipt']['cashboxInfoHolder']['FPD'])

            print(url)
        except Exception as e:
            errorStatus = e
            print('Ошибка авторизации')
            print(e)
        return url

    # создание билета для печати в киоске
    @staticmethod
    def create_ticket_check(id, user=None):

        order = Order.objects.get(id=id)
        printer = '0'
        if user:
            printer = KioskProject.objects.get(project=order.project.id, user=user).type_printing

        if printer == '1':
            data = TicketsPrint.create_ticket_check_1(id, id)

        if printer == '0':
            data = TicketsPrint.create_ticket_check_0(id, id)
        return data

    @staticmethod
    def create_ticket_check_0(id, order):

        C128 = barcode.get_barcode_class('code128')
        tickets = Tickets.objects.filter(order_id=order)

        for t in tickets:
            ticket_category = TicketsCategory.objects.get(id=t.category_id)
            writer = ImageWriter()
            writer.quiet_zone = 0.1
            writer.dpi = 400
            writer.module_width = 0.5

            c128 = C128(t.code, writer=writer)
            c128.save(settings.BASE_DIR + '/media/barcode/barcode_' + t.code)

            fnt = ImageFont.truetype(settings.BASE_DIR + '/static/fonts/DejaVuSansMono.ttf', 22)

            kiosk_tick = Image.new("RGB", size=(500, 900), color=(255, 255, 255, 0))
            draw_kiosk_tick = ImageDraw.Draw(kiosk_tick)

            text = '***********************************   \n'
            text += (ticket_category.project.name.replace(' ', '\n'))
            text += '\n***********************************   \n'

            text += 'Билет: №%s \n ' % (t.code,)
            if ticket_category.project.is_weekend:
                text += '%s %s, \n ' % (
                    ticket_category.name[:30], ('Выходной день' if ticket_category.type_in=='weekend' else 'Будний день'),)
            else:
                text += '%s \n ' % (ticket_category.name[:30])

            if ticket_category.project.is_pay:
                text += 'Стоимость: %s руб. \n ' % (ticket_category.price,)

            if t.order.buyer:
                text += 'Покупатель: %s. \n ' % (t.order.buyer,)

            if t.plase and type(t.plase) != str:
                text += 'ряд: %s место: %s \n' % (t.plase['row'], t.plase['plase'])

            serial = VisitorsSerializerList(t).data

            text += 'Дата: %s' % (serial['date_create'])

            draw_kiosk_tick.multiline_text((0, 10), text, font=fnt, fill=(0, 0, 0), spacing=10, align="center")

            n_count = 1

            for i in text:
                if i == '\n':
                    n_count += 1

            im1 = Image.open(settings.BASE_DIR + '/media/barcode/barcode_' + t.code + ".png")
            kiosk_tick.paste(im1, (0, 400))

            text = '***********************************   \n'
            text += 'ДОНЭКСПОЦЕНТР\n'
            text += 'donexpocentre.ru\n'
            draw_kiosk_tick.multiline_text((0, 800), text, font=fnt, fill=(0, 0, 0), spacing=10, align="center")
            kiosk_tick.save(settings.BASE_DIR + '/media/barcode/kioskticket_' + t.code + ".png")

            t.ticket_check = 'barcode/kioskticket_' + t.code + ".png"
            t.save()
        return 0

    @staticmethod
    def create_ticket_check_1(id, order):

        C128 = barcode.get_barcode_class('code128')
        tickets = Tickets.objects.filter(order_id=order)
        order_obj = Order.objects.get(id = order)

        url = TicketsPrint.ferma_get_data(order_obj.uuid)

        for t in tickets:
            ticket_category = TicketsCategory.objects.get(id=t.category_id)
            writer = ImageWriter()
            writer.quiet_zone = 0.1
            writer.dpi = 400
            writer.module_width = 0.5

            c128 = C128(t.code, writer=writer)
            c128.save(settings.BASE_DIR + '/media/barcode/barcode_' + t.code)

            fnt_head = ImageFont.truetype(settings.BASE_DIR + '/static/fonts/DejaVuSansMono.ttf', 35)
            fnt = ImageFont.truetype(settings.BASE_DIR + '/static/fonts/DejaVuSansMono.ttf', 25)

            result = Image.new("RGB", size=(1100, 1100), color=(255, 255, 255, 0))

            background = Image.new("RGB", size=(1000, 1000), color=(255, 255, 255, 0))
            draw = ImageDraw.Draw(background)

            draw.multiline_text((350, 148), t.category.project.name, font=fnt_head, fill=(0, 0, 0), align="left")

            text = ''
            if ticket_category.project.date_start!=ticket_category.project.date_end:

                text += 'c %s  по %s\n' % (ticket_category.project.date_start.strftime('%d-%m-%Y'), ticket_category.project.date_end.strftime('%d-%m-%Y'))
            else:
                text += '%s  %s\n' % (ticket_category.project.date_start.strftime('%H:%M'),
                                          ticket_category.project.time_start.strftime('%H:%M'))

            text+='%s руб.\n' %(t.total_cost)

            if ticket_category.project.is_weekend:
                text += '%s %s,' % (
                    ticket_category.name[:30], ('Выходной день' if ticket_category.is_weekend else 'Будний день'))
            else:
                text += '%s' % (
                    ticket_category.name[:30])

            if t.plase and type(t.plase) != str:
                text += ' ряд: %s место: %s' % (t.plase['row'], t.plase['plase'])
            draw.multiline_text((350, 200), text, font=fnt, fill=(0, 0, 0), align="left")

            text = 'Дата покупки: %s \nБилет: %s'%(datetime.datetime.now().strftime('%d-%m-%Y %H:%M'),t.code)
            draw.multiline_text((100, 323), text, font=fnt, fill=(0, 0, 0), align="left")

            result.paste(background, (0, 0))

            result = result.rotate(90)
            result = result.crop((0, 0, result.size[0] - 600, result.size[1],))

            im1 = Image.open(settings.BASE_DIR + '/media/barcode/barcode_' + t.code + ".png")
            left, top, right, bottom = 5, 0, im1.width - 5, 100
            im1 = im1.crop((left, top, right, bottom))
            result.paste(im1, (0, 50))

            print(t.code)
            result.save(settings.BASE_DIR + '/media/barcode/kioskticket_g_' + t.code + ".png")

            t.ticket_graph = 'barcode/kioskticket_g_' + t.code + ".png"
            t.save()

        return url

    @permission_classes((IsAuthenticated,))
    def retrieve(self, request, pk=None):
        content = {
            'tickets': [],
        }
        try:
            ticket = Tickets.objects.get(code=pk)
            content['data'] = {
                'order': ticket.order.number,
            }

            if ticket.count_printing>=2:
                return Response({'error': 'Лимит печати исчерпан'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            content['tickets'] = [TicketsSerializer(ticket).data, ]

            try:
                if KioskProject.objects.get(user=request.user, project=ticket.order.project).type_printing == '1':
                    file = open(settings.BASE_DIR + ticket.ticket_graph.url)
                else:
                    file = open(settings.BASE_DIR + ticket.ticket_check.url)
            except IOError as e:
                TicketsBuy.create_ticket_check(ticket.order.id)

        except Tickets.DoesNotExist:
            try:
                order = Order.objects.get(number=pk)
                tickets = Tickets.objects.filter(order=order)
                content['tickets'] = TicketsSerializer(tickets, many=True).data

                for ticket in content['tickets']:
                    if ticket['count_printing'] >= 2:
                        return Response({'error': 'Лимит печати исчерпан'},
                                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                    try:
                        if KioskProject.objects.get(user=request.user, project=order.project).type_printing == '1':
                            file = open(settings.BASE_DIR + ticket['ticket_graph'])
                        else:
                            file = open(settings.BASE_DIR + ticket['ticket_check'])
                    except IOError as e:
                        TicketsPrint.create_ticket_check(order.id)

                content['data'] = {
                    'order': order.number,
                }

            except Order.DoesNotExist:
                return Response({'error': 'Нет билета/заказа с таким номером'}, status=status.HTTP_404_NOT_FOUND)
        return Response(content)

    @list_route(methods=['get'])
    def update_count_printing(self, request, pk=None):
        if request.GET['data']:
            for t in json.loads(request.GET['data'])['arr']:
                ticket = Tickets.objects.get(code = t)
                ticket.count_printing = ticket.count_printing+1
                ticket.save()

        return Response({})

class VisitorsList(viewsets.ModelViewSet):
    queryset = Tickets.objects.all()
    serializer_class = VisitorsSerializer
    authentication_class = (JSONWebTokenAuthentication,)

    # permission_classes = (IsAuthenticated,)

    def list(self, request, id=None, pk=None):
        project = Project.objects.get(id=id)
        visitors = Tickets.objects.filter(category__project=project, order__state=1)
        count_visitors = visitors.count()

        if 'data' in request.GET:
            serverData = json.loads(request.GET['data'])

            if (serverData['sort']['field'] and serverData['sort']['type']):

                if serverData['sort']['field'] == 'buyer':
                    serverData['sort']['field'] = 'order__buyer'

                if serverData['sort']['type'] == 'desc':
                    serverData['sort']['field'] = '-' + serverData['sort']['field']

                visitors = visitors.order_by(serverData['sort']['field'])

            if 'select' in serverData and serverData['select']['state']:
                visitors = visitors.filter(**serverData['select'])

            if 'search' in serverData and len(serverData['search']['value']) >= 2:
                # visitors = visitors.filter(number__icontains=serverData['search']['value'])
                visitors = visitors.filter(Q(number__icontains=serverData['search']['value']) |
                                           Q(code__icontains=serverData['search']['value']) |
                                           Q(order__buyer__icontains=serverData['search']['value']))

            temp_content = VisitorsSerializerList(visitors, many=True).data
            paginator = Paginator(temp_content, serverData['perPage'])
            content = paginator.page(serverData['page']).object_list

            count_visitors = visitors.count()
            return Response({'rows': content, 'totalRecords': count_visitors})

        content = VisitorsSerializerList(visitors, many=True).data
        return Response({'rows': content, 'totalRecords': count_visitors})

    @list_route(methods=['get'])
    def list_kiosk(self, request, id=None, pk=None):

        if 'data' in request.GET:
            serverData = json.loads(request.GET['data'])

            visitors = Tickets.objects.filter(category__project__id=id)
            count_visitors = visitors.count()

            if 'search' in serverData and len(serverData['search']) < 3:
                return Response({'rows': {}, 'totalRecords': 0})

            if (serverData['sort']['field'] and serverData['sort']['type']):

                if serverData['sort']['field'] == 'buyer':
                    serverData['sort']['field'] = 'order__buyer'

                if serverData['sort']['type'] == 'desc':
                    serverData['sort']['field'] = '-' + serverData['sort']['field']

                visitors = visitors.order_by(serverData['sort']['field'])

            if 'search' in serverData and len(serverData['search']) >= 3:
                # visitors = visitors.filter(number__icontains=serverData['search']['value'])
                visitors = visitors.filter(Q(number__icontains=serverData['search']) |
                                           Q(code__icontains=serverData['search']) |
                                           Q(order__number__icontains=serverData['search']) |
                                           Q(order__pan__icontains=serverData['search']) |
                                           Q(order__buyer__icontains=serverData['search']))[:100]

            temp_content = VisitorsKioskSerializerList(visitors, many=True).data
            paginator = Paginator(temp_content, serverData['perPage'])
            content = paginator.page(serverData['page']).object_list

            count_visitors = visitors.count()

            return Response({'rows': content, 'totalRecords': count_visitors})

        return Response({'rows': {}, 'totalRecords': 0})

    def retrieve(self, request, id=None, pk=None):

        try:
            visitors = Tickets.objects.get(code=pk)
            content = VisitorsSerializer(visitors).data
        except Tickets.DoesNotExist:
            content = {}
        return Response(content)

    @list_route(methods=['get'])
    def options(self, request, id=None, ):

        content = {
            'column': [],
        }

        for field in Tickets._meta.get_fields():
            if field.name in ['uuid', 'order'] or field.one_to_many:
                continue
            content['column'].append({
                'value': field.column,
                'label': 'Билет. ' + field.verbose_name,
            })

        for field in Order._meta.get_fields():
            if field.name in ['tickets', 'id', 'uuid', 'project', 'cardAuthInfo', ]:
                continue
            content['column'].append({
                'value': "order__" + field.column,
                'label': 'Заказ. ' + field.verbose_name,
            })
        return Response(content)

    @list_route(methods=['get'])
    def forms(self, request, id=None, ):

        obj = Forms_Data.objects.filter(ticket__order__project__id=id).order_by('-date')
        serializers = Forms_DataSerializers(obj, many=True)

        form = Forms.objects.get(project__id=id, type='registr')
        serializers_form = FormsSerializers(form)

        return Response({'form_data': serializers.data, 'form': serializers_form.data})

        project = Project.objects.get(id=id)
        visitors = Tickets.objects.filter(category__project=project, order__state=1)
        count_visitors = visitors.count()

        if 'data' in request.GET:
            serverData = json.loads(request.GET['data'])

            if (serverData['sort']['field'] and serverData['sort']['type']):

                if serverData['sort']['field'] == 'buyer':
                    serverData['sort']['field'] = 'order__buyer'

                if serverData['sort']['type'] == 'desc':
                    serverData['sort']['field'] = '-' + serverData['sort']['field']

                visitors = visitors.order_by(serverData['sort']['field'])

            if 'select' in serverData and serverData['select']['state']:
                visitors = visitors.filter(**serverData['select'])

            if 'search' in serverData and len(serverData['search']['value']) >= 2:
                # visitors = visitors.filter(number__icontains=serverData['search']['value'])
                visitors = visitors.filter(Q(number__icontains=serverData['search']['value']) |
                                           Q(code__icontains=serverData['search']['value']) |
                                           Q(order__buyer__icontains=serverData['search']['value']))

            temp_content = VisitorsSerializerList(visitors, many=True).data
            paginator = Paginator(temp_content, serverData['perPage'])
            content = paginator.page(serverData['page']).object_list

            count_visitors = visitors.count()

            return Response({'rows': content, 'totalRecords': count_visitors})

        content = VisitorsSerializerList(visitors, many=True).data
        return Response({'rows': content, 'totalRecords': count_visitors})

    @list_route(methods=['get'])
    def get_type_field(self, request, id=None, ):

        if request.GET['data']:
            data = json.loads(request.GET['data'])['field'].split('__')

            model = Tickets
            if data.__len__() > 1:
                if data[0] == 'order':
                    model = Order
                    data[0] = data[1]

            type_field = model._meta.get_field(data[0]).get_internal_type()
            choices = model._meta.get_field(data[0]).choices

            if type_field == 'ForeignKey':
                t_category = model._meta.get_field(data[0]).remote_field.model.objects.filter(project_id=id)
                choices = []
                for t_c in t_category:
                    choices.append([t_c.id, t_c.name + (' (Выходной)' if t_c.is_weekend else ' (Будний)')])

            return Response({
                'type_field': type_field,
                'choices': choices
            })

    @list_route(methods=['get', 'post'])
    def get_report(self, request, id=None, ):

        if request.GET['data']:

            try:
                from StringIO import StringIO
            except ImportError:
                from io import BytesIO

            from django.http import HttpResponse

            from xlsxwriter.workbook import Workbook

            output = BytesIO()

            book = Workbook(output)
            sheet = book.add_worksheet('test')

            head = book.add_format({
                'bold': 1,
                'align': 'center',
                'valign': 'vcenter',
            })

            text = book.add_format({
                'align': 'center',
                'valign': 'vcenter',
            })

            sheet.set_default_row(18)
            sheet.set_column('A:Z', 25)

            data = json.loads(request.GET['data'])

            project = Project.objects.get(id=id)
            tickets = Tickets.objects.filter(category__project=project)

            for filter in data['filters']:
                if filter['table'] == 'Order':
                    filter['field'] = 'order__' + filter['field']

            for filter in data['filters']:
                filter['old'] = True

                if filter['compare'] != 'exclude' and filter['compare'] != '__icontains':
                    tickets = tickets.filter(**{filter['field'] + filter['compare']: filter['data']})

                if filter['compare'] == 'exclude':
                    tickets = tickets.exclude(**{filter['field']: filter['data']})

                if filter['compare'] == '__icontains':
                    tickets = tickets.filter(**{filter['field'] + filter['compare']: filter['data']})


            row = 0
            col = 0
            for i in data['fields']['Tickets']:
                field = Tickets._meta.get_field(i)
                if field:
                    sheet.write(row, col, field.verbose_name, head)
                col += 1
            for i in data['fields']['Order']:
                field = Order._meta.get_field(i)
                if field:
                    sheet.write(row, col, field.verbose_name, head)
                col += 1
            row += 1
            col = 0

            for t in tickets:
                ticket = TicketsReportSerializer(t).data
                for i in data['fields']['Tickets']:

                    if i in ticket:
                        sheet.write(row, col, ticket[i], text)
                    i_temp = i.replace('_id', '')

                    if i_temp in ticket:
                        sheet.write(row, col, ticket[i_temp], text)
                    col += 1

                order = OrderSerializer(t.order).data
                for i in data['fields']['Order']:
                    if i in order:
                        sheet.write(row, col, order[i], text)
                    col += 1
                col = 0
                row += 1

            book.close()

            output.seek(0)
            response = HttpResponse(output.read(),
                                    content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            response['Content-Disposition'] = "attachment; filename=report.xlsx"

        return response

    @list_route(methods=['get', 'post'])
    def get_report_statistic(self, request, id=None, ):

        if request.GET['data']:

            try:
                from StringIO import StringIO
            except ImportError:
                from io import BytesIO

            from django.http import HttpResponse

            from xlsxwriter.workbook import Workbook

            output = BytesIO()

            book = Workbook(output)

            data = json.loads(request.GET['data'])
            project = Project.objects.get(id=id)

            merge_format = book.add_format({
                'bold': 1,
                'border': 1,
                'align': 'center',
                'valign': 'vcenter',
                'fg_color': '#f9f9f9'})

            head = book.add_format({
                'bold': 1,
                'border': 1,
                'align': 'center',
                'valign': 'vcenter',
            })

            text = book.add_format({
                'align': 'center',
                'valign': 'vcenter',
                'border': 1,
            })

            if 'buy' in data:
                sheet = book.add_worksheet('page1')
                sheet.set_default_row(18)
                sheet.set_column('A:A', 30)
                sheet.set_column('B:Z', 12)

                row = 0;

                tickets = Tickets.objects.filter(category__project=project).order_by('date_create')
                tickets = tickets.filter(**data['buy'])
                date_array = []
                for ticket in tickets:
                    if not ticket.date_create.strftime("%d-%m-%Y") in date_array:
                        date_array.append(ticket.date_create.strftime("%d-%m-%Y"))

                sheet.merge_range(row, 0, row, date_array.__len__() + 1, 'Статистика по купленным билетам',
                                  merge_format)
                row += 1
                col = 1

                # даты
                for date in date_array:
                    sheet.write(row, col, date, head)
                    col += 1

                row += 1
                col = 1

                sheet.write(row, 0, 'Всего продано', head)
                sheet.write(row + 1, 0, 'Процент от общего числа', head)

                chart_data_buy = []

                for date in date_array:
                    temp_date = date.split('-')
                    ticket_filter_day = tickets.filter(date_create__day=temp_date[0],
                                                       date_create__year=temp_date[2])
                    sheet.write(row, col, ticket_filter_day.count(), text)
                    sheet.write(row + 1, col, str(round((ticket_filter_day.count() / tickets.count() * 100))) + '%',
                                text)
                    if data['buy_chart']['base']:
                        chart_data_buy.append(ticket_filter_day.count())

                    col += 1

                if data['buy_chart']['base']:
                    chart_buy = book.add_chart({'type': 'line'})
                    chart_buy.add_series({
                        'name': ['page1', 0, 0],
                        'categories': ['page1', row - 1, 1, row - 1, col - 1],
                        'values': ['page1', row, 1, row, col - 1],
                    })

                sheet.write(row - 1, col, 'Всего', head)
                sheet.write(row, col, tickets.count(), text)

                row += 2
                col = 0
                # По категориям
                for c in data['buy']['category_id__in']:
                    ticket_category = TicketsCategory.objects.get(id=c)
                    sheet.write(row, col, ticket_category.name, head)
                    tickets_filter_category = tickets.filter(category=ticket_category)
                    col = 1

                    for date in date_array:
                        temp_date = date.split('-')
                        ticket_filter_day = tickets_filter_category.filter(date_create__day=temp_date[0],
                                                                           date_create__month=temp_date[1],
                                                                           date_create__year=temp_date[2])
                        sheet.write(row, col, ticket_filter_day.count(), text)
                        col += 1
                    sheet.write(row, col, tickets_filter_category.count(), text)
                    row += 1
                    col = 0

                row += 1
                if data['buy_chart']['base']:
                    chart_buy.set_size({'width': 300 + (80 * date_array.__len__()), 'height': 300})
                    sheet.insert_chart(row, 0, chart_buy)

                row += 14

            if 'activate' in data:
                sheet2 = book.add_worksheet('page2')

                sheet2.set_default_row(18)
                sheet2.set_column('A:A', 30)
                sheet2.set_column('B:Z', 12)

                row = 0;
                col = 0
                tickets = Tickets.objects.filter(category__project=project).order_by('date_activate')
                tickets = tickets.filter(**data['activate'])
                date_array = []
                for ticket in tickets:
                    if not ticket.date_activate.strftime("%d-%m-%Y") in date_array:
                        date_array.append(ticket.date_activate.strftime("%d-%m-%Y"))

                sheet2.merge_range(row, 0, row, date_array.__len__() + 1, 'Статистика по активированным билетам',
                                   merge_format)
                row += 1
                col = 1
                # даты
                for date in date_array:
                    sheet2.write(row, col, date, head)
                    col += 1

                row += 1
                col = 1

                sheet2.write(row, 0, 'Всего продано', head)
                sheet2.write(row + 1, 0, 'Процент от общего числа', head)

                # общие даные
                chart_data_activate = []
                for date in date_array:
                    temp_date = date.split('-')
                    ticket_filter_day = tickets.filter(date_activate__day=temp_date[0],
                                                       date_activate__month=temp_date[1],
                                                       date_activate__year=temp_date[2])
                    sheet2.write(row, col, ticket_filter_day.count(), text)
                    sheet2.write(row + 1, col, str(round((ticket_filter_day.count() / tickets.count() * 100))) + '%',
                                 text)

                    if data['activate_chart']['base']:
                        chart_data_activate.append(ticket_filter_day.count())
                    col += 1

                if data['activate_chart']['base']:
                    chart_activate = book.add_chart({'type': 'line'})
                    chart_activate.add_series({
                        'name': ['page2', 0, 0],
                        'categories': ['page2', row - 1, 1, row - 1, col - 1],
                        'values': ['page2', row, 1, row, col - 1],
                    })

                sheet2.write(row - 1, col, 'Всего', head)
                sheet2.write(row, col, tickets.count(), text)
                row += 2
                col = 0

                # По категориям
                for c in data['activate']['category_id__in']:
                    ticket_category = TicketsCategory.objects.get(id=c)
                    sheet2.write(row, col, ticket_category.name, head)
                    tickets_filter_category = tickets.filter(category=ticket_category)

                    col = 1
                    for date in date_array:
                        temp_date = date.split('-')

                        ticket_filter_day = tickets_filter_category.filter(date_activate__day=temp_date[0],
                                                                           date_activate__month=temp_date[1],
                                                                           date_activate__year=temp_date[2])
                        sheet2.write(row, col, ticket_filter_day.count(), text)
                        col += 1
                    sheet2.write(row, col, tickets_filter_category.count(), text)
                    row += 1
                    col = 0
                row += 1

                if data['activate_chart']['base']:
                    chart_activate.set_size({'width': 300 + (80 * date_array.__len__()), 'height': 300})
                    sheet2.insert_chart(row, 0, chart_activate)

                row += 14

        book.close()

        output.seek(0)
        response = HttpResponse(output.read(),
                                content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        response['Content-Disposition'] = "attachment; filename=statistic.xlsx"
        return response

    def create_Report_project(data_request, id, ):

        def rek(mass, sort, layer):
            for i in mass:
                if isinstance(mass[i], QuerySet):

                    if sort == 'category':

                        temp = mass[i]
                        mass[i] = {}
                        for c in category:

                            if c.type_in == 'weekend':
                                mass[i][c.name + ' \n(Вых)'] = temp.filter(category__id=c.id)
                            else:
                                mass[i][c.name] = temp.filter(category__id=c.id)

                    if sort == 'pay':
                        temp = mass[i]
                        mass[i] = {}
                        for c in Order.PAYMENT_METHOD:
                            mass[i][c[1]] = temp.filter(order__payment_method=c[0])
                    if sort == 'day':
                        temp = mass[i]
                        mass[i] = {}
                        for d in date:
                            temp_date = d.split('-')

                            if data['type_report'] == 'people':
                                mass[i][d] = temp.filter(date_activate__day=temp_date[0],
                                                         date_activate__month=temp_date[1],
                                                         date_activate__year=temp_date[2])

                            if data['type_report'] == 'money':
                                mass[i][d] = temp.filter(date_create__day=temp_date[0],
                                                         date_create__month=temp_date[1],
                                                         date_create__year=temp_date[2])

                    if layer <= 1 or layer >= max:
                        mass[i]['Всего'] = temp
                else:
                    rek(mass[i], sort, layer)

        def convert_count(mass):
            for i in mass:
                if isinstance(mass[i], QuerySet):

                    if data['type_report'] == 'people':
                        mass[i] = mass[i].count()

                    if data['type_report'] == 'money':
                        temp_summ = 0
                        temp_summ_add = 0
                        for obj in mass[i]:
                            temp_summ += (obj.first_cost if obj.first_cost else obj.category.price)
                            if obj.total_cost:
                                temp_summ_add += obj.total_cost
                            else:
                                temp_summ_add += (obj.first_cost if obj.first_cost else obj.category.price)

                        if temp_summ_add - temp_summ > 0:
                            mass[i] = '%d (%d/%d)' % (temp_summ_add, (temp_summ_add - temp_summ), temp_summ)
                        else:
                            mass[i] = temp_summ
                else:
                    convert_count(mass[i])
        data = data_request

        project = Project.objects.get(id=id)
        category = TicketsCategory.objects.filter(project=project)
        tickets = Tickets.objects.filter(category__project_id=project.id).exclude(order__state=3).exclude(
            order__state=0)

        if data['type_report'] == 'people':
            tickets = tickets.filter(state=True, status=True, order__state=1).order_by('date_activate')
            if not data['all_date']:
                tickets = tickets.filter(date_activate__gte=data['start'],
                                         date_activate__lte=datetime.datetime.strptime(data['end'],
                                                                                       '%Y-%m-%d') + timedelta(days=1))

        if data['type_report'] == 'money':
            tickets = tickets.exclude(status=False, order__state=2).order_by('date_create')
            if not data['all_date']:
                tickets = tickets.filter(date_create__gte=data['start'],
                                         date_create__lte=datetime.datetime.strptime(data['end'],
                                                                                     '%Y-%m-%d') + timedelta(days=1))

        date = []
        for t in tickets:
            if data['type_report'] == 'people':
                if t.date_activate.strftime('%d-%m-%Y') in date:
                    continue
                else:
                    date.append((t.date_activate + timedelta(hours=3)).strftime('%d-%m-%Y'))

            if data['type_report'] == 'money':
                if t.date_create.strftime('%d-%m-%Y') in date:
                    continue
                else:
                    date.append((t.date_create + timedelta(hours=3)).strftime('%d-%m-%Y'))

        result = {
            0: tickets
        }

        layer = 0
        max = data['type_value'].__len__()

        if 'day' in data['type_value']:
            layer += 1
            rek(result, 'day', layer)

        if 'pay' in data['type_value']:
            layer += 1
            rek(result, 'pay', layer)

        if 'category' in data['type_value']:
            layer += 1
            rek(result, 'category', layer)

        convert_count(result)

        return result

    @list_route(methods=['get', 'post'])
    def get_report_project(self, request, id=None, ret=None, ):

        if request.GET['data']:

            res = VisitorsList.create_Report_project(request.GET['data'], id)
            return Response(res)

            for filter in data['filters']:
                if filter['table'] == 'Order':
                    filter['field'] = 'order__' + filter['field']

            for filter in data['filters']:
                filter['old'] = True

                if filter['compare'] != 'exclude' and filter['compare'] != '__icontains':
                    tickets = tickets.filter(**{filter['field'] + filter['compare']: filter['data']})

                if filter['compare'] == 'exclude':
                    tickets = tickets.exclude(**{filter['field']: filter['data']})

                if filter['compare'] == '__icontains':
                    tickets = tickets.filter(**{filter['field'] + filter['compare']: filter['data']})

            row = 0
            col = 0
            for i in data['fields']['Tickets']:
                field = Tickets._meta.get_field(i)
                if field:
                    sheet.write(row, col, field.verbose_name, head)
                col += 1
            for i in data['fields']['Order']:
                field = Order._meta.get_field(i)
                if field:
                    sheet.write(row, col, field.verbose_name, head)
                col += 1

            row += 1
            col = 0

            for t in tickets:
                ticket = TicketsReportSerializer(t).data
                for i in data['fields']['Tickets']:

                    if i in ticket:
                        sheet.write(row, col, ticket[i], text)
                    i_temp = i.replace('_id', '')

                    if i_temp in ticket:
                        sheet.write(row, col, ticket[i_temp], text)
                    col += 1

                order = OrderSerializer(t.order).data
                for i in data['fields']['Order']:
                    if i in order:
                        sheet.write(row, col, order[i], text)
                    col += 1
                col = 0
                row += 1

            book.close()
            output.seek(0)
            response = HttpResponse(output.read(),
                                    content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            response['Content-Disposition'] = "attachment; filename=report.xlsx"

        return response

    def create_uploading(self, data, id):
        tickets = Tickets.objects.filter(category__project__id=id, order__state=1)
        temp_repeat = {}
        for d in data['filter']:
            if not d['column'] in temp_repeat:
                temp_repeat[d['column']] = []

            temp_repeat[d['column']].append(d)

        for d in temp_repeat:
            union = False
            for item in temp_repeat[d]:
                if item['operator'] == 'exclude':
                    tickets = tickets.exclude(**{item['column']: item['value']})
                elif item['operator'] != '':
                    tickets = tickets.filter(**{item['column'] + item['operator']: item['value']})
                else:
                    if not union:
                        union = tickets.filter(**{item['column'] + item['operator']: item['value']})
                    else:
                        union = (union | tickets.filter(
                            **{item['column'] + item['operator']: item['value']})).distinct()
            if isinstance(union, QuerySet):
                tickets = union

        tick = VisitorsSerializer(tickets, many=True)
        return tick.data

    @list_route(methods=['get', 'post'])
    def download_report(self, request, id=None, ):

        if request.GET['data']:
            data = json.loads(request.GET['data'])

            try:
                from StringIO import StringIO
            except ImportError:
                from io import BytesIO

            from django.http import HttpResponse

            from xlsxwriter.workbook import Workbook

            output = BytesIO()

            book = Workbook(output)
            sheet = book.add_worksheet('test')

            head = book.add_format({
                'bold': 1,
                'align': 'center',
                'valign': 'vcenter',
            })

            text = book.add_format({
                'align': 'center',
                'valign': 'vcenter',
            })

            if data['type'] == 'unloading':
                sheet.set_default_row(18)
                sheet.set_column('A:Z', 10)

                if data['data']['table'] == 'orders':
                    value = OrdersList.create_uploading(self, data['data'], id)
                    model = Order

                if data['data']['table'] == 'visitors':
                    value = VisitorsList.create_uploading(self, data['data'], id)
                    model = Tickets

                col = 0
                row = 1
                for column in data['data']['columns']:
                    if data['data']['table'] == 'visitors':

                        if '__' in column:
                            fields = Order._meta.get_field(column.split('__')[1])
                            sheet.write(1, col, fields.verbose_name, head)
                            col += 1
                        else:

                            fields = model._meta.get_field(column)
                            sheet.write(1, col, fields.verbose_name, head)
                            col += 1

                    else:
                        fields = model._meta.get_field(column)
                        sheet.write(1, col, fields.verbose_name, head)
                        col += 1
                row += 1

                for val in value:
                    col = 0
                    for column in data['data']['columns']:
                        if column in val:
                            if column == 'category_id':
                                column = 'category'
                            sheet.write(row, col, str(val[column]), text)
                        if '__' in column:
                            if column.split('__')[1] in val:
                                sheet.write(row, col, str(val[column.split('__')[1]]), text)
                        col += 1
                    row += 1

                book.close()
                output.seek(0)
                response = HttpResponse(output.read(),
                                        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                response['Content-Disposition'] = "attachment; filename=report.xlsx"

                return response

            else:
                data = json.loads(request.GET['data'])
                res = VisitorsList.create_Report_project(data['data'], id)

                sheet.set_default_row(18)
                sheet.set_column('A:Z', 10)

                def rek(mass, row, col):
                    if isinstance(mass, dict):
                        for i in mass:
                            if isinstance(mass[i], dict):
                                col += 1
                                sheet.merge_range(1, col, 1, (col + mass[i].__len__() - 1), i, head)
                                rek(mass[i], row + 1, col - 1)
                                col += mass[i].__len__() - 1
                            else:
                                col += 1
                                sheet.write(row, col, i, head)
                    else:
                        col += 1
                        sheet.write(row, col, mass, head)

                def rek_data(mass, row, col):
                    if isinstance(mass, dict):
                        for i in mass:
                            if isinstance(mass[i], dict):
                                col += 1
                                rek_data(mass[i], row, col - 1)
                                col += mass[i].__len__() - 1

                            else:
                                # col += 1
                                sheet.write(row, col, mass[i], text)
                                col += 1
                    else:
                        sheet.write(row, col, mass, text)
                        col += 1

                rek(res[0][list(res[0].keys())[0]], 1, 0)
                row = json.loads(request.GET['data'])['data']['type_value'].__len__()
                col = 0
                for r in res[0]:
                    sheet.write(row, col, r, head)
                    row += 1
                row = json.loads(request.GET['data'])['data']['type_value'].__len__()
                col = 1

                for r in res[0]:
                    rek_data(res[0][r], row, col)
                    row += 1

                book.close()

                output.seek(0)
                response = HttpResponse(output.read(),
                                        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                response['Content-Disposition'] = "attachment; filename=report.xlsx"

                return response

class OrdersList(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializerData
    authentication_class = (JSONWebTokenAuthentication,)

    # permission_classes = (IsAuthenticated,)

    def list(self, request, id=None, pk=None):
        project = Project.objects.get(id=id)
        orders = Order.objects.filter(project=project)
        count_orders = orders.count()

        if 'data' in request.GET:
            serverData = json.loads(request.GET['data'])

            if (serverData['sort']['field'] and serverData['sort']['type']):

                if serverData['sort']['type'] == 'desc':
                    serverData['sort']['field'] = '-' + serverData['sort']['field']

                    orders = orders.order_by(serverData['sort']['field'])

            if 'select' in serverData and serverData['select']['state']:
                orders = orders.filter(**serverData['select'])

            if 'search' in serverData and len(serverData['search']['value']) >= 2:
                # visitors = visitors.filter(number__icontains=serverData['search']['value'])
                orders = orders.filter(Q(number__icontains=serverData['search']['value']) |
                                       Q(buyer__icontains=serverData['search']['value']) |
                                       Q(pan__icontains=serverData['search']['value']))

            temp_content = OrderSerializer(orders, many=True).data
            paginator = Paginator(temp_content, serverData['perPage'])
            content = paginator.page(serverData['page']).object_list

            count_orders = orders.count()

            return Response({'rows': content, 'totalRecords': count_orders})

        content = OrderSerializer(orders, many=True).data
        return Response({'rows': content, 'totalRecords': count_orders})

    def retrieve(self, request, id=None, pk=None):
        try:
            order = Order.objects.get(number=pk)
            content = OrderSerializerData(order).data
        except Tickets.DoesNotExist:
            content = {}
        return Response(content)

    @list_route(methods=['get'])
    def get_type_field(self, request, id=None, ):

        if request.GET['data']:
            data = json.loads(request.GET['data'])['field'].split('__')

            model = Order
            if data.__len__() > 1:
                if data[0] == 'order':
                    model = Order
                    data[0] = data[1]

            type_field = model._meta.get_field(data[0]).get_internal_type()
            choices = model._meta.get_field(data[0]).choices

            if type_field == 'ForeignKey':
                t_category = model._meta.get_field(data[0]).remote_field.model.objects.filter(project_id=id)
                choices = []
                for t_c in t_category:
                    choices.append([t_c.id, t_c.name])

            return Response({
                'type_field': type_field,
                'choices': choices
            })

    @list_route(methods=['get'])
    def options(self, request, id=None, ):

        content = {
            'column': [],
        }

        for field in Order._meta.get_fields():
            if field.name in ['tickets', 'id', 'uuid', 'project', 'cardAuthInfo', ]:
                continue
            content['column'].append({
                'value': field.column,
                'label': '' + field.verbose_name,
            })

        return Response(content)

    @detail_route(methods=['get'])
    def tickets(self, request, id=None, pk=None):
        try:
            order = Order.objects.get(number=pk)
            tickets = Tickets.objects.filter(order_id=order.id)
            content = VisitorsSerializer(tickets, many=True).data
        except Tickets.DoesNotExist:
            content = {}
        return Response(content)

    @detail_route(methods=['post'])
    def cancel_pay(self, request, id=None, pk=None):
        # try:
        order = Order.objects.get(id=pk)

        tickets = Tickets.objects.filter(order_id=order.id)

        for t in tickets:
            if t.state == True:
                return Response({'error': 'Есть активированные билеты'}, status=status.HTTP_400_BAD_REQUEST)
        sbr_data_account = order.project.sbr
        url = '%s/payment/rest/reverse.do' \
              '?userName=%s&password=%s&language=ru&orderNumber=%s' % (
              sbr_data_account.url, sbr_data_account.api, sbr_data_account.password, order.number)
        request_sbr = requests.post(url)
        content = json.loads(request_sbr._content.decode())

        if content['errorCode'] == '0':
            order.state = 2
            order.save()
            ferma.delay(order.uuid, 'IncomeReturn')

            tickets = Tickets.objects.filter(order_id=order.id)

            for t in tickets:
                t.status = False
                t.save()

        else:
            return Response({'error': content['errorMessage']}, status=status.HTTP_400_BAD_REQUEST)

        return Response({})

    def create_uploading(self, data, id):
        tickets = Order.objects.filter(project__id=id)
        temp_repeat = {}
        for d in data['filter']:
            if not d['column'] in temp_repeat:
                temp_repeat[d['column']] = []

            temp_repeat[d['column']].append(d)

        for d in temp_repeat:
            union = False
            for item in temp_repeat[d]:
                if item['operator'] == 'exclude':
                    tickets = tickets.exclude(**{item['column']: item['value']})
                elif item['operator'] != '':
                    tickets = tickets.filter(**{item['column'] + item['operator']: item['value']})
                else:
                    if not union:
                        union = tickets.filter(**{item['column'] + item['operator']: item['value']})
                    else:
                        union = (union | tickets.filter(
                            **{item['column'] + item['operator']: item['value']})).distinct()
            if isinstance(union, QuerySet):
                tickets = union

        tick = OrderSerializer(tickets, many=True)
        return tick.data


class PromocodeList(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = PromocodeSerializer
    authentication_class = (JSONWebTokenAuthentication,)

    # permission_classes = (IsAuthenticated,)

    def list(self, request, id=None, pk=None):
        project = Project.objects.get(id=id)
        promocode = Promocode.objects.filter(project=project)
        count_promocode = promocode.count()

        if 'data' in request.GET:
            serverData = json.loads(request.GET['data'])

            if (serverData['sort']['field'] and serverData['sort']['type']):

                if serverData['sort']['type'] == 'desc':
                    serverData['sort']['field'] = '-' + serverData['sort']['field']

                    promocode = promocode.order_by(serverData['sort']['field'])

            if 'select' in serverData and serverData['select']['contact']:
                # promocode = promocode.filter(**serverData['select'])

                if serverData['select']['contact'] == '1':
                    promocode = promocode.exclude(contact=None).exclude(contact=[])
                if serverData['select']['contact'] == '0':
                    promocode = promocode.filter(Q(contact=None) |
                                                 Q(contact=[]))
            if 'search' in serverData and len(serverData['search']['value']) >= 2:
                # visitors = visitors.filter(number__icontains=serverData['search']['value'])
                promocode = promocode.filter(Q(code__icontains=serverData['search']['value']) |
                                             Q(contact__icontains=serverData['search']['value']) |
                                             Q(value__icontains=serverData['search']['value']))

            temp_content = PromocodeSerializer(promocode, many=True).data
            paginator = Paginator(temp_content, serverData['perPage'])
            content = paginator.page(serverData['page']).object_list

            count_promocode = promocode.count()

            return Response({'rows': content, 'totalRecords': count_promocode})

        content = PromocodeSerializer(promocode, many=True).data
        return Response({'rows': content, 'totalRecords': count_promocode})

    @list_route(methods=['get'])
    def options(self, request, id=None, ):

        data = {}
        type_promo = Promocode.TYPE_PROMO
        data['type'] = []
        for t in type_promo:
            data['type'].append({
                'key': t[0],
                'value': t[1],
            })

        data['tickets_category'] = []

        ticket_category = TicketsCategory.objects.filter(project__id=id)
        for t in ticket_category:
            data['tickets_category'].append({
                'key': t.id,
                'value': t.name,
            })
        return Response(data)

    def retrieve(self, request, id=None, pk=None):

        try:
            promocode = Promocode.objects.get(id=pk)
        except:
            promocode = Promocode()

        content = PromocodeSerializer(promocode).data
        return Response(content)

    # def create(self, request):
    #     pass
    @list_route(methods=['get', 'post', 'patch'])
    def update_data(self, request, id=None, pk=None):
        if request.data:

            data = request.data.copy()
            if 'id' in data:
                promocode = Promocode.objects.get(id=data['id'])
            else:
                promocode = Promocode()

            if promocode:
                data.update({'project': id})
                # del data['tickets_category']
                if not ('count' in  data):
                    data.update({'count': None})
            serializer = PromocodeSerializerUpdate(promocode, data=data)
            if serializer.is_valid():
                serializer.validated_data
                obj = serializer.save()

                if 'tickets_category' in request.data:
                    a = 0
                    for cat in request.data['tickets_category']:
                        category = TicketsCategory.objects.get(id=cat)
                        obj.tickets_category.add(category)

                content = PromocodeSerializer(Promocode.objects.get(id=obj.id)).data
                return Response(content)

            return Response({})

    @list_route(methods=['post'])
    def delete_data(self, request, id=None, pk=None):

        if request.data:
            promocode = Promocode.objects.get(id=request.data['id'])
            promocode.delete()

        return Response({})
