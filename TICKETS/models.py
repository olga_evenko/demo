import datetime
from django.db import models
import uuid

from PROJECT.models import Project
from plus_administration.models import ExtUser
from django.contrib.postgres.fields import JSONField
from simple_history.models import HistoricalRecords
from django.utils import timezone
ACTION = (
    ('create', 'Создание'),
    ('update', 'Изменение')
)
class CashChange(models.Model):
    date_close = models.DateTimeField(
        verbose_name='Дата закрытия',
        auto_now_add=True,
    )

    class Meta:
        db_table = 'TICKETS_CashChange'

class TicketsCategory(models.Model):
    TYPE_DISCOUNT = (
        ('online', 'Покупка on-line'),
        ('date', 'Дата покупки')
    )
    TYPE_VALUE_DISCOUNT = (
        ('percent', 'Процент'),
        ('number', 'Число')
    )
    TYPE_IN = (
        ('weekend', 'Выходной'),
        ('weekdays', 'Будний'),
        ('all', 'Все дни'),
    )

    name = models.CharField(
        max_length=250, verbose_name='Заголовок',
        blank=True, null=True
    )
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE,
        verbose_name='Проект',
        null=False, blank=False,
    )
    price = models.FloatField(
        verbose_name='Цена',
        blank=True, null=True
    )
    color = models.CharField(
        max_length=25, verbose_name='Цвет',
        blank=True, null=True
    )
    is_weekend = models.BooleanField(
        verbose_name='Выходной день'
    )

    type_in = models.CharField(
        max_length=10, verbose_name='Тип входа',
        choices=TYPE_IN, default='all'
    )

    # is_free = models.BooleanField(
    #     verbose_name='Бесплатный вход',
    #     default=False,
    # )
    count_all = models.IntegerField(
        verbose_name='Всего билетов',
        blank=True, null=True,
    )

    count = models.IntegerField(
        verbose_name='Минимальное количество',
        default=1
    )
    description = models.CharField(
        max_length=2000, verbose_name='Описание категории',
        blank=True, )

    ticket_img = models.ImageField(
        blank=True,
        upload_to='./ticket_logo/',
        verbose_name='Билет')

    discount = JSONField(
        verbose_name='Скидка')

    date_create = models.DateField(
        verbose_name='Дата создания',
        blank=True, null=True
    )
    date_update = models.DateField(
        verbose_name='Дата обновления',
        blank=True, null=True
    )
    history = HistoricalRecords()


    class Meta:
        db_table = 'TICKETS_TicketsCategory'
        ordering = ['-price']

    def save(self, *args, **kwargs):
        try:
            obj = TicketsCategory.objects.get(id=self.id)
            self.date_update = datetime.datetime.today()

        except TicketsCategory.DoesNotExist:

            self.date_create = datetime.datetime.today()

        super(TicketsCategory, self).save(*args, **kwargs)

class Receptions(models.Model):

    name = models.CharField(
        max_length=250, verbose_name='Название',
        blank=True
    )
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE,
        verbose_name='Проект',
        null=False, blank=False,
    )
    # date_start = models.DateField(
    #     verbose_name='Дата начала работы',
    #     blank=True, null=True,
    # )
    # date_end = models.DateField(
    #     verbose_name='Дата окончания работы',
    #     blank=True, null=True,
    # )
    # time_start = models.TimeField(
    #     blank=True, null=True,
    #     verbose_name='Время начала работы')
    #
    # time_end = models.TimeField(
    #     blank=True, null=True,
    #     verbose_name='Время окончания работы')

    user = models.ForeignKey(ExtUser, on_delete=models.CASCADE,
        verbose_name='Пользователь',
        blank=True, null=True)

    ticket_category = models.ManyToManyField(TicketsCategory)

    history = HistoricalRecords()

    class Meta:
        verbose_name = 'Стойка регистрации'
        verbose_name_plural = 'Стойки регистрации'
        db_table = 'TICKETS_Receptionist'


    def __str__(self):
        return self.name

class Promocode(models.Model):
    TYPE_PROMO = (
        ('percent', 'Процент'),
        ('number', 'Число')
    )

    code = models.CharField(
        null=False, blank=False,
        max_length=100, verbose_name='Промокод',
        unique=False,
    )
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE,
        verbose_name='Проект',
    )
    date_create = models.DateTimeField(
        verbose_name='Дата создания',
        auto_now_add=True,
    )
    date_activate = models.DateTimeField(
        verbose_name='Дата активации',
        null=True, blank=True
    )
    date_start = models.DateField(
        verbose_name='Дата начала',
        null=True, blank=True
    )
    date_end = models.DateField(
        verbose_name='Дата окончания',
        null=True, blank=True
    )
    tickets_category = models.ManyToManyField(TicketsCategory, blank=True)

    type = models.CharField(
        verbose_name='Вид скидки', default='percent',
        choices=TYPE_PROMO, max_length=100
    )
    value = models.FloatField(
        verbose_name='Величина'
    )
    contact = JSONField(null=True, blank=True,)

    count = models.IntegerField(
        null=True, blank=True,
        verbose_name='Количество',
    )
    summ_all = models.BooleanField(
        default=True, verbose_name='Суммировать с остальными скидками'
    )
    history = HistoricalRecords()

    class Meta:
        db_table = 'TICKETS_Promocode'
        # ordering = ['-date']

    def save(self, *args, **kwargs):
        if self.contact and self.contact.__len__()>0:
            self.count = self.contact.__len__()
        if not self.date_start:
            self.date_start = datetime.datetime.now()

        if not self.date_end:
            self.date_end = datetime.datetime.strptime(self.project.date_end.strftime('%m/%d/%Y ') + self.project.time_end.strftime('%H:%M'), '%m/%d/%Y %H:%M')

        super(Promocode, self).save(*args, **kwargs)

class Order(models.Model):
    STATE_ORDER = (
        (0, 'не оплачен'),
        # (0, 'не оплачен'),
        (1, 'оплачен'),
        (2, 'отменен'),
    )

    STATE_EMAIL = (
        (0, 'не отправлен'),
        (1, 'отправлен'),
        (2, 'повтор'),
    )
    PAYMENT_METHOD =(
        ('online','Онлайн'),
        ('cash','Касса'),
        ('free_online','Бесплатный Онлайн'),
        ('free_cash','Бесплатный Касса'),
        ('kiosk','Киоск'),
        ('expobit','Expobit'),
    )
    LOYALYY_TYPE = (
        ('promocode','Промокод'),
    )

    number = models.CharField(
        max_length=30, verbose_name='Номер заказа',
        # blank=True, null=True
    )

    project = models.ForeignKey(
        Project, on_delete=models.CASCADE,
        verbose_name='Проект',
        null=False, blank=False,
    )

    date = models.DateTimeField(
        verbose_name='Дата',
        auto_now_add=True,
    )
    uuid = models.UUIDField(null=True, blank=True, editable=False)

    receipt_id = JSONField(null=True, blank=True,)

    state = models.IntegerField(
        verbose_name='Статус заказа', default=0,
        choices=STATE_ORDER,
    )

    state_email = models.IntegerField(
        verbose_name='Статус отправки почты', default=0,
        choices=STATE_EMAIL,
    )

    buyer = models.CharField(
        max_length=200,
        verbose_name='E-mail'
    )
    payment_method  = models.CharField(
        verbose_name='Способ оплаты', max_length=30,
        default='online',
        choices=PAYMENT_METHOD,
    )

    pan = models.CharField(
        max_length=30, verbose_name='Номер карты',
        blank=True, null=True
    )

    amount = models.FloatField(
        verbose_name='Сумма', default=0,
    )
    surcharge = models.FloatField(
        verbose_name='Доплата', default=0,
    )
    loyalty =  models.CharField(
        verbose_name='Вид лояльности', max_length=30,
        null=True, blank=True,
        choices=LOYALYY_TYPE,
    )
    loyalty_promocode = models.ForeignKey(
        Promocode, on_delete=models.CASCADE,
        verbose_name='Промокод',
        null=True, blank=True,
    )

    cardAuthInfo = models.CharField(
        max_length=30, verbose_name='Имя владельца',
        blank=True, null=True
    )

    history = HistoricalRecords()

    class Meta:
        db_table = 'TICKETS_Order'
        # ordering = ['-date']

class TicketsPlane(models.Model):
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE,
        verbose_name='Проект',
        null=False, blank=False,
    )

    row = models.IntegerField(
        default=0
    )
    plase = models.IntegerField(
        default=0
    )
    count = models.IntegerField(
        default=0, verbose_name='Проект',
    )
    edit = models.BooleanField(
        default=True,
        verbose_name='Доступность редактирования',
    )

    json_tickets = JSONField()



    class Meta:
        verbose_name = 'Планировка зала'
        verbose_name_plural = 'Планировки залов'

    # def __str__(self):
    #     return self.phone

class Tickets(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    number = models.IntegerField(
        verbose_name='Номер билета',
    )
    code = models.CharField(
        max_length=13, verbose_name='Код билета',
        unique=True,
    )
    category = models.ForeignKey(
        TicketsCategory, on_delete=models.CASCADE,
        verbose_name='Категория билета',
        null=False, blank=False,
    )
    plase = JSONField()

    order = models.ForeignKey(
        Order, on_delete=models.CASCADE,
        verbose_name='Номер заказа',
        null=True, blank=True,
    )
    first_cost = models.IntegerField(
        blank=True, null=True,
        verbose_name='Начальная стоимость',
    )
    total_cost = models.IntegerField(
        blank=True, null=True,
        verbose_name='Итоговая стоимость',
    )
    edit_cost = models.BooleanField(
        default=False, verbose_name='Редактирование оплаты'
    )
    state = models.BooleanField(
        verbose_name='Состояние активации',default=False
    )

    status = models.BooleanField(
        default=True, verbose_name='Статус'
    )

    date_create = models.DateTimeField(
        verbose_name='Дата создания', auto_now_add=True
    )
    date_activate = models.DateTimeField(
        verbose_name='Дата активации', blank=True, null=True
    )
    count_printing = models.IntegerField(default=0, verbose_name='Количество распечаток')

    ticket_email = models.ImageField(
        blank=True,
        upload_to='./barcode/',
        verbose_name='Билет')

    ticket_check = models.ImageField(
        blank=True,
        upload_to='./barcode/',
        verbose_name='Билет')

    ticket_graph = models.ImageField(
        blank=True,
        upload_to='./barcode/',
        verbose_name='Билет')

    history = HistoricalRecords()

    class Meta:
        db_table = 'TICKETS_Tickets'
        ordering = ['-date_create']

    def save(self, *args, **kwargs):
        # try:
        #     obj = TicketsCategory.objects.get(id=self.id)
        #     self.date_update = datetime.datetime.today()
        #
        # except TicketsCategory.DoesNotExist:
        #
        #     self.date_create = datetime.datetime.today()

        super(Tickets, self).save(*args, **kwargs)

class Forms_Data(models.Model):
    ticket = models.ForeignKey(
        Tickets, on_delete=models.CASCADE,
        verbose_name='Привязка к билету',
        null=True, blank=True,
    )

    date = models.DateTimeField(
        verbose_name='Дата создания',
        auto_now_add=True, blank=True
    )

    data = JSONField(verbose_name='Данные анкеты')

    class Meta:
        db_table = 'TICKETS_Forms_Data'


