from django.urls import path, include
from rest_framework.routers import DefaultRouter

from QUALITY import views
from .views import *


router = DefaultRouter()


urlpatterns = [
    path(r'', views.index, name='index'),
    # path(r'<str:room_name>/', views.room, name='room'),
    # path(r'', include(router.urls)),
]

