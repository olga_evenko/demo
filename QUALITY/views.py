from django.shortcuts import render
from .models import *
from rest_framework.generics import ListAPIView
from django.http import JsonResponse
from django.core.paginator import Paginator
from django.apps import apps
from datetime import *

import json
import os
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework import status
from django.db import connections
from rest_framework.permissions import IsAuthenticated, AllowAny, BasePermission,IsAuthenticatedOrReadOnly

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.decorators import detail_route, list_route, permission_classes
from operator import itemgetter

# from backend.backend.PROJECT.models import Project
from .models import *
from .serializers import *
# Create your views here.


def index(request):
    return render(request, 'QUALITY/index.html', {})


class TestQuality(viewsets.ModelViewSet):

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    authentication_class = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def list(self, request, id=None, pk=None):
        a=0
        # return Response({'rows': content, 'totalRecords': count_visitors})


    def retrieve(self, request, pk=None):
        a = 0
            # return Response({'detail':' Ошибочный запрос'}, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['get'])
    def options(self, request, ):
        content = {}

        content['question_type'] = []
        for TYPE in Question.TYPE:
            content['question_type'].append({'value':TYPE[0], 'text':TYPE[1]})

        content['interview_user'] = []
        users = ExtUser.objects.filter(is_quality=True)
        for user in users:
            content['interview_user'].append({'value':user.id, 'text':user.firstname})

        content['interview_project'] = []
        projects = Project.objects.all().order_by('date_start')[:20]
        for project in projects:
            content['interview_project'].append({'value': project.id, 'text': project.name})

        return Response(content)


    @list_route(methods=['get'])
    def questions(self, request, ):

        questions = Question.objects.all()

        serializer = QuestionsListSerializer(questions, many=True).data

        return Response({'rows': serializer})


    @list_route(methods=['get', 'post'])
    def question(self, request):

        if request.GET:
            data = json.loads(request.GET['data'])
            obj = Question.objects.get(id=data['id'])
            serializer = QuestionsSerializer(obj)
            return Response(serializer.data)

        if request.POST:
            data = request.POST
            if 'id' in data:
                obj = Question.objects.get(id=data['id'])
            else:
                obj = Question()

            serializer = QuestionsSerializer(obj, data=data)

            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)

            return Response({})


    @list_route(methods=['get'])
    def interviews(self, request, ):

        if request.user.is_quality:
            interview = Interview.objects.get(user=request.user, active=True)
            serializer = InterviewSerializer(interview)
            return Response(serializer.data)
        else:
            interviews = Interview.objects.all()
            serializer = InterviewListSerializer(interviews, many=True).data
            return Response({'rows': serializer})

    @list_route(methods=['get', 'post'])
    def interview(self, request):

        if request.GET:
            data = json.loads(request.GET['data'])
            obj = Interview.objects.get(id=data['id'])
            serializer = InterviewSerializer(obj)
            return Response(serializer.data)

        if request.POST:
            data = request.POST
            if 'id' in data:
                obj = Interview.objects.get(id=data['id'])
            else:
                obj = Interview()

            serializer = InterviewSerializer(obj, data=data)

            if serializer.is_valid():
                serializer.save()

                if 'questions' in data:
                    q = QuestionCustom.objects.filter(interview = Interview.objects.get(id = data['id']))
                    for i in q:
                        i.delete()

                    questions = json.loads(data['questions'])
                    for question in questions:
                        ques = QuestionCustom(
                            question = Question.objects.get(id=question['id']),
                            interview = Interview.objects.get(id = data['id']),
                            audio=question['audio']
                        )

                        ques.save()

                return Response(serializer.data)

            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['get', 'post'])
    def answers(self, request):
        if 'data' in request.GET:
            obj = Answer.objects.all()

            serverData = json.loads(request.GET['data'])

            if (serverData['sort']['field'] and serverData['sort']['type']):

                if serverData['sort']['type'] == 'desc':
                    serverData['sort']['field'] = '-' + serverData['sort']['field']

                    obj = obj.order_by(serverData['sort']['field'])

            if 'select' in serverData and serverData['select'].keys().__len__():
                for select in serverData['select']:
                    if serverData['select'][select]:
                        obj = obj.filter(**serverData['select'])


            temp_content = AnswerReactionSerializer(obj, many=True).data
            paginator = Paginator(temp_content, serverData['perPage'])
            content = paginator.page(serverData['page']).object_list

            count_visitors = obj.count()

            return Response({'rows': content, 'totalRecords': count_visitors})

        return Response({'rows': {}, 'totalRecords': 0})


    @list_route(methods=['get', 'post'])
    def answer(self, request):
        if request.GET:
            data = json.loads(request.GET['data'])
            obj = Answer.objects.get(id=data['id'])
            serializer = AnswerReactionSerializer(obj)
            return Response(serializer.data)

        if request.POST:
            data = request.POST
            if 'id' in data:
                obj = Interview.objects.get(id=data['id'])
            else:
                obj = Interview()

            serializer = InterviewSerializer(obj, data=data)

            if serializer.is_valid():
                serializer.save()

                if 'questions' in data:
                    q = QuestionCustom.objects.filter(interview=Interview.objects.get(id=data['id']))
                    for i in q:
                        i.delete()

                    questions = json.loads(data['questions'])
                    for question in questions:
                        ques = QuestionCustom(
                            question=Question.objects.get(id=question['id']),
                            interview=Interview.objects.get(id=data['id']),
                            audio=question['audio']
                        )

                        ques.save()

                return Response(serializer.data)

            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['get', 'post'])
    def reaction(self, request):
        if request.GET:
            data = json.loads(request.GET['data'])
            try:
                obj = Reaction.objects.get(id=data['id'])
            except Reaction.DoesNotExist:
                obj = Reaction()
            serializer = ReactionSerializer(obj)
            return Response(serializer.data)

        if request.POST:
            data = request.POST

            obj = Answer.objects.get(id=data['answer'])

            if obj.reaction:
                react = obj.reaction
            else:
                react = Reaction()

            data_reaction = json.loads(data['reaction'])
            serializer = ReactionSerializer(react, data=data_reaction)

            if serializer.is_valid():
                serializer.save()

                obj.reaction = Reaction.objects.get(id = serializer.data['id'])
                obj.save()

                return Response(serializer.data)

            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'])
    def save_audio(self, request, ):
        import os
        from django.core.files.storage import default_storage
        from django.conf import settings
        from pydub import AudioSegment

        if 'answer' in request.data:
            data = json.loads(request.data['answer'])
            interview = json.loads(request.data['interview'])
            audio_index=0
            index=0
            for ques in interview['questions']:
                obj = Answer()

                answer = {
                    "question":ques['id'],
                    "interview": interview['id'],
                    "value":data[index],
                    "upload":None,
                }
                index+=1

                if ques['audio'] and 'file'+str(audio_index) in request.FILES:
                    file = request.FILES['file'+str(audio_index)]
                    filename = str(datetime.today())

                    with default_storage.open(settings.BASE_DIR + '/media/sing/' + filename+'.webm', 'wb+') as destination:
                        for chunk in file.chunks():
                            destination.write(chunk)

                    answer['upload'] = "/media/sing/"+filename+".webm"
                    audio_index+=1

                serializer = AnswerSerializer(obj, answer)
                if serializer.is_valid():
                    serializer.save()

        return Response({})


    @list_route(methods=['get'])
    def chart_data(self, request):
        if request.GET:
            content=[]
            data = json.loads(request.GET['data'])
            if 'all' in data:
                questions = Interview.objects.get(id = data['all']['interview']).question.all()
                answer = Answer.objects.filter(date_create__gte = data['all']['start'],
                                               date_create__lte = data['all']['end'],
                                               interview__id= data['all']['interview'])

                start = datetime.strptime(data['all']['start'], "%Y-%m-%d")
                end = datetime.strptime(data['all']['end'], "%Y-%m-%d") + timedelta(days=1)
                date_generated = [start + timedelta(days=x) for x in range(0, (end - start).days)]

                for question in questions:

                    temp = {
                        'name':question.text,
                        'label':[],
                        'data':[],
                    }

                    for date in date_generated:
                        temp['label'].append(date.strftime("%Y-%m-%d"))
                        answer_list = answer.filter(question__id=question.id, date_create__year=date.year,
                                                          date_create__month=date.month, date_create__day=date.day)
                        answer_summ = 0
                        answer_count = 0
                        for answ in answer_list:
                            answer_summ += int(answ.value)
                            answer_count+=1

                        temp['data'].append((answer_summ/answer_count) if answer_count else 0)

                    content.append(temp)
                return Response({'statictic':content})







                    # if (data['data']['category']):
                    #     content['buy']['site'][date.strftime("%Y-%m-%d")] = []
                    #     for cat in category:
                    #         content['buy']['site'][date.strftime("%Y-%m-%d")] = Tickets.objects.filter(
                    #             order__project__id=id, order__state=1,
                    #             order__payment_method__in=['online', 'free_online'],
                    #             order__date__year=date.year, order__date__month=date.month,
                    #             order__date__day=date.day).count()
                    # a = 0
            if 'one' in data:
                a = 0
