from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from .models import *
from datetime import *
from django.utils import timezone

class CustomDateTimeField(serializers.DateTimeField):
    def to_representation(self, value):
        tz = timezone.get_default_timezone()
        value = timezone.localtime(value, timezone=tz)
        return super().to_representation(value)


class QuestionCustomSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField("_is_id")
    text = serializers.SerializerMethodField("_is_text")
    type = serializers.SerializerMethodField("_is_type")

    class Meta:
        model = QuestionCustom
        fields = ("id", "text", "audio", 'type')

    def _is_id(self, obj):
        return obj.question.id

    def _is_text(self, obj):
        return obj.question.text

    def _is_type(self, obj):
        return obj.question.type

class QuestionsListSerializer(serializers.ModelSerializer):
    interview = serializers.SerializerMethodField("_is_interview")

    class Meta:
        model = Question

        fields = ("id", "text", "type", "interview", )
    def _is_interview(self, obj):
        return ""


class QuestionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ("id", "text", "type")


class InterviewListSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField("_is_user")
    project = serializers.SerializerMethodField("_is_project")
    active = serializers.SerializerMethodField("_is_active")

    class Meta:
        model = Interview

        fields = ("id", "name", "user", "project", "active")
    def _is_user(self, obj):
        if obj.user:
            return obj.user.firstname
        return ""

    def _is_project(self, obj):
        if obj.project:
            return obj.project.name
        return ""

    def _is_active(self, obj):
        if obj.active:
            return 'Активная'
        return "Не активная"

class InterviewSerializer(serializers.ModelSerializer):
    questions = serializers.SerializerMethodField("_is_questions")

    class Meta:
        validators = [
            UniqueTogetherValidator(
                queryset=Interview.objects.filter(active=True),
                fields=('user',)
            )
        ]
        model = Interview

        fields = ("id", "name", "user", "project", "active", 'questions')

    def _is_questions(self, obj):
        if obj.question:
            ques = QuestionCustom.objects.filter(interview = obj.id)
            # ques = obj.question.all()


            return QuestionCustomSerializer(ques, many=True).data
        return []


class AnswerSerializer(serializers.ModelSerializer):
    date_create = CustomDateTimeField(read_only=True)
    question_text = serializers.SerializerMethodField("_is_question")
    value_text = serializers.SerializerMethodField("_is_value")
    upload_text = serializers.SerializerMethodField("_is_upload")

    class Meta:
        model = Answer

        fields = ("id", "question", "interview", "value", "upload", 'date_create', 'question_text', 'value_text', 'upload_text')

    def _is_question(self, obj):
        if obj.question:
           return obj.question.text
        return ''

    def _is_value(self, obj):
        if obj.question:
            if obj.question.type=='rate':
                return obj.value
            if obj.question.type=='bool':
                return 'Отлично' if obj.value else 'Плохо'

        return ''

    def _is_upload(self, obj):
        if obj.upload:
            return 'Да'

        return ''

class AnswerReactionSerializer(serializers.ModelSerializer):
    date_create = CustomDateTimeField(read_only=True)
    reaction__listening = serializers.SerializerMethodField("_is_reaction__listening")
    question__text = serializers.SerializerMethodField("_is_question__text")
    interview__id = serializers.SerializerMethodField("_is_interview__id")
    upload_text = serializers.SerializerMethodField("_is_upload")

    class Meta:
        model = Answer

        fields = ("id", "value", "question__text", "interview__id",  "upload", "upload_text", 'reaction__listening', 'date_create', 'reaction')

    def _is_reaction__listening(self, obj):
        if obj.reaction:
            if obj.reaction.listening:
                return 'Прослушано'
        return ''

    def _is_question__text(self, obj):
        if obj.question:
            return obj.question.text
        return ''

    def _is_interview__id(self, obj):
        if obj.interview:
            return obj.interview.id
        return ''

    def _is_upload(self, obj):
        if obj.upload:
            return 'Да'
        return ''


class ReactionSerializer(serializers.ModelSerializer):
    date_create = CustomDateTimeField(read_only=True)

    class Meta:
        model = Reaction

        fields = ("id", "listening", "coment", "priority",  'date_create')




