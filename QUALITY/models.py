from django.db import models
from PROJECT.views import *
from plus_administration.models import *
from simple_history.models import HistoricalRecords

from datetime import *

from PIL import Image
# import numpy as np
from django.contrib.postgres.fields import JSONField
from simple_history.models import HistoricalRecords

class Question(models.Model):

    TYPE = (
        ('bool','Да/Нет'),
        ('rate','Рейтинг'),
    )

    text = models.CharField( verbose_name='Текст вопроса', max_length=1000,)
    type = models.CharField( choices=TYPE, verbose_name='Тип вопроса',max_length=20,)
    interview = models.ManyToManyField('Interview', through='QuestionCustom', related_name='question')

    # history = HistoricalRecords()

    class Meta:
        db_table = 'QUALITY_Question'
        # ordering = ['date_create']


class Interview(models.Model):

    name = models.CharField(verbose_name='Наименование', max_length=200,)
    user = models.ForeignKey(ExtUser, on_delete=models.CASCADE, null=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE,verbose_name='Проект',null=True, blank=False,)

    active =models.BooleanField(verbose_name='Активность')
    # history = HistoricalRecords()

    class Meta:
        db_table = 'QUALITY_Interview'
        # ordering = ['date_create']

    def save(self, *args, **kwargs):
        if(self.user or self.project):
            super(Interview, self).save(*args, **kwargs)




class QuestionCustom(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='membership')
    interview = models.ForeignKey(Interview, on_delete=models.CASCADE, related_name='membership')
    audio = models.BooleanField(default=False, verbose_name='Запись аудио ответа')

    class Meta:
        db_table = 'QUALITY_QuestionCustom'
        # ordering = ['date_create']

class Reaction(models.Model):
    PRIORITY = (
        ('1', 'Низкий'),
        ('3', 'Средний'),
        ('5', 'Высокий'),
    )
    listening = models.BooleanField(verbose_name='Статус прослушивания', default=False)
    coment = models.CharField(verbose_name='Коментарий', max_length=1000, null=True)
    priority = models.CharField(verbose_name='Приоритет', choices=PRIORITY, default='1', max_length=2, null=True)
    date_create = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True, blank=True, null=True)
    history = HistoricalRecords()

    class Meta:
        db_table = 'QUALITY_Reaction'

class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, verbose_name='Вопрос')
    interview = models.ForeignKey(Interview, on_delete=models.CASCADE, verbose_name='Опрос')
    value = models.CharField(verbose_name='Значение',max_length=20,)
    upload = models.CharField(verbose_name='Ссылка на файл',max_length=200, null=True)
    reaction = models.ForeignKey(Reaction, on_delete=models.CASCADE, verbose_name='Решение', null=True)
    date_create = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True, blank=True, null=True)

    class Meta:
        db_table = 'QUALITY_Answer'



